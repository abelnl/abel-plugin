function setIconChosenDropdown(){
	jQuery('.icon-dropdown select').chosen({
		html_template: '<svg role="img" class="{type}"><use xlink:href={url}></use></svg><span>{text}</span>'
	});
}


function setButtonPanel(){

	jQuery('.button-settings .chosen' ).chosen({ 'allow_single_deselect': true });
	
	jQuery('.button-settings > .dashicons').on( 'click tap', function(){
		jQuery( this ).parent().find('.type-sub-menu').toggleClass( 'active' );

		jQuery('.button-settings .icon-dropdown select').chosen({
			html_template: '<svg role="img" class="{type}"><use xlink:href={url}></use></svg><span>{text}</span>'
		});
	});
}


function stopButtonPanel(){

	jQuery('.button-settings > .dashicons' ).off();
	jQuery('.button-settings > #close' ).off();
}


function setTitleFields(){

	$('.settings-wrapper .title-wrapper .multi').on( 'change', function( evt ){
		var _item = $( evt.target );
		var _val = _item.val();

		_item.parent().parent().parent().find( '.icon' ).html( _val );
	});

	$('.settings-wrapper .title-wrapper .multi').each( function(){
		var _name = $( this ).data( 'name' );
		$( this ).attr( 'name', _name );
	})
}


function setMenuSelect(){

	$('.field-menutype').on( 'change', function(){
		var _val = $( this ).val();
		if( $( '#menu-img' ).length == 0 )
			$( this ).parent().before('<div id="menu-img" class="menu-img-container"></div>');

		$('#menu-img').removeClass('menu-1');
		$('#menu-img').removeClass('menu-2');
		$('#menu-img').removeClass('menu-3');
		$('#menu-img').removeClass('menu-4');
		$('#menu-img').removeClass('menu-5');
		$('#menu-img').removeClass('menu-101');

		$('#menu-img').addClass( 'menu-'+_val );
	});

	$('.field-menutype' ).trigger( 'change' );

}

/**
 * Toggle grid view based on post-types
 */
function setCollectionAdditions(){

	$('.collection .field-post_type, .handpickedcollection .field-post_type' ).on( 'change', function(){

		var _val = $( this ).val();
		var _notAllowed = [ 'brand', 'partner', 'reference', 'teammember'];
		var _column = $( this ).parents( '.column' );

		if( $.inArray( _val, _notAllowed ) > 0 ){
			_column.find('.valgrid').addClass( 'hidden' );
		}else{
			_column.find('.valgrid').removeClass( 'hidden' );
		}

	});

	$('.collection .field-post_type, .handpickedcollection .field-post_type' ).each( function(){
		$( this ).trigger( 'change' );
	});
}

/**
 * Add slider time based on slider-selection
 */
function setSliderTime(){
	$('.viewselect input').on( 'change', function(){
		
		var _parent = $( this ).parents('.viewselect');
		var _slidertime = _parent.next('.slidertime');

		if( $( this ).val() == 'slider' ){
			_slidertime.removeClass( 'hidden' );
		}else{
			_slidertime.addClass( 'hidden' );
		}
	});

	$('.viewselect input:checked').trigger( 'change' );
}


jQuery( document ).ready(function( $ ){
	setIconChosenDropdown();
	setButtonPanel();
	setTitleFields();
	setMenuSelect();
	setCollectionAdditions();
	setSliderTime();
});

jQuery( document ).on( 'refreshFields', function(){
 	setIconChosenDropdown();

 	stopButtonPanel();
 	setButtonPanel();
 	setTitleFields();
	setMenuSelect();
	setCollectionAdditions();
	setSliderTime();
});