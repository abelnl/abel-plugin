
<a href="<?= $column->getField('link'); ?>" class="ac_toptasks_item" data-s-amount-item>
    <div class="ac_toptasks_item_container">
        <div class="ac_toptasks_item_image-container">
            <figure class="ac_toptasks_item_image a_cover-cc a_medium_cover-ct" data-interchange="[<?= $column->getField('image'); ?>, small], [<?= $column->getField('image'); ?>, large]" style="background-image: url(<?= $column->getField('image'); ?>)"></figure>
        </div>
        <div class="ac_toptasks_item_icon-container">
            <div class="ac_toptasks_item_icon">
                <?= get_svg_symbol($column->getField('icon')); ?>
            </div>
        </div>
        <div class="ac_toptasks_item_content">
            <div class="ac_toptasks_item_content_title"><?php $column->theTitle(); ?></div>
            <div class="ac_toptasks_item_content_description"><?= $column->getField('description');?></div>
            <div class="ac_toptasks_item_content_btn-container">

                <div href="<?= $column->getField('link'); ?>" class="button v_has-icon-right">
                    <?= $column->getField('buttontext'); ?>
                    <span class="svg-container">
                        <?= get_svg_symbol($column->getField('icon')); ?>
                    </span>
                </div>

            </div>
        </div>
    </div>
</a>