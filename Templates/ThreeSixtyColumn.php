<figure class="360-column" style="height: <?php echo $column->getHeight();?>">
    <a-scene loading-screen="enabled: false;" embedded>
        <a-entity camera="" look-controls="reverseMouseDrag: true"></a-entity>
        <a-sky src="<?php echo $column->getImage('full');?>"></a-sky>
    </a-scene>
</figure>
