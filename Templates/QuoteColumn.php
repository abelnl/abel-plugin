<?php
$quote = $column->getField('quote');
$author = $column->getField('author');
$alignment = $column->getField('alignment');
?>
<div class="ac_quote_item_container">
   
    <div class="ac_quote_item_content quote_alignment_<?= $alignment; ?>">
        <div class="ac_quote_item_content_quote"><?= $quote; ?></div>
        <?php 
        if( $author && $author != '' ) 
            echo '<div class="ac_quote_item_content_author">'.$author .'</div>';
        ?>
    </div>

</div>