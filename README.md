Abel Base Plugin
===========================

The Abel plugin provides the base framework for creating, generating and running new websites for Abel Nederland.

---

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| PHP Shorttags	  | `php.ini` 	 | [enable shorttags](https://stackoverflow.com/questions/2185320/how-to-enable-php-short-tags) |

---

## Installation

Installation of this plugin out-of-the-box is pretty easy: 

1. Clone it to an "Abel"-folder: `git clone git@bitbucket.org:abelnl/abel-plugin.git abel`
2. Place that folder in wp-content/plugins
3. Enable the WordPress plugin.

If you're using the [Abel deployment tools](https://bitbucket.org/abelnl/abel-deployment), you won't have to install anything manually.

---

## Contributing

Everyone is welcome to help [contribute](CONTRIBUTING.md) and improve this project. There are several ways you can contribute:

* Reporting issues
* Suggesting new features
* Writing or refactoring code
* Fixing [issues](https://github.com/cuisine-wp/cuisine/issues)



