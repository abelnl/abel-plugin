<?php

	namespace Abel\Front;


	class Settings{


		/**
		 * Get a setting
		 *
		 * @param  string $key
		 * @return mixed
		 */
		public static function get( $key, $default = false ){

			$settings = self::findSettings();

			if( isset( $settings[ $key ] ) )
				return $settings[ $key ];

			return $default;

		}


		/**
		 * Returns all default settings
		 * 
		 * @return Array
		 */
		public static function getDefaults()
		{
			$defaults = [
				'calltoaction' => '',
				'button_1_text' => '',
				'button_1_link' => '',
				'button_1_icon' => '',
                'button_2_text' => '',
				'button_2_link' => '',
				'button_2_icon' => '',
                'telephone' => '',
				'email' => '',
                'googleplus' => '',
				'facebook' => '',
                'twitter' => '',
				'linkedin' => '',
				'instagram' => '',
				'social_description' => '',
				'googleplus_description' => '',
				'facebook_description' => '',
                'twitter_description' => '',
				'linkedin_description' => '',
				'instagram_description' => '',
				'news-page' => 'none',
				'reference-page' => 'none',
				'service-page' => 'none',
				'partner-page' => 'none',
				'brand-page' => 'none',
				'product-page' => 'none',
				'teammember-page' => 'none',
				'abel-media-page' => false,
                'ga_code' => '',
				'hotjar_code' => '',
                'toptask_image' => 'icon',
				'products' => 'none',
				'menu_type' => 'horizontal_1',
				'icon_class' => 'stroke',
				'icon_library' => 'set1',
				'footertype' => 1,
				'footersection' => 'none',
				'cookie_show' => false,
				'cookie_title' => 'Cookies',
				'cookie_message' => __( 'Op deze site gebruiken wij cookies', 'abelplugin' ),
				'cookie_buttontext' => __( 'Ik begrijp het', 'abelplugin' ),
				'default_background_color' => 'transparent',
				'default_text_color' => 'darkest-gray',
				'default_container_width' => '1200',
				'default_fill_color' => 'default',
				'default_padding-top' => '20',
				'default_padding-bottom' => '20',
				'default_margin-top' => '0',
				'default_margin-bottom' => '0'
			];

			return $defaults;	
		}



		/**
		 * Get the settings
		 *
		 * @return array
		 */
		public static function findSettings(){

			//$defaults = array_merge( $defaults, Messages::all() );
			return get_option( 'website-settings', static::getDefaults() );

		}



	}

