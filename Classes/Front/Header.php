<?php

	namespace Abel\Front;

	use Abel\Helpers\Icon;
	use Abel\Front\Settings;
	use Cuisine\Utilities\Url;
	use Cuisine\Wrappers\Template;
	use Abel\Helpers\HeaderLayouts as Layouts;

	class Header{


		/**
		 * String containing the layout
		 * 
		 * @var String
		 */
		protected $layout;


		/**
		 * The current menu
		 * 
		 * @var String
		 */
		protected $menu;

		/**
		 * The current zone
		 * 
		 * @var Array
		 */
		protected $zone;


		/**
		 * The current template
		 * 
		 * @var TemplateFinder object
		 */
		protected $template;


		/**
		 * Does this zone have a divider?
		 * 
		 * @var bool
		 */
		protected $divider;


		/**
		 * Get the content from a zone in a menu
		 * 
		 * @param  String $menu
		 * @param  Int $zone
		 * 
		 * @return Abel\Front\Header
		 */
		public function content( $menu, $zone )
		{
			$this->menu = $menu;
			$this->divider = false;
			$this->zone	= $this->getZone( $zone );
			
			if( !is_null( $this->zone ) )
				$this->layout = $this->zone['layout'];

			return $this;
		}


		/**
		 * Return a zone-type for mobile
		 * 
		 * @param  String $zoneType
		 * 
		 * @return String
		 */
		public function mobile( $zoneType )
		{

			if( $zoneType == 'menu' ){

				$this->zone = [
					'layout' => 'menu-mobile',
					'menuId' => get_option('abel_mobile_menu', 0),
					'position' => 0
				];

			}else if( $zoneType == 'socials' ){

				$this->zone = [
					'layout' => 'socials',
					'position' => 0,
					'iconSize' => 'small'
				];

			}else{
				$zone = $this->searchForzone( $zoneType );
				if( !is_null( $zone ) )
					$this->zone = $zone;
			}

			$this->layout = $this->zone['layout'];

			if( is_array( $this->zone ) && !empty( $this->zone ) )
				return $this->get();
					

			return '';
		}


		/**
		 * Search for a specific zone-type
		 * 
		 * @param  String $type
		 * 
		 * @return Array
		 */
		public function searchForZone( $type )
		{
			$settings = get_option( 'website-settings', [] );
			foreach( $settings as $key => $setting ){

				if( substr( $key, 0, 4 ) == 'zone' && isset( $setting[0] ) ){
						
					$setting = $setting[0];

					if( $setting['layout'] == $type )
						return $setting;

				}
			}

			return null;
		}
	

		/**
		 * Returns the zone args
		 * 
		 * @return Array
		 */
		public function getZone( $_zone )
		{
			$zone = Settings::get( 'zone_'.$_zone );
			if( !empty( $zone ) ){
				$zone = array_values( $zone );

				if( isset( $zone[1] ) && $zone[1]['layout'] == 'divider' )
					$this->divider = true;

				return $zone[0];
			}

			return null;
		}

		/**
		 * Returns the template with the right variables
		 * 
		 * @return String
		 */
		public function get()
		{
			if( is_null( $this->zone ) )
				return '';

			//else, return the template
			ob_start();
			$this->display();
			return ob_get_clean();
		}

		/**
		 * Display the template with the right variables
		 * 
		 * @return void
		 */
		public function display()
		{
			$args = $this->getArgs();

			Template::find( 
				'partials/menus/content/'.$this->layout, 
				null
			)->display( $args );

			if( $this->divider ){
				Template::find( 'partials/menus/content/divider', null )->display([ 'menu' => $args['menu'] ]);
			}
		
		}


		/**
		 * Display the site's logo:
		 * 
		 * @return String
		 */
		public function logo()
		{

			$logo = get_transient( 'abel_theme_logo' );

			//if( !$logo || ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) ){

				$path = Url::path('theme', 'public/assets/img/logo/' );
				$url = Url::theme( 'public/assets/img/logo/' );

				if( file_exists( $path . 'logo.svg' ) ){
					$logo = $url . 'logo.svg';
				}else if( file_exists( $path . 'logo.png' ) ){
					$logo = $url . 'logo.png';
				}else{
					$logo = Url::parentTheme( '/public/assets/img/logos/logo_abel-installatie.svg' );
				}

			//	set_transient( 'abel_theme_logo', $logo );			
			//}

			return $logo;
		}


		/**
		 * Returns arguments for a header layout
		 * 
		 * @return Array
		 */
		public function getArgs()
		{
			$response = [];
			$fields = Layouts::getFields( $this->layout );

			$response['menu'] = $this->menu;
			$response['layout'] = $this->layout;		

			foreach( $fields as $field ){

				$value = null;
				if( isset( $this->zone[ $field->name ] ) ){
					$value = $this->zone[ $field->name ];

					if( $field->type == 'icon' && $this->zone[ $field->name ] !== 'none' )
						$value = Icon::get( $this->zone[ $field->name ] ); 
				}

				$response[ $field->name ] = $value;

			}

			if( $this->layout == 'menu-mobile' )
				$response['menuId'] = $this->zone['menuId'];

			return $response; 
		}


		/**
		 * Add Google Tagmanager
		 * 
		 * @return String
		 */
		public function tagManager()
		{

			$html = '';
			$ga = Settings::get( 'ga_code' );
    	    
			if( $ga && $ga != '' ){

				if( substr( $ga, 0, 3 ) == 'GTM' ){

					$html = "<noscript><iframe src=\"http://www.googletagmanager.com/ns.html?id={$ga}\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','{$ga}');</script>";
				}else{

					$html = "<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                    ga('create', '".$ga."', 'auto');
                    ga('set', 'anonymizeIp', true);
					ga('send', 'pageview');\n";
					
					$html .= "</script>";

				}
			}

			return preg_replace( '/[ \t]+/', ' ', preg_replace( '/[\r\n]+/', "\n", $html ) );
		}

	}