<?php

    namespace Abel\Front;

    use Cuisine\Utilities\Url;
    use \Abel\Wrappers\StaticInstance;
    use \Abel\Front\Settings;

	class SMTP extends StaticInstance{

		var $resolution;

		var $settings;

		/**
		 * Init mail events & vars
		 */
		function __construct(){
            $this->listen();
		}

		
		/**
		 * Listen to the required mailevents
		 * 
		 * @return void
		 */
		function listen(){

			add_action( 'phpmailer_init', function( $phpmailer ){

                $settings = $this->getSettings();

				//settings:
				$settings = array(
					'ssl'		=> 	true,
					'auth'		=>	true,
					'host'		=> 	$settings['host'],
					'port'		=> 	'587',
					'user'		=>	$settings['user'],
					'password'	=>	$settings['password']
				);


				$settings = apply_filters( 'abel_smtp_settings', $settings );

				$phpmailer->IsSMTP();

				// Set the SMTPSecure value, if set to none, leave this blank
				if( $settings['ssl'] ){
					$phpmailer->SMTPSecure = 'tls';
                }
                
				// Set the other options
				$phpmailer->Host = $settings['host'];
				$phpmailer->Port = $settings['port'];
					
				// If we're using smtp auth, set the username & password
				if ( $settings['auth'] ) {

					$phpmailer->SMTPAuth = true;
					$phpmailer->Username = $settings['user'];
					$phpmailer->Password = $settings['password'];
				
				}

				$from_email = Settings::get( 'from_email' ) ?? null;
                $from_name = Settings::get( 'from_name' ) ?? null;

                if( is_null( $from_email ) || $from_email == '' ) {
	                $smtp_plugin_settings = get_option( "wp_mail_smtp", true );
		            if( is_array($smtp_plugin_settings) && isset($smtp_plugin_settings['mail']) ) {
		            	$from_email = $smtp_plugin_settings['mail']['from_email'];
		            	$from_name = $smtp_plugin_settings['mail']['from_name'];
		            }
		        }

				$phpmailer->From = $from_email;
				$phpmailer->FromName = $from_name;
			
				// You can add your own options here, 
				// see the phpmailer documentation for more info:
				// http://phpmailer.sourceforge.net/docs/
				$phpmailer = apply_filters('abel_smtp_custom_options', $phpmailer);
				
			});

		}




		/**
		 * Get all default settings in an array
		 * 
		 * @return array
		 */
		private function getSettings(){

			return array(
				'use_mandrill'	=> 'true',
				'host'		=> 'smtp.mandrillapp.com',
				'user'		=> 'luc.princen@gmail.com',
				'password'	=> 'nNaOcRRM9DU4KBDuDPXSFg'
			);

		}


    }

    \Abel\Front\SMTP::getInstance();