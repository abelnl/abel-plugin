<?php

	namespace Abel\Frontend;

	use \Abel\Front\Settings;
	use \Cuisine\Utilities\Url;
	use \Cuisine\Wrappers\User;
	use \Cuisine\Wrappers\Field;
	use \Cuisine\Wrappers\Route;
	use \Cuisine\Wrappers\Taxonomy;
	use \Cuisine\Wrappers\PostType;
	use \Abel\Wrappers\StaticInstance;
	use \Abel\Generators\ProductsGenerator;
	use \Abel\Helpers\Section as SectionHelper;

	class EventListeners extends StaticInstance{


		/**
		 * Init events & vars
		 */
		function __construct(){

			$this->settings();
			$this->postTypes();
			$this->hooks();
			$this->sections();
            $this->collection();
            $this->multisite();
            $this->deployment();
            $this->deprecated();
            $this->bugfixes();

		}



		/**
		 * Listen to front-end events
		 * 
		 * @return void
		 */
		public function settings(){

           

			//general settings:
			add_filter( 'cuisine_use_template_engine', '__return_false' );
			//add_filter(s 'chef_sections_save_html_output_as_content', '__return_false' );
			add_filter( 'chef_sections_display_section_wrapper', '__return_false' );

            add_filter( 'chef_sections_show_bottom_controls', '__return_false' );
		}

		/**
		 * Set post types
		 * 
		 * @return void
		 */
		public function postTypes()
		{
				/**
			 * Below are just some examples
			 */
			add_action( 'init', function(){

				//add featured image support:
				add_theme_support( 'post-thumbnails' ); 

				//removed default editor for a post:
				remove_post_type_support( 'post', 'editor' );

				//add a post-type
				PostType::make( 'service', 'Diensten', 'Dienst' )->set([
					'menu_icon' => 'dashicons-hammer',
                    'supports' => ['title', 'thumbnail', 'page-attributes'],
                    'hierarchical' => true,
                    'labels' => $this->getLabels( 'Dienst', 'Diensten' ),
					'rewrite' => [
						'with_front' => false,
						'slug' => 'dienst'
					],
					'has_archive' => false,
					'public' => true,
					'show_in_nav_menus' => true
				]);
				
				Taxonomy::make( 'service_cat', 'service', 'Dienst Categorieen', 'Dienst Categorie' )->set([
					'hierarchical' => true
				]);

				PostType::make( 'brand', 'Merken', 'Merk' )->set([
                    'menu_icon' => 'dashicons-awards',
                    'labels' => $this->getLabels( 'Merk', 'Merken' ),
					'supports' => [ 'title' ],
					'rewrite' => [
						'with_front' => false,
						'slug' => 'merk'
					],
					'has_archive' => false,
					'public' => true,
					'show_in_nav_menus' => true
				]);

				PostType::make( 'partner', 'Partners', 'Partner' )->set([
                    'menu_icon' => 'dashicons-businessman',
                    'labels' => $this->getLabels( 'Partner', 'Partners' ),
					'supports' => [ 'title' ],
					'rewrite' => [
						'with_front' => false,
						'slug' => 'partner'
					],
					'has_archive' => false,
					'public' => true,
					'show_in_nav_menus' => true

				]);
				
				PostType::make( 'reference', 'Projecten', 'Project' )->set([
                    'menu_icon' => 'dashicons-portfolio',
                    'labels' => $this->getLabels( 'Project', 'Projecten' ),
					'rewrite' => [
						'with_front' => false,
						'slug' => 'project'
					],
					'has_archive' => false,
					'supports' => [ 'title', 'thumbnail' ]
				]);

				Taxonomy::make( 'type', 'reference', 'Types', 'Type' )->set([
					'hierarchical' => true
				]);


				PostType::make( 'teammember', 'Teamleden', 'Teamlid' )->set([
					'menu_icon' => 'dashicons-id',
                    'public' => true,
                    'labels' => $this->getLabels( 'Teamlid', 'Teamleden' ),
					'exclude_from_search' => true,
					'show_in_admin_bar'   => false,
					'show_in_nav_menus'   => false,
					'publicly_queryable'  => false,
					'query_var'           => false,
					'has_archive'		  => false
				]);

                //local products:
                PostType::make( 'product', 'Producten', 'Product' )->set([
                    'menu_icon' => 'dashicons-cart',
                    'public' => false,
                    'show_ui' => true,
                    'supports' => [ 'title', 'thumbnail' ],
                    'has_archive' => false
                ]);

			});


			/**
			 * Add sections to products
			 */
			add_filter( 'chef_sections_post_types', function( $types ){

				$newTypes = [
					'post',
					'service',
					'brand',
					'partner',
					'reference'
				];

				//$products = Settings::get( 'products' );
				//if ( $products == 'manual' ) 
					$newTypes[] = 'product';

				return array_merge( $types, $newTypes );

            });
            
            //fixes an issue with collection post-types
            add_filter( 'chef_sections_collection_post_types', function( $types ){
                $types['product'] = 'Product';
                return $types;
            });
        }
        
        /**
         * Set dutch labels
         *
         * @param String $singular
         * @param String $plural
         * @return Array
         */
        public function getLabels( $singular, $plural ) 
        {
            return array(
                'name'                  => __( $plural, 'cuisine' ),
                'singular_name'         => __( $singular, 'cuisine' ),
                'add_new'               => __( 'Nieuwe toevoegen', 'cuisine' ),
                'add_new_item'          => __( 'Nieuwe toevoegen '. $singular, 'cuisine' ),
                'edit_item'             => __( 'Bewerk '. $singular, 'cuisine' ),
                'new_item'              => __( 'Nieuwe ' . $singular, 'cuisine' ),
                'view_item'             => __( 'Bekijk ' . $singular, 'cuisine' ),
                'search_items'          => __( 'Doorzoek ' . $singular, 'cuisine' ),
                'not_found'             => __( 'Geen '. $singular .' gevonden', 'cuisine' ),
                'not_found_in_trash'    => __( 'Geen '. $singular .' gevonden in de prullenbak', 'cuisine' ),
                'all_items'             => __( 'Alle ' . $plural, 'cuisine' ),
                'archives'              => __( 'Archieven', 'cuisine' ),
                'insert_into_item'      => __( 'Voeg in '.$singular, 'cuisine' ),
                'uploaded_to_this_item' => __( 'Upload naar '.$singular, 'cuisine' ),
                'featured_image'        => __( 'Uitgelichte afbeelding', 'cuisine' ),
                'set_featured_image'    => __( 'Stel uitgelichte afbeelding in', 'cuisine' ),
                'remove_featured_image' => __( 'Verwijder uitgelichte afbeelding', 'cuisine' ),
                'use_featured_image'    => __( 'Gebruik uitgelichte afbeelding', 'cuisine' ),
                'menu_name'             => __( $plural, 'cuisine' ),
                'filter_items_list'     => __( 'Filter '.$plural, 'cuisine' ),
                'name_admin_bar'        => __( $singular, 'cuisine' ),
                'parent_item_colon'     => '',
                'items_list_navigation' => '',
                'items_list'            => ''
            );
        }


		/**
		 * Hook into various plugins
		 * 
		 * @return void
		 */
		public function hooks()
		{
			//custom columns:
			add_filter( 'chef_sections_column_types', function( $types ){

				$base = Url::path( 'plugin', 'abel', true );

				$types['toptask'] = array(
					'name'		=> __('Toptaak', 'abelplugin'),
					'class'		=> 'Abel\Columns\ToptaskColumn',
					'template'	=> $base.'Templates/ToptaskColumn.php'
				);

				$types['quote'] = array(
					'name'		=> __('Quote', 'abelplugin'),
					'class'		=> 'Abel\Columns\QuoteColumn',
					'template'	=> $base.'Templates/QuoteColumn.php'
				);

				$types['cta'] = array(
					'name'		=> __( 'Call to Action', 'abelplugin' ),
					'class'		=> 'Abel\Columns\CtaColumn',
					'template'	=> $base.'Templates/CtaColumn.php'
				);

				$types['button-repeater'] = array(
					'name'		=> __( 'Button lijst', 'abelplugin' ),
					'class'		=> 'Abel\Columns\ButtonRepeaterColumn',
					'template'	=> $base.'Templates/ButtonRepeaterColumn.php'
				);

				$types['gallery'] = array(
					'name'		=> __( 'Fotogallerij', 'abelplugin' ),
					'class'		=> 'Abel\Columns\GalleryColumn',
					'template'	=> $base.'Templates/GalleryColumn.php'
				);

				$types['map'] = array(
					'name'		=> __( 'Google Maps', 'abelplugin' ),
					'class'		=> 'Abel\Columns\MapColumn',
					'template'	=> $base.'Templates/MapColumn.php'
				);

				$types['abelimage'] = array(
					'name'		=> __( 'Afbeelding', 'abelplugin' ),
					'class'		=> 'Abel\Columns\ImageColumn',
					'template'	=> $base.'Templates/Image.php'
				);

				$types['productfeed'] = array(
					'name'		=> __( 'Externe Producten', 'abelplugin' ),
					'class'		=> 'Abel\Columns\ProductFeedColumn',
					'template'	=> $base.'Templates/ProductFeedColumn.php'
				);

				$types['html'] = array(
					'name'		=> __( 'HTML Code', 'abelplugin' ),
					'class'		=> 'Abel\Columns\HtmlColumn',
					'template'	=> $base.'Templates/HtmlColumn.php'
				);

				$types['filter'] = array(
					'name'		=> __( 'Filter', 'abelplugin' ),
					'class'		=> 'Abel\Columns\FilterColumn',
					'template' 	=> $base.'Templates/FilterColumn.php'
                );
                
                $types['abel360'] = array(
                    'name'      => __( '360° Afbeelding', 'abelplugin'),
                    'class'     => 'Abel\Columns\ThreeSixtyColumn',
                    'template'  => $base.'Templates/ThreeSixtyColumn.php'
                );


				if( class_exists( '\\GFAPI' ) ){

					$types['form'] = array(
						'name' => __( 'Formulier', 'abelplugin' ),
						'class' => 'Abel\Columns\FormColumn',
						'template' => $base.'Templates/FormColumn.php'
					);
				}

				return $types;

			});

			//custom field-type:
			add_filter( 'cuisine_field_types', function( $types ){

				$types['icon'] = array(
					'name'		=> 'Icon',
					'class'		=> 'Abel\Fields\IconField'
				);

				$types['button'] = array(
					'name'		=> 'Button',
					'class'		=> 'Abel\Fields\ButtonField'
				);

				return $types;
			});			
		}

		/**
		 * Section alterations
		 * 
		 * @return void
		 */
		public function sections()
		{
			//only allow 4 basic columns in the default section:
			add_filter( 'chef_sections_default_allowed_columns', function( $allowed, $column, $section ){

				$generated = SectionHelper::generated();
				if( 
					is_null( $section->container_id ) &&  
					( $section->name == '' || !in_array( $section->name, $generated ) )
				){
					$allowed = [ 'content', 'video', 'abelimage', 'html', 'form', 'abel360' ];				
				}

				if( 
					User::hasRole( 'administrator' ) && 
					in_array( 'handpickedcollection', $allowed )
				){
					$allowed[] = 'collection';
				}


				return $allowed;

			}, 100, 3);	

			//only allow extra views in the content sections
			add_filter( 'chef_sections_default_allowed_views', function( $allowed, $section ){

				$generated = SectionHelper::generated();

				if( 
					is_null( $section->container_id ) &&  
					( $section->name == '' || !in_array( $section->name, $generated ) )
				){
					$allowed = [ 
						'fullwidth', 
						'half-half', 
						'sidebar-left', 
						'sidebar-right', 
						'three-columns', 
						'three-columns-left', 
						'three-columns-middle', 
						'three-columns-right'
					];
				}

				return $allowed;

			}, 100, 2);


			//change default template locations of columns:
			add_filter( 'chef_sections_located_template', function( $located, $template ){

				if( $template->getType() == 'column' )
					$located = locate_template(['partials/columns/'.$template->getObject()->type.'.php' ]);

				return $located;

			}, 100, 2);


			//set new view options:
			add_filter( 'chef_sections_section_types', function( $views ){

				$newViews = [
					'three-columns-left' => 3,
					'three-columns-middle' => 3,
					'three-columns-right' => 3
				];

				return array_merge( $views, $newViews );
            });
            
            //force og:url to appear:
            add_filter('wpseo_opengraph_url' , '__return_true' );
            add_filter( 'wpseo_opengraph_url', function( $url ){
                return Url::current();
            });

		}

		/**
		 * Collection hooks
		 * 
		 * @return void
		 */
		public function collection()
		{
			
			//custom fields in collection:
			add_filter( 'chef_sections_collection_column_fields', function( $fields ){

				foreach( $fields as $key => $field ){

					//hide title field:
					if( $field->type == 'title' )
						$fields[ $key ]->properties['wrapper-class'] = ['hidden'];
				
					//add menu-order option
					if( $field->name == 'orderby' )
						$fields[ $key ]->properties['options']['menu_order'] = 'CMS Volgorde';
				}

				return $fields;
			});

			//add products as a possible post-type:
			add_filter( 'chef_sections_collection_post_types', function( $postTypes ){

				$products = Settings::get( 'products' );
				if ( $products == 'manual' ) {
					$postTypes['product'] = __( 'Producten', 'abelplugin' );
				}

				return $postTypes;
			});
			
			//custom side-fields:
			add_filter( 'chef_sections_collection_side_fields', function( $fields, $column ){
				
				$view = array(
					'grid' 		=> __( 'Grid', 'abelplugin' ),
					'overview'	=> __( 'Overview', 'abelplugin' ),
					'slider'	=> __( 'Slider', 'abelplugin' )
				);

				$nav = array(
					'none'			=> __( 'None', 'abelplugin' ),
					'pagination'	=> __( 'Pagination', 'abelplugin' )
				);

				$nav_arrows = array(
					'none'			=> __( 'None', 'abelplugin' ),
					'arrows'	=> __( 'Arrows', 'abelplugin' )
				);

				$fields = array(

					'view'	=> Field::radio(
						'view',
						__( 'View', 'abelplugin' ),
						$view,
						array(
							'defaultValue' => $column->getField( 'view', 'grid' ),
							'subname' => $column->fullId.'view',
							'wrapper-class' => ['viewselect']
						)
					),

					'slider-time' => Field::number(
						'slider-time',
						__( 'Auto-slide time', 'abelplugin' ),
						array(
							'defaultValue' => $column->getField( 'slider-time', 0 ),
							'wrapper-class' => ['slidertime']
						)
					),

					'nav'	=> Field::radio(
						'nav',
						__( 'Navigation', 'abelplugin' ),
						$nav,
						array(
							'defaultValue'	=> $column->getField( 'nav', 'none' ),
							'subname' => $column->fullId.'nav'
						)
					),

					'nav_arrows' => Field::radio(
						'nav_arrows',
						__( 'Navigation arrows', 'abelplugin' ),
						$nav_arrows,
						array(
							'defaultValue' => $column->getField( 'nav_arrows', 'none' ),
							'subname' => $column->fullId.'nav_arrows'
						)
					),
					'display_link' => Field::checkbox (
						'display_link',
						__( 'Toon link', 'abelplugin' ),
						array(
							'defaultValue' => $column->getField( 'display_link', true ),
							'subname' => $column->fullId.'display_link'
						)
					)

				);

				return $fields;

			},100,2);


			//make collection queries respond to filters:
			add_filter( 'chef_sections_collection_query', function( $query, $column ){

				$altered = false;

				if( isset( $_POST['filter_on'] ) && isset( $_POST['filter_val'] ) ){
					$altered = true;

					$taxQuery = array(
						'taxonomy'  => $_POST['filter_on'],
						'field'		=> 'term_id',
						'terms'		=> $_POST['filter_val']
					);


					if( !isset( $query['tax_query'] ) )
						$query['tax_query'] = array();

					array_push( $query['tax_query'], $taxQuery );

				}


				if( isset( $_POST['search'] ) ){
					$altered = true;
					$query['s'] = $_POST['search'];
				}

				if( $altered ){
					$query[ 'posts_per_page' ] = '-1';
					$query[ 'paged' ] = '1';
				}

				if( $query['orderby'] == 'rand' ){
					unset( $query['paged'] );
				}else if( $query['orderby'] == 'menu_order' ){
					$query['order'] = 'DESC';
				}

				return $query;

			}, 100, 2 );


			//add custom breadcrumbs for collection-singles:
			add_filter( 'wpseo_breadcrumb_links', function( $links ){

				$pts = [
					'post' => 'news',
					'reference' => 'references',
					'service' => 'services'
				];

				if( 
					is_single() && 
					!is_search() &&
					!is_tax() &&
					in_array( get_post_type(), array_keys( $pts ) ) 
				){

					$id = Settings::get( $pts[ get_post_type() ].'-page' );
					if( $id !== 'none' ){

						$links[2] = $links[1];
						$links[1]['id'] = $id;

					}
				}

				return $links;
			});
		}
        
        /**
         * Various multisite fixes:
         *
         * @return void
         */
        public function multisite()
        {
            add_filter('chef_media_gallery_preview_url', function( $preview ){

                if( is_multisite() ){
                        
                    $path = str_replace( get_site_url().'wp-content/', '', $preview );
                    if( !file_exists( Url::path( 'content', $path ) ) ){

                        $siteUpload = 'wp-content/uploads/sites/'.get_current_blog_id();

                        if( strpos( $preview, 'sites/'.get_current_blog_id() ) !== false ){
                            return $preview;
                        }else{
                            $preview = str_replace( 'wp-content/uploads', $siteUpload, $preview );
                        }
                    }
                }

                return $preview;
                
            });
        }

        /**
         * Deprecated functions and what-not
         *
         * @return String
         */
        public function deprecated()
        {
            //brands-alt got changed to partners
            add_filter( 'abel_object_name', function( $name, $object ){

                if( $name == 'brands-alt' )
                    return 'partners';

                return $name;

            }, 100, 2 );
        }


		/**
		 * Alter the default stijlbreuk url to the new remote url
		 * 
		 * @return String
		 */
		public function deployment()
		{
			
			add_filter( 'chef_deploy_default_replace_old', function( $replace ){
				$replace[2] = '//dev.stijlbreuk.nu:3002';
				return $replace;
			});

			add_filter( 'chef_deploy_default_replace_new', function( $replace, $profile ){
				$replace[2] = str_replace( ['https:', 'http:'], '', $profile->getUrl( false ) );
				return $replace;
			}, 20, 2 );
        }
        
        /**
         * Bugfixes for the post_types bug in sections.
         *
         * @return void
         */
        public function bugfixes()
        {
            //change field name:
            add_filter( 'chef_sections_collection_column_fields', function( $fields ){
                foreach( $fields as $key => $field ){
                    if( $field->name == 'post_type' ){
                        $fields[ $key ]->name = 'collection_post_type';
                    }

                    if( !$field || is_null( $field ) ){
                        unset( $fields[ $key ] );
                    }
                }
                return array_values( $fields );
            });

            //filter field value:
            add_filter( 'chef_sections_save_column_properties', function( $props ){
                if( isset( $props['collection_post_type'] ) ){
                    $props['post_type'] = $props['collection_post_type'];
                    unset( $props['collection_post_type'] );
                }
                return $props;
            });
        }
	}

	\Abel\Frontend\EventListeners::getInstance();
