<?php

	namespace Abel\Front;

	use WP_Query;
	use Abel\Helpers\Icon;
	use Abel\Front\Settings;
	use Cuisine\Wrappers\Template;
	use Abel\Helpers\FooterLayouts as Layouts;

	class Footer{


		/**
		 * String containing the layout
		 * 
		 * @var String
		 */
		protected $layout;


		/**
		 * The current footer
		 * 
		 * @var String
		 */
		protected $footer;

		/**
		 * The current zone
		 * 
		 * @var Array
		 */
		protected $zone;


		/**
		 * The current template
		 * 
		 * @var TemplateFinder object
		 */
		protected $template;


		/**
		 * Get the content from a zone in a menu
		 * 
		 * @param  String $footer
		 * @param  Int $zone
		 * 
		 * @return Abel\Front\Footer
		 */
		public function content( $footer, $zone )
		{
			$this->menu = $footer;
			$this->zone	= $this->getZone( $zone );
			
			if( !is_null( $this->zone ) )
				$this->layout = $this->zone['layout'];

			return $this;
		}
	
		/**
		 * Returns the zone args
		 * 
		 * @return Array
		 */
		public function getZone( $_zone )
		{
			$zone = Settings::get( 'footer_zone_'.$_zone );
			if( !empty( $zone ) ){
				$zone = array_values( $zone );
				return $zone[0];
			}

			return null;
		}

		/**
		 * Returns the template with the right variables
		 * 
		 * @return String
		 */
		public function get()
		{
			if( is_null( $this->zone ) )
				return '';

			//else, return the template
			ob_start();
			$this->display();
			return ob_get_clean();
		}

		/**
		 * Display the template with the right variables
		 * 
		 * @return void
		 */
		public function display()
		{
			$args = $this->getArgs();
			Template::find( 
				'partials/footers/content/'.$this->layout, 
				null
			)->display( $args );		
		}


		/**
		 * Returns arguments for a footer layout
		 * 
		 * @return Array
		 */
		public function getArgs()
		{
			$response = [];
			$fields = Layouts::getFields( $this->layout );

			$response['menu'] = $this->menu;
			$response['layout'] = $this->layout;		

			foreach( $fields as $field ){

                $value = '';

                if( isset( $this->zone[ $field->name] ) )
				    $value = $this->zone[ $field->name ];
                
                    if( $field->type == 'icon' && $this->zone[ $field->name ] !== 'none' )
					$value = Icon::get( $this->zone[ $field->name ] ); 

				if( $field->type == 'title' ){
					if( isset( $value['text'] ) && $value['text'] != '' && !is_null( $value['text'] ) ){
						$value = "<{$value['type']}>{$value['text']}</{$value['type']}>";
					}else{
						$value = '';
					}
				}

				$response[ $field->name ] = $value;
			}

			if( !isset( $response[ 'title'] ) )
				$response['title'] = '';

			if( $this->layout == 'list' ){
				$response['query'] = $this->getQuery();
			}

			return $response; 
		}


		/**
		 * Return a clean-cut WP_Query
		 * 
		 * @return WP_Query
		 */
		public function getQuery()
		{

			$args = [
				'post_type' => $this->zone['post_type'],
				'orderby' => $this->zone['orderby'],
				'posts_per_page' => $this->zone['posts_per_page']
			];

			$query = new WP_Query( $args );
			return $query;	
		}


		/**
		 * Returns the footer section
		 * 
		 * @return Html
		 */
		public function section()
		{
			$section = Settings::get( 'footersection' );
			if( !is_null( $section ) && $section != 'none' ){
				echo get_section( $section );
			}
		}

	}