<?php

	namespace Abel\Front;

	use Cuisine\Utilities\Url;
	use Cuisine\Wrappers\Script;
	use Cuisine\Wrappers\Sass;
	use Abel\Wrappers\StaticInstance;

	class Assets extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->enqueues();

		}

		/**
		 * Enqueue scripts & Styles
		 * 
		 * @return void
		 */
		private function enqueues(){

            //Gravity forms custom translation for the datepicker,
            add_action( 'gform_enqueue_scripts', function(){
                
                if( wp_script_is( 'gform_datepicker_init' ) ){
                    $url = Url::plugin( 'abel/Assets/js/datepicker.nl.js' );
                    wp_enqueue_script( 'datepicker-regional', $url, [ 'gform_datepicker_init' ], false, true );
                }
                
            }, 11 );
           
		}



	}

	if( !is_admin() )
		\Abel\Front\Assets::getInstance();
