<?php

	namespace Abel\Front;

	use \ChefSections\Collections\SectionCollection;
	use ChefSections\Wrappers\AjaxInstance;
	use ChefSections\Wrappers\Column;
	use ChefSections\Wrappers\Template;
	use stdClass;

	class Ajax extends AjaxInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->listen();

		}

		/**
		 * All ajax events
		 * 
		 * @return string, echoed
		 */
		private function listen(){

			//creating a section:
			add_action( 'wp_ajax_filter', array( &$this, 'handleFilter' ) );
			add_action( 'wp_ajax_nopriv_filter', array( &$this, 'handleFilter' ) );

		}


		public function handleFilter(){

			$this->setPostGlobal();

			$template = '';
			$postId = $_POST['post_id'];
			$sectionId = $_POST['section'];
			$sections = new SectionCollection( $postId );

			//if the collection isn't empty:
			if( !$sections->isEmpty() ){

				//get the section, default the first if section Id turns out to be null
				$section = ( is_null( $sectionId ) ? $sections->first() : $sections->get( $sectionId ) );

				if( !is_null( $section ) ){

					//get the collection
					$column = $section->getColumn( 'collection' );
					$column->setPage( $_POST['page'] );
			
					//then, check if there are posts:
					$q = $column->getQuery();

					if( !$q->have_posts() ){
				
						$template = 'message';

					}else{
				

						ob_start();

							$section->beforeTemplate();
								Template::section( $section )->display();
							$section->afterTemplate();

						$template = ob_get_clean();
					}
				}
			}

			echo $template;
			die();
		}

	}


	\Abel\Front\Ajax::getInstance();
