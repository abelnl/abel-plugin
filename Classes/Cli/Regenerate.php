<?php
	namespace Abel\Cli;

	use WP_CLI;
	use WP_CLI_Command;
	use Abel\Wrappers\Generators;


	class RegenerateCommands extends WP_CLI_Command{
	
		/**
		 * Regenerates section templates
		 * 
		 * @param  array $args
		 * @param  array $assoc_args
		 * 
		 * @return WP_CLI::success message
		 */
        public function sections( $args, $assoc_args )
        {

			if( isset( $assoc_args['force'] ) )
				delete_option( 'abel_generators' );
			
			Generators::regenerateAll();
    		
    		// Print a success message
    		WP_CLI::success( "All sections regenerated" );

        }
        

        public function section( $args, $assoc_args )
        {
            $name = $args[0];
            $response = Generators::regenerate( $name );
            if( $response ){
                WP_CLI::success( "Section {$name} regenerated." );
            }else{
                WP_CLI::error( "Section {$name} couldn't be found" );
            }
        }
		
	
	
	}


	WP_CLI::add_command( 'regenerate', 'Abel\Cli\RegenerateCommands' );

