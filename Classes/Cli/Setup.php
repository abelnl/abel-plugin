<?php
	namespace Abel\Cli;

	use WP_CLI;
    use WP_CLI_Command;
    use Cuisine\Utilities\Url;
    use Abel\Broadcasting\MigrationHandler;


	class SetupCommands extends WP_CLI_Command{
	
		/**
		 * Setup a default site
		 * 
		 * @param  array $args
		 * @param  array $assoc_args
		 * 
		 * @return WP_CLI::success message
		 */
        public function setup( $args, $assoc_args )
        {
            //set the smtp mailer:
            $smtpOptions = file_get_contents( Url::path( 'plugin', 'abel/Templates/defaults/smtp.txt' ) );
            update_option( 'wp_email_smtp_option_name', unserialize( $smtpOptions ) );

            //set adminimize:
            $adminimize = file_get_contents( Url::path( 'plugin', 'abel/Templates/defaults/adminimize.txt' ) );
            update_option( 'mw_adminimize', $adminimize );

            WP_CLI::success( "Base setup completed" );
        }
        
        /**
         * Regenerate sections
         *
         * @param Array $args
         * @param Array $assoc_args
         * @return void
         */
        public function section( $args, $assoc_args )
        {
            $name = $args[0];
            $response = Generators::regenerate( $name );
            if( $response ){
                WP_CLI::success( "Section {$name} regenerated." );
            }else{
                WP_CLI::error( "Section {$name} couldn't be found" );
            }
        }
	
	}


	WP_CLI::add_command( 'abel', 'Abel\Cli\SetupCommands' );

