<?php

	namespace Abel\Fields;

	use Abel\Helpers\Icon;
	use Cuisine\Fields\SelectField;

	class IconField extends SelectField{

		    /**
	     * Method to override to define the input type
	     * that handles the value.
	     *
	     * @return void
	     */
	    protected function fieldType(){
	        $this->type = 'icon';
	    }


	 	/**
	     * Handle the field HTML code for metabox output,
	     * also enqueue the image chosen script
	     *
	     * @return string
	     */
	    public function render(){

	        $class = $this->getWrapperClass();
	        $class .= ' icon-dropdown';

	        echo '<div class="'.$class.'">';

	            echo $this->getLabel();
	            echo $this->build();

	        echo '</div>';
	    }

		/**
		 * Get choices
		 *
		 * @return Array / void
		 */
		public function getChoices(){
		  
			if( $this->getProperty( 'category' ) == 'ui' ){
		  		$icons = Icon::getUiIcons();
			}else{
				$icons = Icon::getAll();
			}
		  
		  	return array_merge( ['none' => [ 'label' => __( 'Geen', 'abelplugin'), 'url' => '', 'ui' => true ] ], $icons );
		}


		 /**
	     * Return html for an option
	     * 
	     * @param  array/string $choice
	     * @return string HTML
	     */
	    public function buildOption( $choice ){
	    	
	    	//set choice variables:
	        $value = $choice['key'];
	        $label = $choice['label']['label'];
	        $class = Icon::getClass( $choice['label'] );


	        $html = '<option value="'.$value.'"';

	            $html .= $this->selected( $value );
	            $html .= ' data-img-src="'.$choice['label']['url'].'"';
	            $html .= ' data-class="svg_img '.$class.'"';

	        $html .= '>'.$label.'</option>';

	        return $html;
	    }
	}