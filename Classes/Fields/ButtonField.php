<?php

	namespace Abel\Fields;

	use Abel\Helpers\Icon;
	use Abel\Helpers\Button;
	use Cuisine\Wrappers\User;
	use Cuisine\Wrappers\Field;
	use Cuisine\Fields\DefaultField;

	class ButtonField extends DefaultField{

		/**
	     * Method to override to define the input type
	     * that handles the value.
	     *
	     * @return void
	     */
	    protected function fieldType(){
	        $this->type = 'button';
	    }

	     /**
         * Build the html
         *
         * @return String;
         */
        public function build(){

            $value = $this->getValue();
			if ( !$value )
				$value = $this->getDefault();
            ob_start();

            echo '<span class="button-wrapper">';

            	//link
            	Field::text( 
            		$this->name.'[link]', 
            		__( 'Link', 'abelplugin' ), 
            		[
            			'label' => false,
            			'defaultValue' => stripcslashes( $value['link'] ),
              			'classes' => ['multi'],
            			'placeholder' => __( 'Link', 'abelplugin' )
            		]
            	)->render();

            	//text
            	Field::text( 
            		$this->name.'[text]', 
            		__( 'Tekst', 'abelplugin' ),
            		[
               			'label' => false,
	           			'defaultValue' => $value['text'],
               			'classes' => ['multi'],
            			'placeholder' => __( 'Tekst', 'abelplugin' )

            		]
            	)->render();


            	echo '<span class="button-settings">';

                	echo '<span class="dashicons dashicons-admin-generic"></span>';                
                	echo '<div class="type-sub-menu">';

	                	Field::checkbox( 
	                		$this->name.'[target]', 
	                		__( 'Openen in nieuw venster?' ), 
	                		[ 
	                			'defaultValue' => $value['target'],
								'classes' => ['multi']
							]
	                	)->render();

	                	Field::icon( 
	                		$this->name.'[icon]',
	                		__( 'Icoon', 'abelplugin' ),
	                		[
	                			'defaultValue' => $value['icon'],
	                			'classes' => ['multi'],
	                			'category' => 'ui',
	                		]
	                	)->render();

	                	Field::select(
	                		$this->name.'[class]',
	                		__( 'Class', 'abelplugin' ),
	                		Button::getClasses(),
	                		[
	                			'defaultValue' => $value['class'],
	                			'classes' => [ 'chosen', 'multi' ]
	                		]
	                	)->render();

	                	Field::select(
	                		$this->name.'[iconClass]',
	                		__( 'Icoon class', 'abelplugin' ),
	                		Button::getIconClasses(),
	                		[
	                			'defaultValue' => $value['iconClass'],
	                			'classes' => [ 'chosen', 'multi' ]
	                		]
	                	)->render();

	                echo '</div>';
            	echo '</span>';
            echo '</span>';



            return ob_get_clean();
        }




	    /**
	     * Get the default value html
	     * 
	     * @return String
	     */
	    public function getDefault(){

	        if( $this->properties['defaultValue' ] )
	            $prop = $this->properties['defaultValue'];
			else 
				$prop = null;
	 		

	 		$default = [

	 			'link' => '',
	 			'text' => '',
	 			'target' => 'false',
	 			'icon' => 'icon_ui_arrow-right',
	 			'class' => 'v_transparent_body',
	 			'iconClass' => 'v_has-icon-right'
	 		];
	    
	 		return wp_parse_args( $prop, $default );
	    }



    /**
     * Returns the wrapper class
     * 
     * @return String
     */
    public function getWrapperClass()
    {
        $classes = [];
        $classes[] = 'field-wrapper';
        $classes[] = $this->type.'-field';
        $rightRole = false;

        //check if thie right user-roles are present:
        if( !empty( $this->userRoles ) && is_admin() ){

            foreach( $this->userRoles as $role ){
                if( User::hasRole( $role ) ){
                    $rightRole = true;
                    break;
                }
            }
            
            if( !$rightRole )
                $classes[] = 'user-no-valid-role';
        }


        if( $this->properties['label'] )
            $classes[] = ' label-'.sanitize_title( $this->properties['label'] );

        if( $this->properties['wrapper-class'] && is_array( $this->properties['wrapper-class'] ) )
            $classes = array_merge( $classes, $this->properties['wrapper-class'] );

        $output = implode( ' ', $classes );
        return $output;
    }
	}