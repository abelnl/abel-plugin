<?php

	namespace Abel\Helpers;


	class Animations{


		public static function get()
		{
			$animations = [
				'none' 			=> __('geen', 'abelplugin'),
				'default'		=> 'default',
				'transform-in'	=> 'transform-in',
				'scale'			=> 'scale',
				'scale-slow'	=> 'scale-slow'
			];	

			$animations = apply_filters( 'abel_animations', $animations );
			return $animations;	
		}


		/**
		 * Returns all color slugs
		 * 
		 * @return Array
		 */
		public static function getValues()
		{
			return array_keys( static::get() );
		}

	}