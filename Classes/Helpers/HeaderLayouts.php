<?php

	namespace Abel\Helpers;

	use Cuisine\Utilities\Sort;
	use Cuisine\Wrappers\Field;

	class HeaderLayouts{

		/**
		 * Returns all header layouts
		 * 
		 * @return Array
		 */
		public static function getAll()
		{			
			
			return [

				'button' => [
					'layout' => 'button',
					'label' => __( 'Button', 'abelplugin' ),
					'fields' => [
						Field::button( 'button', __( 'Klik hier','abelplugin' ) )	
					]
				],

				'cta' => [
					'layout' => 'cta',
					'label' => __( 'Call to action', 'abelplugin' ),
					'fields' => [
						Field::text( 'text', __( 'Tekst', 'abelplugin' ) ),
						Field::text( 'btnText', __( 'Tekst op button', 'abelplugin' ) ),
						Field::text( 'link', __( 'Link', 'abelplugin' ) ),
						Field::icon( 'icon', __( 'Icoon', 'abelplugin' ) )
					]
				],

				'menu' => [
					'layout' => 'menu',
					'label' => __( 'Menu', 'abelplugin' ),
					'fields' => [
						Field::select( 
							'menuId', 
							__( 'Menu', 'abelplugin' ), 
							static::getMenus()
						),
						Field::select(
							'type',
							__( 'Type', 'abelplugin' ),
							[
								'_primary' => __( 'Primair', 'abelplugin' ),
								'_secondary' => __ ('Secondair', 'abelplugin' ),
								'_other' => __( 'Anders', 'abelplugin' )
							]
						),
						Field::radio(
							'align',
							__( 'Dropdown uitlijning', 'abelplugin' ),
							[
								'left' => __( 'Links', 'abelplugin'),
								'right' => __( 'Rechts', 'abelplugin' )
							]
						)
					]
				],

				'search' => [
					'layout' => 'search',
					'label' => __( 'Zoeken', 'abelplugin' ),
					'fields' => [
						Field::text( 'label', 'Label' ),
						Field::text( 'placeholder', 'Placeholder' ),
						Field::text( 'btnText', __( 'Button Tekst', 'abelplugin' ) ),
						Field::icon( 'icon', __( 'Icoon', 'abelplugin' ) ),
						Field::checkbox( 'form', __( 'Direct zoekveld laten zien', 'abelplugin' ) )
					]
				],

				'socials' => [
					'layout' => 'socials',
					'label' => __( 'Socials', 'abelplugin' ),
					'fields' => [
						Field::select( 
							'iconSize', 
							__( 'Icoongrootte', 'abelplugin' ),
							[ 
								'small' => __( 'Klein', 'abelplugin' ),
								'middle' => __( 'Gemiddeld', 'abelplugin' ),
								'big' => __( 'Groot', 'abelplugin' ) 
							]
						)
					]
				],

				'divider' => [
					'layout' => 'divider',
					'label' => __( 'Divider', 'abelplugin' ),
					'fields' => []
				]
			];	
		}


		/**
		 * Returns an array with all menus
		 * 
		 * @return Array
		 */
		public static function getMenus()
		{
			$menus = wp_get_nav_menus();
			$menus = array_combine( 
				Sort::pluck( $menus, 'term_id' ),
				Sort::pluck( $menus, 'name' )
			);

			return $menus;

		}


		/**
		 * Returns the header layout specified by it's layout-slug
		 * 
		 * @param  String $slug
		 * 
		 * @return Array
		 */
		public static function get( $slug )
		{
			$layouts = static::getAll();
			if( isset( $layouts[ $slug ] ) )
				return $layouts[ $slug ];

			return null;
		}

		/**
		 * Get the fields for a specific layout
		 * 
		 * @param  String $slug
		 * 
		 * @return Array
		 */
		public static function getFields( $slug )
		{
			$layout = static::get( $slug );
			if( !is_null( $layout ) )
				return $layout['fields'];

			return [];
		}


		/**
		 * Return the mobile menu ID
		 * 
		 * @return Int
		 */
		public static function getMobileMenuId()
		{
			return '4';
		}
	}