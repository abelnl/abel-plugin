<?php

	namespace Abel\Helpers;

	class Button{


		/**
		 * Returns the class of a button
		 * 
		 * @param  Array $button
		 * 
		 * @return String
		 */
		public static function getClass( $button )
		{
			$classes[] = 'button';

			if( $button['class'] != 'none' )
				$classes[] = $button['class'];

			if( $button['iconClass'] != 'none' );
				$classes[] = $button['iconClass'];

			return implode( ' ', $classes );		
		}

		/**
		 * Return button classes
		 * 
		 * @return Array
		 */
		public static function getClasses()
		{
			$classes = [
				'none' => __( 'Geen', 'abelplugin' ),
				'v_brand_primary' => __( 'Primair', 'abelplugin' ),
				'v_brand_secondary' => __( 'Secondair', 'abelplugin' ),
				'v_brand_tertiary' => __( 'Tertiar', 'abelplugin' ),
				'v_transparent' => __( 'Transparant', 'abelplugin' ),
				'v_ghost' => __( 'Ghost', 'abelplugin' ),
				'v_ghost_brand_primary' => __( 'Ghost primair', 'abelplugin' ),
				'v_ghost_brand_secondary' => __( 'Ghost secondair', 'abelplugin' ),
				'v_transparent_body' => __( 'Transparant body', 'abelplugin' ),
				'v_brand_white' => __( 'Wit', 'abelplugin' ),
				'v_brand_black' => __( 'Zwart', 'abelplugin' )
			];

			$classes = apply_filters( 'abel_button_classes', $classes );

			return $classes;
		}

		/**
		 * Return button icon classes
		 * 
		 * @return Array
		 */
		public static function getIconClasses()
		{
			$classes = [
				'none' => __( 'Geen', 'abelplugin' ),
				'v_has-icon-right' => __( 'Icoon rechts', 'abelplugin' ),
				'v_has-icon-left' => __( 'Icoon links', 'abelplugin' )
			];

			$classes = apply_filters( 'abel_button_icon_classes', $classes );

			return $classes;
		}

	}