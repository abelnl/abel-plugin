<?php

	namespace Abel\Helpers;

	class Dividers{


		/**
		 * Returns all possible deviders
		 * 
		 * @return Array
		 */
		public static function get()
		{
			$deviders = [
				'none' => 'None',
				'angled' => 'Angled',
				'bottom-angled' => 'Bottom angled',
				'bottom-butt-cheeks' => 'Bottom butt cheeks',
				'bottom-dotted' => 'Bottom dotted',
				'bottom-spiked-small' => 'Bottom spiked small',
				'bottom-spiked' => 'Bottom spiked',
				'butt-cheeks' => 'Butt cheeks',
				'dotted' => 'Dotted',
				'spiked-small' => 'Spiked small',
				'top-angled' => 'Top angled',
				'top-butt-cheeks' => 'Top butt cheeks',
				'top-curved-cheeks' => 'Top curved cheeks',
				'top-dotted' => 'Top dotted',
				'top-semi-transparent' => 'Top semi transparent',
				'top-smile' => 'Top smile',
				'top-spiked-small' => 'Top spiked small',
				'top-spiked' => 'Top spiked'
			];

			$deviders = apply_filters( 'abel_deviders', $deviders );
			return $deviders;
	
		}
	}