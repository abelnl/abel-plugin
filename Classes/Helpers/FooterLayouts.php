<?php

	namespace Abel\Helpers;

	use Cuisine\Utilities\Sort;
	use Cuisine\Wrappers\Field;

	class FooterLayouts{

		/**
		 * Returns all footer layouts
		 * 
		 * @return Array
		 */
		public static function getAll()
		{			
			
			return [

				'button' => [
					'layout' => 'button',
					'label' => __( 'Button', 'abelplugin' ),
					'fields' => [
						Field::title( 'title', __( 'Titel', 'abelplugin' ) ),
						Field::button( 'button', __( 'Klik hier','abelplugin' ) )	
					]
				],

				'textblock' => [
					'layout' => 'textblock',
					'label' => __( 'Tekst blok', 'abelplugin' ),
					'fields' => [
						Field::title( 'title', __( 'Titel', 'abelplugin' ) ),
						Field::editor( 
							'textarea', 
							__( 'Tekstveld', 'abelplugin' )
						)
					]
				],

				'list' => [
					'layout' => 'list',
					'label' => __( 'Lijst met posts', 'abelplugin' ),
					'fields' => [
						Field::title( 'title', __( 'Titel', 'abelplugin' ) ),
						Field::select( 
							'post_type', 
							__( 'Berichttype', 'abelplugin' ),
							static::getPostTypes()
						),
						Field::number(
							'posts_per_page',
							__( 'Aantal', 'abelplugin' )
						),
						Field::select(
							'orderby',
							__( 'Sorteer op', 'abelplugin' ),
							[
								'date' => __( 'Datum', 'abelplugin' ),
								'menu_order' => __( 'CMS Volgorde', 'abelplugin' ),
								'title' => __( 'Alfabetisch', 'abelplugin' ),
								'random' => __( 'Willekeurig', 'abelplugin')
							]
						)
					]
				],

				'menu' => [
					'layout' => 'menu',
					'label' => __( 'Menu', 'abelplugin' ),
					'fields' => [
						Field::title( 'title', __( 'Titel', 'abelplugin' ) ),
						Field::select( 
							'menuId', 
							__( 'Menu', 'abelplugin' ), 
							static::getMenus()
						)
					]
				],

				'html' => [
					'layout' => 'html',
					'label' => 'iFrame',
					'fields' => [
						Field::title( 'title', __( 'Titel', 'abelplugin' ) ),
						Field::textarea(
							'html',
							__( 'Broncode', 'abelplugin' ),
							[
								'rows' => 15,
								'stripSlashes' => true
							]
						)
					]
				],

				'socials' => [
					'layout' => 'socials',
					'label' => __( 'Socials', 'abelplugin' ),
					'fields' => [
						Field::title( 'title', __( 'Titel', 'abelplugin' ) ),
						Field::select( 
							'iconSize', 
							__( 'Icoongrootte', 'abelplugin' ),
							[ 
								'small' => __( 'Klein', 'abelplugin' ),
								'middle' => __( 'Gemiddeld', 'abelplugin' ),
								'big' => __( 'Groot', 'abelplugin' ) 
							]
						)
					]
				]
			];	
		}


		/**
		 * Returns an array with all menus
		 * 
		 * @return Array
		 */
		public static function getMenus()
		{
			$menus = wp_get_nav_menus();
			$menus = array_combine( 
				Sort::pluck( $menus, 'term_id' ),
				Sort::pluck( $menus, 'name' )
			);

			return $menus;

		}


		/**
		 * Returns the header layout specified by it's layout-slug
		 * 
		 * @param  String $slug
		 * 
		 * @return Array
		 */
		public static function get( $slug )
		{
			$layouts = static::getAll();
			if( isset( $layouts[ $slug ] ) )
				return $layouts[ $slug ];

			return null;
		}

		/**
		 * Get the fields for a specific layout
		 * 
		 * @param  String $slug
		 * 
		 * @return Array
		 */
		public static function getFields( $slug )
		{
			$layout = static::get( $slug );
			if( !is_null( $layout ) )
				return $layout['fields'];

			return [];
		}


		/**
		 * Get post types as key / value pairs
		 *
		 * @return array
		 */
		public static function getPostTypes()
		{
			
			$pts = get_post_types( array( 'public' => true ) );
			$arr = array();
			foreach( $pts as $post_type ){

				$obj = get_post_type_object( $post_type );
				$arr[$post_type] = $obj->labels->name;

			}

			unset( $arr['attachment'] );
			unset( $arr['form'] );
			unset( $arr['section-template'] );

			return $arr;

		}


		/**
		 * Return a WP Query
		 * 
		 * @return Query 
		 */
		public static function getQuery()
		{
				
		}
	}