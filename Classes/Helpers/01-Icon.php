<?php

	namespace Abel\Helpers;

	use SimpleXMLElement;
	use Abel\Front\Settings;
	use Cuisine\Utilities\Url;
	use Cuisine\Utilities\Sort;

	class Icon{

		/**
		 * Get an icon
		 * 
		 * @param  String $slug
		 * 
		 * @return String
		 */
		public static function get( $slug, $addSlashes = false )
		{
			// $svgUrl = 'http://dev.stijlbreuk.nu/abelenco/werving/content/themes/abelenco-werving/public/assets/symbols.svg#' . $icon ;
			
			$all = static::getAll();
			$url = $all[ $slug ]['url'];

			//ui icons are always fills
			$class = static::getClass( $all[ $slug ] );

	        $svg = '<svg role="img" class="icon-'.$class.'"><use xlink:href=[URL]></use></svg>';

	        if($addSlashes)
	            $svg = addslashes($svg);

	        $svg = str_replace('[URL]', $url , $svg);
	        
	        return $svg;
		}

		/**
		 * Return all svg icons
		 * 
		 * @return Array
		 */
		public static function getAll()
		{
			$set = Settings::get( 'icon_library', 'set1' );

			$base = Url::theme( 'public/assets/symbols.svg#' );
			$path = Url::path( 'theme', 'public/assets/symbols.svg' );

			if( $set ){
				$base = Url::theme('public/assets/symbols-'.$set.'.svg#' );
				$path = Url::path( 'theme', 'public/assets/symbols-'.$set.'.svg' );
			}

			$file = new SimpleXMLElement( file_get_contents( $path ) );
			$icons = [];

			foreach( $file->symbol as $symbol ){

				$id = (string)$symbol['id'];
				$label = str_replace( 'icon', '', $id );
				$label = ucwords( str_replace( ['-', '_'], ' ', $label ) );
				$isUi = ( ( strpos( $label, 'Ui' ) !== false ) ? true : false );

				$icons[ $id ] = [
					'label' => $label,
					'url' => $base.$id,
					'ui' => $isUi
				];
			}	

			return $icons;
		}


		/**
		 * Only return UI icons
		 * 
		 * @return Array
		 */
		public static function getUiIcons()
		{
			$all = static::getAll();
			$uiIcons = [];
			foreach( $all as $id => $icon ){

				if( $icon['ui'] )
					$uiIcons[ $id ] = $icon;
			}

			return $uiIcons;
		}


		/**
		 * Returns the right class for this icon
		 * 
		 * @return String 
		 */
		public static function getClass( $icon )
		{
			if( $icon['ui'] ){
				return 'fill';
			}

			$set = Settings::get( 'icon_library', 'set1' );
			$libraries = static::getLibraries();
			if( isset( $libraries[ $set ] ) )
				return $libraries[ $set ]['type'];

			return 'stroke';
		}



		/**
		 * Return the possible icon libraries
		 * 
		 * @return Array
		 */
		public static function getLibraries( $keyValue = false )
		{
			$libraries = [
				'set1' => [
					'name' => 'Set 1',
					'type' => 'fill'
				],

				'set2' => [
					'name' => 'Set 2',
					'type' => 'fill'
				],

				'set3' => [
					'name' => 'Set 3',
					'type' => 'stroke'
				],

				'set4' => [
					'name' => 'Set 4',
					'type' => 'fill'
				]
			];

			$libraries = apply_filters( 'abel_icon_libraries', $libraries );

			if( $keyValue )
				$libraries = array_combine( array_keys( $libraries), Sort::pluck( $libraries, 'name' ) );

			return $libraries;
		}

	}