<?php	

	namespace Abel\Helpers;

	use Cuisine\View\Loop;
	use Abel\Helpers\Image;
	use Abel\Front\Settings;

	class Brand extends Post{

		/**
		 * Returns a brand logo
		 * 
		 * @return String
		 */
		public static function logo( $alt = false )
		{
			$logo = ( $alt ? Loop::field( 'logo-alt', null ) : Loop::field( 'logo', null ) );
			$image = null;

			if( !is_null( $logo ) ){
                if( isset( $logo[0] ) ){
                    $logo = $logo[0];
                }

                $image = Image::url( $logo['img-id'] );
            }
			return $image;
		}

		/**
		 * Returns the brand logo ID
		 * 
		 * @return String
		 */
		public static function logoId()
		{
			$logo = Loop::field( 'logo', null );
			
			if( !is_null( $logo ) )
				return $logo['img-id'];

			return null;
		}

		/**
		 * Returns the logo class for the current post in loop:
		 * 
		 * @return String
		 */
		public static function logoClass()
		{			
			$size = Loop::field( 'size', false );
			if( $size && $size != 'default' )
				return ' v_'.$size.'-logo';
		}

		/**
		 * Returns the brand link
		 * 
		 * @return String
		 */
		public static function link()
		{
			return get_permalink();
		}

		/**
		 * Returns the brand link
		 * 
		 * @return String of false
		 */
		public static function externalLink()
		{
			$link = Loop::field( 'external-link', false );
			if( $link != '' && $link ) 
				return $link;

			return false;
		}
	}