<?php

	namespace Abel\Helpers;

	class Service extends Post{
		
		/**
		 * Returns the icon
		 * 
		 * @return String
		 */
		public static function icon()
		{
			$icon = get_post_meta( get_the_ID(), 'icon', true );

			if( $icon && $icon != 'none' )
				return Icon::get( $icon );

			return null;
		}


		/**
		 * Returns the description
		 * 
		 * @return String
		 */
		public static function description()
		{
			$sub = get_post_meta( get_the_ID(), 'description', true );
			if( $sub )
				return $sub;

			return '';
		}

		/**
		 * Returns the subtitle for this service
		 * 
		 * @return String
		 */
		public static function subtitle()
		{
			$sub = get_post_meta( get_the_ID(), 'subtitle', true );
			if( $sub )
				return $sub;

			return '';
		}

	}