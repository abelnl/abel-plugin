<?php

	namespace Abel\Helpers;


	class Colors{

		/**
		 * Returns all available colors
		 * 
		 * @return Array
		 */
		public static function get()
		{
			$colors = [
				'brand_primary' => 'Primair',
				'brand_primary-darker' => 'Primair (donker)',
				'brand_secondary' => 'Secondair',
				'brand_secondary_darker' => 'Secondair (donker)',
				'brand_tertiary' => 'Tertiar',
				'brand_tertiary-darker' => 'Tertiar (donkerder)',
				'brand_black' => 'Zwart',
				'brand_white' => 'Wit',
				'brand_darkest-gray' => 'Donkerste grijs',
				'brand_dark-gray' => 'Donker grijs',
				'brand_gray' => 'Grijs',
				'brand_light-gray' => 'Licht grijs',
				'brand_lightest-gray' => 'Lichtste grijs',
				'gradient_brand_primary' => 'Gradient primair',
				'gradient_brand_secondary' => 'Gradient secondair',
				'gradient_brand_tertiary' => 'Gradient tertiar'
			];	

			$colors = apply_filters( 'abel_colors', $colors );
			return $colors;	
		}


		/**
		 * Return devider colors
		 * 
		 * @return Array
		 */
		public static function getForDevider()
		{
			$colors = [
				'inherit' => 'Standaard', 
				'default_light' => 'Standaard licht',
				'default_dark' => 'Standaard donker',				
				'brand_primary' => 'Primair',
				'brand_primary-darker' => 'Primair (donker)',
				'brand_secondary' => 'Secondair',
				'brand_secondary-darker' => 'Secondair (donker)',
				'brand_tertiary' => 'Tertiar',
				'brand_tertiary-darker' => 'Tertiar (donker)',
				'brand_black' => 'Zwart',
				'brand_white' => 'Wit',
				'brand_light-gray' => 'Licht grijs',
				'brand_lightest-gray' => 'Lichtste grijs'
			];


			$colors = apply_filters( 'abel_text_colors', $colors );
			return $colors;
		}


		/**
		 * Returns all available opacities
		 * 
		 * @return Array
		 */
		public static function getOpacities() {
			$opacities = [ 
				'a_opacity-0'   => 0,
				'a_opacity-5'   => 5,
				'a_opacity-10'  => 10, 
				'a_opacity-20'  => 20,
				'a_opacity-30'  => 30,
				'a_opacity-40'  => 40,
				'a_opacity-50'  => 50,
				'a_opacity-60'  => 60,
				'a_opacity-70'  => 70,
				'a_opacity-80'  => 80,
				'a_opacity-90'  => 90,
				'a_opacity-100' => 100
			];
			$opacities = apply_filters('abel_opacities', $opacities );

			return $opacities;

		}

		/**
		 * Returns all available blend modes
		 * 
		 * @return Array
		 */
		public static function getBlendModes() {
			$blendModes = [ 
				'none' 				 => __('geen', 'abelplugin'),
				'a_blend-multiply'   => 'multiply',
				'a_blend-screen'     => 'screen',
				'a_blend-overlay'    => 'overlay',
				'a_blend-exclusion'  => 'exclusion',
				'a_blend-difference' => 'difference'
			];

			$blendModes = apply_filters('abel_blend_modes', $blendModes);

			return $blendModes;
		}


		/**
		 * Return text colors
		 * 
		 * @return Array
		 */
		public static function getForText()
		{
			$colors = [
				'inherit' => 'Standaard', 
				'default_light' => 'Standaard licht',
				'default_dark' => 'Standaard donker',				
				'brand_primary' => 'Primair',
				'brand_primary-darker' => 'Primair (donker)',
				'brand_secondary' => 'Secondair',
				'brand_secondary-darker' => 'Secondair (donker)',
				'brand_tertiary' => 'Tertiar',
				'brand_tertiary-darker' => 'Tertiar (donker)',
				'brand_black' => 'Zwart',
				'brand_white' => 'Wit',
				'brand_light-gray' => 'Licht grijs',
				'brand_lightest-gray' => 'Lichtste grijs'
			];


			$colors = apply_filters( 'abel_text_colors', $colors );
			return $colors;
		}

		/**
		 * Return fill colors
		 * 
		 * @return Array
		 */
		public static function getForFill()
		{
			$colors = [
				'inherit' => 'Standaard', 
				'default_light' => 'Standaard licht',
				'default_dark' => 'Standaard donker',				
				'brand_primary' => 'Primair',
				'brand_primary-darker' => 'Primair (donker)',
				'brand_secondary' => 'Secondair',
				'brand_secondary-darker' => 'Secondair (donker)',
				'brand_tertiary' => 'Tertiar',
				'brand_tertiary-darker' => 'Tertiar (donker)',
				'brand_black' => 'Zwart',
				'brand_white' => 'Wit',
				'brand_light-gray' => 'Licht grijs',
				'brand_lightest-gray' => 'Lichtste grijs'
			];


			$colors = apply_filters( 'abel_fill_colors', $colors );
			return $colors;
		}

		/**
		*Return grayscale options
		*
		*@return Array
		*/
		public static function getGrayscales(){
			$grayscales = [
				'none'				=> __('geen', 'abelplugin'),
				'a_grayscale-20'	=> '20',
				'a_grayscale-40'	=> '40',
				'a_grayscale-60'	=> '60',
				'a_grayscale-80'	=> '80',
				'a_grayscale-100'	=> '100',
			];

			$grayscales = apply_filters( 'abel_grayscales', $grayscales );
			return $grayscales;

		}


		/**
		 * Returns all color slugs
		 * 
		 * @return Array
		 */
		public static function getValues()
		{
			return array_keys( static::get() );
		}

	}