<?php

	namespace Abel\Helpers;

	use Cuisine\View\Loop;

	class Reference extends Post{
		
	
		/**
		 * Returns the location text
		 * 
		 * @return String
		 */
		public static function location()
		{
			return Loop::field( 'location', false );
		}


		/**
		 * Returns the description
		 * 
		 * @return String
		 */
		public static function description()
		{
			$sub = get_post_meta( get_the_ID(), 'description', true );
			if( $sub )
				return $sub;

			return '';
		}
	}