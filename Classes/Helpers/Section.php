<?php


	namespace Abel\Helpers;

	use WP_Query;
	use Cuisine\Utilities\Sort;

	class Section{

		/**
		 * Get all generated sections
		 * 
		 * @return Array
		 */
		public static function generated()
		{
			return [
				'brands',
				'content-boxed',
				'content-centered',
				'content-video',
				'cta',
				'listbuttons',
				'filter',
				'hero',
				'hero-alt',
				'image-bg-fixed',
				'image-gallery',
				'image-gallery-grid',
				'map',
				'news',
               	'partners',
                'products',
				'quotes',
				'quote',
				'references',
                'references-grid',
                'remote-products',
				'services',
				'services-grid',
				'team',
				'toptasks',
				'vac'
			];		
		}


		/**
		 * Returns all templates
		 * 
		 * @return Array
		 */
		public static function templates()
		{
			$args = [
				'post_type' => 'section-template',
				'posts_per_page' => -1
			];
			
			$query = new WP_Query( $args );
			$response = [];

			if( !empty( $query->posts ) ){
			
				$response = array_combine( 
					Sort::pluck( $query->posts, 'ID' ), 
					Sort::pluck( $query->posts, 'post_title' )
				);
			
			}

			return $response;

		}

	}