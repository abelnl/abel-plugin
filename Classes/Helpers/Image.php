<?php

	namespace Abel\Helpers;

	use Cuisine\Wrappers\Template;
	use Cuisine\View\Image as Media;

	class Image extends Post{

		/**
		 * Set the current global image
		 *
		 * @param Array $item
		 *
		 * @return void
		 */
		public static function set( $item )
		{
			if ( is_array( $item ) && array_key_exists( 'img-id', $item ) && $item['img-id'] != '' ) {
				$GLOBALS['image'] = $item;
				$GLOBALS['post'] = get_post( $item['img-id'] );
				return true;
			} 
			return false;
		}

		/**
		 * Returns the image media url (full)
		 * 
		 * @return String
		 */
		public static function url( $id = null, $size = 'full' )
		{
			if( is_null( $id ) ){
				global $image;
				$id = $image['img-id'];
			}

			if( !is_null( $id ) && $id != '' ){
				$url = Media::getMediaUrl( $id, $size );

				if ( $url == false || $url == '' )
					$url = Media::getMediaUrl( $id, 'full' );

				return $url;
			}

			return '';
		}

		/**
		 * Returns the image as an img-tag
		 * 
		 * @param  Array $item
		 * 
		 * @return String
		 */
		public static function tag()
		{

			return '<img src="'.static::url().'"/>';	
		}


		/**
		 * Get the image title
		 * 
		 * @return boolean
		 */
		public static function hasTitle()
		{
			global $post;
			if( $post->post_excerpt != '' )
				return true;

			return false;
		}

		/**
		 * Return the image title (caption in WP)
		 * 
		 * @return String
		 */
		public static function title()
		{
			return get_the_excerpt();
		}

		/**
		 * Show content, if there is any
		 * 
		 * @return String
		 */
		public static function content()
		{
			global $post;
 			return wpautop( $post->post_content );
		}

		/**
		* Get the background alignment is set
		*
		* @return String
		*/
		public static function alignment( $id = null ) 
		{
			if( is_null( $id ) ){
				global $image;
				$id = $image['img-id'];
			}
			$backgroundClass = get_post_meta( $id , 'background_alignment', true );
			if (!$backgroundClass || $backgroundClass == '') 
				$backgroundClass = 'a_cover-cc';
			
			return $backgroundClass;
		}

		/**
		 * Fetch theg grid-item template
		 * 
		 * @param  Array | WP_Post object $item
		 * @param  Section $section
		 * 
		 * @return String (html)
		 */
		public static function gridItem( $item, $section, $index = 0 )
		{
			
			$column = $section->getColumn();
			$name = 'section-'.$section->name;
			$name = str_replace( '-grid', '', $name );

			ob_start();

			Template::find( 'partials/'.$name.'-grid-item', null )->display([
				'item' => $item,
				'lightbox' => $column->hasLightbox(),
				'index' => $index
			]);

			return ob_get_clean();
		}

		/**
		 * Returns the lightbox template
		 * 
		 * @return String
		 */
		public static function lightbox( $media, $view )
		{
			ob_start();

			Template::find( 'partials/section-image-gallery-overlay', null )->display([
				'media' => $media,
				'view' => $view
			]);

			return ob_get_clean();
		}


		/**
		 * Return image field keys for broadcasting
		 * 
		 * @return Array
		 */
		public static function fieldKeys()
		{
			return [ 'img-id', 'image-id', 'attachment-id', 'attachment_id', 'img_id' ];
		}
	}