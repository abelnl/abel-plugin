<?php	

	namespace Abel\Helpers;

	use Cuisine\View\Loop;

	class Product extends Post{

		/**
		 * Returns the jobtitle of the teammember
		 * 
		 * @return String
		 */
		public static function url()
		{			
			return Loop::field( 'url', false );
		}

		/**
		 * Returns e-mailaddress or false
		 * 
		 * @return String
		 */
		public static function oldPrice()
		{			
			return Loop::field( 'old_price', false );
		}

		/**
		 * Returns telephone-number or false
		 * 
		 * @return String
		 */
		public static function price()
		{			
			return Loop::field( 'price', false );
		}


		/**
		 * Check socials for this teammember
		 * 
		 * @return boolean
		 */
		public static function isSale()
		{
			$isSale = Loop::field( 'sale', false );
			if( !$isSale || $isSale == ''  || $isSale == 'false' ) 
				return false;
			return true;
		}

		/**
		 * Returns the description
		 * 
		 * @return String
		 */
		public static function description()
		{
			$sub = get_post_meta( get_the_ID(), 'description', true );
			if( $sub )
				return $sub;

			return '';
		}
	}