<?php

	namespace Abel\Helpers;

	use Cuisine\View\Image;
	use Cuisine\Utilities\Date;
	use Cuisine\Wrappers\Template;
	use Abel\Templates\LayoutHandler;

	abstract class Post{
		
		/**
		 * Echo the post title
		 * 
		 * @return String
		 */
		public static function title()
		{
			return get_the_title();
		}


		/**
		 * Echo the post title
		 * 
		 * @return String
		 */
		public static function content()
		{
			return get_the_content();
		}


		/**
		 * Echo the date
		 * 
		 * @param  string $format
		 * 
		 * @return String
		 */
		public static function date( $format = 'j F Y' )
		{
			return Date::get( null, $format );
		}

		/**
		 * Return the link
		 * 
		 * @return String
		 */
		public static function link(){
			return get_the_permalink();
		}


		

		/**
		 * Returns the first post category
		 * 
		 * @return String
		 */
		public static function cat()
		{
			$terms = wp_get_post_terms( get_the_ID(), 'category' );
			if( !is_wp_error( $terms ) && !empty( $terms ) && $terms[0]->slug != 'geen-categorie' ){
				return $terms[0]->name;
			}

			return '';
		}


		/**
		 * Return the post thumbnail url
		 * 
		 * @return String
		 */
		public static function thumb( $size )
		{
			global $post;
			$postId = $post->ID;

			if( has_post_thumbnail( $postId ) ){
				return Image::getThumbnailUrl( $size, $postId );
			}
		
			return '';
		}


		/**
		 * Return the post thumbnail url
		 * 
		 * @return String
		 */
		public static function thumbId( )
		{
			global $post;
			$postId = $post->ID;

			$thumbId = get_post_thumbnail_id( $postId );
			if ($thumbId == '')
				return null;
		
			return $thumbId;
		}


		/**
		 * Fetch the grid-item template
		 * 
		 * @param  Array | WP_Post object $item
		 * @param  Section $section
		 * 
		 * @return String (html)
		 */
		public static function gridItem( $item, $section )
		{
			
			$name = 'section-'.$section->name;
			$name = str_replace( '-grid', '', $name );
			$view = new LayoutHandler( $section );

			ob_start();

			Template::find( 'partials/'.$name.'-grid-item', null )->display([
				'item' => $item,
				'view' => $view,
				
			]);

			return ob_get_clean();
		}

		/**
		 * Set the post global
		 * 
		 * @param Array $post
		 *
		 * @return void
		 */
		public static function set( $item )
		{
			$GLOBALS['post'] = $item;
		}
	}