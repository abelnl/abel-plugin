<?php

	namespace Abel\Helpers;

	use Cuisine\View\Pagination as CuisinePagination;

	class Pagination extends CuisinePagination{

		/**
		 * Returns the html for the current pagination
		 * 
		 * @return string ( html )
		 */
		public function get( $_query = null ){

			$this->init( $_query );

			//we have pages:
			if( $this->pages > 1 ){

				$html = '';
				$html .= '<nav class="ac_pagination" itemscope="itemscope" itemtype="http://www.schema.org/SiteNavigationElement">';

					for( $i = 1; $i <= $this->pages; $i++ ){

						if( $i == $this->current ){

							$html .= '<span itemprop="name">'.$this->current.'</span>';

						}else{

							$url = self::getLink( $i );
							$html .= '<a  itemprop="url" href="'.$url.'">'.$i.'</a>';

						}

					}

				$html .= '</nav>';

				return $html;

			//we don't have pages:
			}else{
				
				return false;
			}

		}
	}