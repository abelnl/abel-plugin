<?php	

	namespace Abel\Helpers;

	use Cuisine\View\Loop;

	class TeamMember extends Post{

		
		/**
		 * Echo the name
		 * 
		 * @return String
		 */
		public static function name()
		{
			return parent::title();
		}

		/**
		 * Echo the bio
		 * 
		 * @return String
		 */
		public static function bio()
		{
			return parent::content();
		}
		
		/**
		 * Returns the teammembers avatar
		 * 
		 * @return String
		 */
		public static function avatar()
		{
			$avatar = Loop::field( 'avatar', null );
			
			if( !is_null( $avatar ) )
				return Image::url( $avatar['img-id'] );

			return null;
		}

		/**
		 * Returns the teammembers avatar ID
		 * 
		 * @return String
		 */
		public static function avatarId()
		{
			$avatar = Loop::field( 'avatar', null );
			
			if( !is_null( $avatar ) )
				return $avatar['img-id'];

			return null;
		}

		/**
		 * Returns the jobtitle of the teammember
		 * 
		 * @return String
		 */
		public static function jobtitle()
		{			
			return Loop::field( 'jobtitle', false );
		}

		/**
		 * Returns e-mailaddress or false
		 * 
		 * @return String
		 */
		public static function email()
		{			
			return Loop::field( 'email', false );
		}

		/**
		 * Returns telephone-number or false
		 * 
		 * @return String
		 */
		public static function telephone()
		{			
			return Loop::field( 'telephone', false );
		}


		/**
		 * Check socials for this teammember
		 * 
		 * @return boolean
		 */
		public static function hasSocials()
		{
			$fields = static::getSocialFields();
			$hasSocials = false;

			foreach( $fields as $key ){
				if( Loop::field( $key, false ) ){
					$hasSocials = true;
					break;
				}
			}
		}


		/**
		 * Return all socials in an array
		 * 
		 * @return Array
		 */
		public static function getSocials()
		{
			$fields = static::getSocialFields();
			$socials = [];

			foreach( $fields as $key ){

				if( $value = Loop::field( $key, false ) ){

					$socials[ $key ] = [
						'link' => $value,
						'type' => $key
					];

				}

			}

			return $socials;
		}


		/**
		 * Return the keys of all social fields
		 * 
		 * @return Array
		 */
		public static function getSocialFields()
		{
			return ['facebook', 'twitter', 'linkedin', 'googleplus', 'youtube', 'instagram' ];
		}

		/**
		 * Returns facebook url or false
		 * 
		 * @return String
		 */
		public static function facebook()
		{			
			return Loop::field( 'facebook', false );
		}

		/**
		 * Returns twitter url or false
		 * 
		 * @return String
		 */
		public static function twitter()
		{			
			return Loop::field( 'twitter', false );
		}

		/**
		 * Returns googleplus url or false
		 * 
		 * @return String
		 */
		public static function googleplus()
		{			
			return Loop::field( 'googleplus', false );
		}

		/**
		 * Returns linkedin url or false
		 * 
		 * @return String
		 */
		public static function linkedin()
		{			
			return Loop::field( 'linkedin', false );
        }

        /**
         * Returns the youtube link or false
         *
         * @return String
         */
        public static function youtube() {
            return Loop::field( 'youtube', false );
        }

		/**
		 * Returns instagram url or false
		 * 
		 * @return String
		 */
		public static function instagram()
		{			
			return Loop::field( 'instagram', false );
		}
	}