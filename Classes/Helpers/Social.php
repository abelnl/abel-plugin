<?php

	namespace Abel\Helpers;

	use Abel\Front\Settings;

	class Social{


		/**
		 * Returns all social keys
		 * 
		 * @return Array
		 */
		public static function all()
		{
			return [
				'facebook' => 'Facebook',
				'twitter' => 'Twitter',
				'linkedin' => 'Linkedin',
                'googleplus' => 'Google Plus',
                'youtube'    => 'Youtube',
				'instagram' => 'Instagram'
			];
		}

		/**
		 * Check if a social has a link
		 * 
		 * @param  String  $key
		 * 
		 * @return boolean
		 */
		public static function hasLink( $key )
		{
			if( Settings::get( $key, false ) )
				return true;

			return false;	
		}

		/**
		 * Returns the link
		 * 
		 * @param  String $key
		 * 
		 * @return String
		 */
		public static function link( $key )
		{
			return Settings::get( $key );
		}

		/**
		 * Returns the link desdescription
		 * 
		 * @param  String $key
		 * 
		 * @return String
		 */
		public static function description( $key )
		{
			return Settings::get( $key.'_description' );
		}

		/**
		 * Returns the social media desdescription
		 * 
		 * @param  String $key
		 * 
		 * @return String
		 */
		public static function social_description(  )
		{
			return Settings::get( 'social_description' );
		}


	}