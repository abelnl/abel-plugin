<?php

	namespace Abel\Helpers;

	use WP_Query;
	use Cuisine\Utilities\Sort;


	class Pages{


		/**
		 * Return key-value pairs of pages
		 * 
		 * @return Array
		 */
		public static function all()
		{
			$args = [
				'post_type' => 'page',
				'posts_per_page' => -1
			];
			
			$query = new WP_Query( $args );
			$response = [];

			if( !empty( $query->posts ) ){
			
				$response = array_combine( 
					Sort::pluck( $query->posts, 'ID' ), 
					Sort::pluck( $query->posts, 'post_title' )
				);
			
			}

			return $response;
		}

	}