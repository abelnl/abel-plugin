<?php

	namespace Abel\Helpers;

	/**
	* Class containing measurements, sizes, margins etc.
	*
	*
	*/
	class Measurements{


		/**
		 * Container witdhs
		 * 
		 * @return Array
		 */
		public static function getWidths()
		{
			$widths = [
				'a_max-width-inherit' => '100%',
				'a_max-width-600' => '600',
				'a_max-width-800' => '800',
				'a_max-width-1000' => '1000',
				'a_max-width-1200' => '1200',
				'a_max-width-1400' => '1400',
				'a_max-width-1600' => '1600',
				'a_max-width-1800' => '1800',
				'a_max-width-2000' => '2000'
			];	

			$widths = apply_filters( 'abel_widths', $widths );
			return $widths;	
		}

		/**
		 * Paddings and margins
		 * 
		 * @return Array
		 */
		public static function getSizes()
		{
			$sizes = [
				'0'		=> '0',
				'10'	=> '10',
			 	'20'	=> '20',
			 	'40'	=> '40',
			 	'60'	=> '60',
			 	'80'	=> '80',
			 	'100'	=> '100',
			 	'120'	=> '120',
			 	'140'	=> '140',
			 	'160'	=> '160',
			 	'180'	=> '180',
			 	'200'	=> '200'
			];	

			$sizes = apply_filters( 'abel_sizes', $sizes );
			return $sizes;	
		}

		/**
		 * Alignment options
		 * 
		 * @return Array
		 */
		public static function getAlignments()
		{
			$alignments = [
				'v_title-left_button-left'			=> 'title left, button left',
			 	'v_title-centered_button-right'		=> 'title center, button right',
			 	'v_title-centered_button-centered'	=> 'title center, button center',
				'v_title-left_button-right'			=> 'title left, button right'	
			];	

			$alignments = apply_filters( 'abel_alignments', $alignments );
			return $alignments;	
		}


		/**
		 * Background alignment
		 * 
		 * @return Array
		 */
		public static function getBackgroundAlignments()
		{
			$background_alignments = [
				'a_cover-ct'	=> 'center top',
			 	'a_cover-cc'	=> 'center',
			 	'a_cover-cb'	=> 'center bottom',
				'a_cover-lt'	=> 'left top',
			 	'a_cover-lc'	=> 'left center',
			 	'a_cover-lb'	=> 'left bottom',
				'a_cover-rt'	=> 'right top',
			 	'a_cover-rc'	=> 'right center',
			 	'a_cover-rb​'	 => 'right bottom'
			];	


			$background_alignments = apply_filters( 'abel_background_alignments', $background_alignments );
			return $background_alignments;	
		}
		
	}