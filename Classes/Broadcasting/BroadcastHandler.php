<?php

	namespace Abel\Broadcasting;

	use Abel\Helpers\Image;

	abstract class BroadcastHandler{

		/**
		 * Broadcast object
		 * @var 
		 */
		public $broadcast;

		/**
		 * Data to broadcast
		 * 
		 * @var
		 */
		public $data;


		/**
		 * The new attachments to 'attach'
		 * 
		 * @var Array
		 */
		protected $attachments;


		/**
		 * Original Post Id
		 * 
		 * @var Int
		 */
		protected $originalPostId;

		/**
		 * Original sections data
		 * 
		 * @var Array
		 */
		protected $originalMeta;

		/**
		 * Constructor
		 * 
		 * @param BroadcastData $bcd
		 * @param Array $attachments
		 *
		 * @return void
		 */
		public function __construct( $bcd )
		{
			$this->broadcast = $bcd;
			$this->data = $this->broadcast->broadcasting_data;

			$this->originalPostId = $this->data->_POST['post_ID'];
			$currentBlog = get_current_blog_id();
			$mainBlog = (int) get_network()->site_id;

			switch_to_blog( $mainBlog );
			$this->originalMeta = get_post_custom( $this->originalPostId );
			switch_to_blog( $currentBlog );
		}


		/**
		 * Change the attachments of an array
		 *
		 * @param Array $object
		 * 
		 * @return Array
		 */
		public function alterAttachments( $object, $data = null )
		{
			if( is_null( $data ) )
				$data = $this->data;

			$keys = Image::fieldKeys();
			if( is_array( $object ) && !empty( $object )){
				foreach( $object as $key => $item ){

					if( is_array( $item ) ){
					
						$object[ $key ] = $this->alterAttachments( $item );
					
					}else if( in_array( $key, $keys ) ){

						if( $object[ $key ] != '' && !is_null( $object[ $key ] ) ){


							$newValue = $data->copied_attachments()->get( $object[ $key ] );
							$object[ $key ] = $newValue;
						}

					}
				}
			}

			return $object;
		}	
	}