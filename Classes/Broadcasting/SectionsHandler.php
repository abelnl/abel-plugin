<?php


	namespace Abel\Broadcasting;


	class SectionsHandler extends BroadcastHandler{


		/**
		 * Broadcast section data
		 * 
		 * @return void
		 */
		public function broadcast()
		{
			$this->saveSections();
			$this->saveColumns();			
		}

		/**
		 * Save sections in this new post
		 * 
		 * @return void
		 */
		public function saveSections()
		{
			$meta = $this->originalMeta;
			$newPost = $this->data->new_post->ID;

			if( !isset( $meta['sections'] ) )
				return;

			$sections = unserialize( $meta['sections'][0] );
			foreach( $sections as $i => $section ){

				$sections[$i]['post_id'] = $newPost;
				$sections[$i] = $this->alterAttachments( $sections[ $i ], $this->data );
			}


			update_post_meta( $newPost, 'sections', $sections );
		}


		/**
		 * Save custom column data
		 * 
		 * @return void
		 */
		public function saveColumns()
		{
			$meta = $this->originalMeta;
			$newPost = $this->data->new_post->ID;

			foreach( $meta as $key => $metaItem ){

				if( substr( $key, 0, 7 ) == '_column' ){
					$column = unserialize( $metaItem[0] );

					if( is_array( $column ) ){
						
						//change post-ids
						if( isset( $column['post_id'] ) )
							$column['post_id'] = $newPost;

						//alter attachments:
						$column = $this->alterAttachments( $column, $this->data );
					}

					update_post_meta( $newPost, $key, $column );
				}

			}
		}

	}