<?php

	namespace Abel\Broadcasting;

	use WP_Query;

	class ContentHandler{

		/**
		 * Remove all broadcasted children
		 * 
		 * @return void
		 */
		public function removeBroadcastedChildren()
		{
			$args = [
				'posts_per_page' => -1,
				'post_type' => 'any',
				'post_status'=>'publish',
				'meta_query' => [[
					'key' => 'broadcast_child_original',
					'compare' => 'EXISTS'
				]]
			];

			$query = new WP_Query( $args );

			if( $query->have_posts() ){
				foreach( $query->posts as $_post ){
					wp_delete_post( $_post->ID );
				}
			}

		}

	}
