<?php

	namespace Abel\Broadcasting;

	use RecursiveArrayIterator;
	use RecursiveIteratorIterator;
	use Abel\Helpers\Image;

	class IndicatorHandler extends BroadcastHandler{

		/**
		 * Set the indicator on the target post
		 *
		 * @return void
		 */
		public function setIndicator()
		{
			$newPost = $this->data->new_post->ID;
			$originalPost = $this->originalPostId;

			update_post_meta( $newPost, 'broadcast_child_original', $originalPost );
			
			
		}


		/**
		 * Remove the indicator
		 *
		 * @return void
		 */
		public static function removeIndicator( $data ){

			if( $data->action == 'unlink' ){
				delete_post_meta( $data->post_id, 'broadcast_child_original' );
			}
		}
	}
