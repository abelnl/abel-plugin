<?php

    namespace Abel\Broadcasting;

    use WP_Query;
    use Cuisine\Wrappers\Record;
    use ChefDeploy\Wrappers\Environment;

    class MigrationHandler{


        /**
         * Current blog id
         *
         * @var Int
         */
        protected $currentBlogId;

        /**
         * Main blog id
         *
         * @var Int
         */
        protected $mainBlog;


        /**
         * Table name of Broadcast data
         *
         * @var string
         */
        protected $tableName = '_3wp_broadcast_broadcastdata';


        /**
         * Constructor
         */
        public function __construct()
        {
            $this->currentBlogId = get_current_blog_id();
            $this->mainBlog = (int) get_network()->site_id;
            $this->environment = Environment::getByUrl( get_site_url() );
        }

        /**
         * Get all posts, and re-link them
         *
         * @return void
         */
        public function process()
        {
            //runs on local:
            $posts = new WP_Query( $this->getQuery() );

            if( !empty( $posts->posts ) ){
                foreach( $posts->posts as $p ){

                    $parentId = $this->getParent( $p );

                    if( !is_null( $parentId ) ){
                        
                        switch_to_blog( $this->mainBlog );
                            $this->setParent( $parentId, $p->ID );
                            $this->setChild( $parentId, $p->ID );
                        switch_to_blog( $this->currentBlogId );
                        
                        if ( defined('WP_CLI') && WP_CLI ){
                            \WP_CLI::success( "Post {$p->post_title} reconnected to broadcast parent." );
                        }


                    }else{
                        //delete_post_meta( $p->ID, 'broadcast_child_original' );
                    }
                }
            }
        }

        /**
         * Return all posts that need to be reconnected
         *
         * @return Array
         */
        public function getQuery()
        {
            return [
				'posts_per_page' => -1,
				'post_type' => $_GET['post_type'],
				'post_status'=>'publish',
				'meta_query' => [[
					'key' => 'broadcast_child_original',
					'compare' => 'EXISTS'
				]]
			];
        }


        /**
         * Get the right parent
         *
         * @param WP_Post $post
         * 
         * @return Int
         */
        public function getParent( $post )
        {
            switch_to_blog( $this->mainBlog );

                $_post = new WP_Query([
                    'title' => $post->post_title,
                    'post_type' => $post->post_type,
                    'posts_per_page' => 1
                ]);
                
            switch_to_blog( $this->currentBlogId );

            if( !empty( $_post->posts ) ){
                return $_post->posts[0]->ID;
            }else{
                return null;
            }
        }

        /**
         * Set the parent value of a post
         *
         * @return void
         */
        public function setParent( $original, $child )
        {
            switch_to_blog( $this->mainBlog );
            
            $record = Record::find( $this->tableName )->where([
                'blog_id' => $this->mainBlog,
                'post_id' => $original
            ])->first();

            //if this record exists:
            if( !is_null( $record ) ){
                
                $data = $this->unhash( $record->data );
                if( empty( $data['linked_children'] ) )
                    $data['linked_children'] = [];

                $data['linked_children'][ $this->currentBlogId ] = $child;

                Record::delete( $this->tableName, $record->id );

            }else{

                $data = [
                    'version' => '2',
                    'linked_children' => [
                        $this->currentBlogId => $child
                    ]
                ];
            }

            Record::insert( $this->tableName, [
                'blog_id' => $this->mainBlog,
                'post_id' => $original,
                'data' => $this->hash( $data )
            ]);
        }

        /**
         * Set the child value of a post
         *
         * @param Int $original
         * @param Int $child
         * @return void
         */
        public function setChild( $original, $child )
        {
            $data = $this->hash([
                'version' => 2,
                'linked_parent' => [
                    'blog_id' => $this->mainBlog,
                    'post_id' => $original
                ]
            ]);

            $this->clearRecord( $this->currentBlogId, $child );

            $output = Record::insert( $this->tableName, [
                'blog_id' => $this->currentBlogId,
                'post_id' => $child,
                'data' => $data
            ]);
        }

        /**
         * Clear an existing record
         *
         * @param Int $blogId
         * @param Int $postId
         * @return void
         */
        public function clearRecord( $blogId, $postId )
        {
            $record = Record::find( $this->tableName )->where([
                'blog_id' => $blogId,
                'post_id' => $postId
            ])->first();

            if( !is_null( $record ) ){
                Record::delete( $this->tableName, $record->id );
            }
        }

        /**
         * Create a data hash
         *
         * @param Array $array
         * @return String
         */
        public function hash( $array )
        {
            return base64_encode( serialize( $array ) );
        }

        /**
         * Unhash a string
         *
         * @param String $string
         * @return Array
         */
        public function unhash( $string )
        {
            return unserialize( base64_decode( $string ) );
        }

    }