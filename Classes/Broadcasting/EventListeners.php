<?php

	namespace Abel\Broadcasting;

    use Cuisine\Wrappers\User;
    use Abel\Wrappers\StaticInstance;

	class EventListeners extends StaticInstance{


		/**
		 * Constructor
		 */
		public function __construct()
		{

			if( is_multisite() ){
			
                $this->multisite();
			
			}else{
            
                $this->singlesite();
			
            }
            
		}

		/**
		 * Listen for broadcasting events
		 * 
		 * @return void
		 */
		public function multisite()
		{	

			add_action( 'threewp_broadcast_preparse_content', function( $data ){				
				( new MediaHandler( $data ) )->broadcast();
			});

			add_action( 'threewp_broadcast_broadcasting_before_restore_current_blog', function( $data ){

				//handle linked posts
				( new MediaHandler( $data ) )->setMetadata();
				( new SectionsHandler( $data ) )->broadcast();
				( new IndicatorHandler( $data ) )->setIndicator();

			}, 100 );


			//remove our manual link, when the post is unlinked.
			add_action( 'threewp_broadcast_post_action', function( $data ){

				IndicatorHandler::removeIndicator( $data );

            });
            

            //currently, we'll do it fugly:
            $this->placeButton();
            
            
            add_action( 'admin_init', function(){
                if( isset( $_GET['broad'] ) ){
                    ( new MigrationHandler() )->process();
                    wp_redirect( remove_query_arg( 'broad' ) );
                    exit();
                }
            });

		}


		/**
		 * Listen for broadcasting events on the single site
		 * 
		 * @return void
		 */
		public function singlesite()
		{
			//remove broadcasted items of a cloned site:
			add_action( 'chef_deploy_after_fetch_staging', function( $args, $assoc_args ){

				if( isset( $assoc_args['clone'] ) ){

					( new ContentHandler() )->removeBroadcastedChildren();

					if( defined( 'WP_CLI' ) )
						\WP_CLI::success( "Old broadcasted posts cleared." );

				}

			}, 20, 2 );
		}

        /**
         * Place the button
         *
         * @return void
         */
        public function placeButton()
        {
            global $pagenow;
            
            $allowed = ['brand', 'partner', 'post', 'page'];
            
            if( User::hasRole( 'administrator' ) ){

                if( $pagenow == 'edit.php' && isset( $_GET['post_type'] ) && in_array( $_GET['post_type'], $allowed ) ){
                    add_action( 'all_admin_notices', function(){
                        echo '<a href="edit.php?post_type='.$_GET['post_type'].'&broad=true" style="padding:4px 8px;background:#444;color:white;border-radius:4px;display:inline-block;margin:30px 0 0 0;text-decoration: none;">Re-connect broadcast items</a>';
                    });
                }

            }
        }
	}


	\Abel\Broadcasting\EventListeners::getInstance();