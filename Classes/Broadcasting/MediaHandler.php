<?php

	namespace Abel\Broadcasting;

	use RecursiveArrayIterator;
	use RecursiveIteratorIterator;
	use Abel\Helpers\Image;

	class MediaHandler extends BroadcastHandler{

		/**
		 * Broadcast all loose attachments
		 * 
		 * @return void
		 */
		public function broadcast()
		{
			$attachments = $this->filterAttachments();
			if( !empty( $attachments ) ){
				foreach( $attachments as $imgId ){
					$this->data->add_attachment( $imgId );
				}
			}
		}


		/**
		 * Update image id's in meta data:
		 */
		public function setMetadata()
		{

			$meta = $this->originalMeta;
			$newPost = $this->data->new_post->ID;

			$keys = array_keys( $meta );
			$notAllowed = [];
			foreach( $keys as $key ){
				
				if( 
					$key == 'sections' || 
					substr( $key, 0, 7 ) == '_column' || 
					$key == '_thumbnail_id'
				)
					$notAllowed[] = $key;
			}

		
			foreach( $meta as $key => $value ){

				if( !in_array( $key, $notAllowed ) ){
					if( sizeof( $value ) == 1 ){
						$value = unserialize( $value[0] );
						$value = $this->alterAttachments( $value, $this->data );
					}

					update_post_meta( $newPost, $key, $value );
				}
			}	
		}

		/**
		 * Recursively loop through the array and get all image id's being posted
		 * 
		 * @return Array
		 */
		public function filterAttachments()
		{
			$result = [];
			$postData = $this->data->_POST;
			$keys = $this->getImageMetaKeys();

			$iterator = new RecursiveIteratorIterator( new RecursiveArrayIterator( $postData ) ); 
			foreach( $iterator as $key => $value ){
				
				if( 
					in_array( $key, $keys ) && 
					$value !== '' && 
					$value !== '-' 
					&& (string)$value !== '0'
				)
					$result[] = $value;
			}

			return $result;
		}


		/**
		 * Returns a list of possible image meta-data keys
		 * 
		 * @return Array
		 */
		public static function getImageMetaKeys()
		{
			return [ 'img-id', 'image-id', 'attachment-id', 'attachment_id', 'img_id' ];
		}


	}