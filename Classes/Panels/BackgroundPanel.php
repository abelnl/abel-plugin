<?php

	namespace Abel\Panels;

	use Abel\Front\Settings;
	use Abel\Helpers\Colors;
	use Abel\Helpers\Dividers;
	use \Abel\Helpers\Measurements;
	use \Abel\Helpers\Animations;
	use Cuisine\Wrappers\Field;
	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\SettingsPanel as Panel;

	class BackgroundPanel extends StaticInstance{


		/**
		 * Constructor
		 * 
		 */
		public function __construct()
		{
			add_action( 'init', [ $this, 'registerPanel' ]);
		}


		/**
		 * Register this panel
		 * 
		 * @return void
		 */
		public function registerPanel()
		{
			
			Panel::make( 
				'Appearance',
				'design',
				array(
					'position' 		=> 5,
					'icon'			=> 'dashicons-admin-appearance',
					'rules' 		=> [ 
						'notInContainer' 	=> 'all',
						'userRole' 			=> 'administrator' 
					]
				)

			)->set( $this->getFields() );	
		}


		/**
		 * Returns the fields for this panel
		 * 
		 * @return Array
		 */
		public function getFields()
		{

			$none = __( 'Geen', 'abelplugin' );
			$colors = array_merge(['none' => $none ], Colors::get());
			$deviderColors = Colors::getForDevider();
			$textColors = Colors::getForText();
			$fillColors = Colors::getForFill();
			$grayscales = Colors::getGrayscales();
			$dividers = array_merge(['none' => $none ], Dividers::get());
			$backgroundAlignments = Measurements::getBackgroundAlignments();
			$widths = Measurements::getWidths();
			$animations = Animations::get();
			$opacities = Colors::getOpacities();
			$blendModes = Colors::getBlendModes();
			
			return [

				Field::select(
					'divider',
					__( 'Sectie divider', 'abelplugin' ),
					$dividers
				),
				Field::select(
					'divider_color',
					__( 'Divider kleur', 'abelplugin' ),
					$deviderColors
				),

				Field::select( 'container_textColor', __( 'Tekstkleur', 'abelplugin' ), $textColors, ['defaultValue' => Settings::get( 'default_text_color' )] ),
				Field::select( 'container_fillColor', __( 'Vulkleur', 'abelplugin' ), $fillColors, ['defaultValue' => Settings::get( 'default_fill_color' )] ),
				Field::select( 'backgroundColor', __( 'Achtergrondkleur', 'abelplugin' ), $colors ),
				Field::image( 'background', __( 'Achtergrondafbeelding', 'abelplugin' ) ),
				Field::select(
					'background_alignment',
					__( 'Achtergrond uitlijning', 'abelplugin' ),
					$backgroundAlignments,
					['defaultValue' => 'a_cover-ct']
				),
				
				Field::select(
					'background_opacity',
					__( 'Achtergrond Opacity', 'abelplugin' ),
					$opacities,
					[ 'defaultValue' => 100 ]
				),

				Field::select(
					'background_blendmode',
					__( 'Achtergrond Blend Mode', 'abelplugin' ),
					$blendModes,
					['defaultValue' => 'none']
				),

				Field::select(
					'background_grayscale',
					__( 'Achtergrond Grayscale', 'abelplugin' ),
					$grayscales,
					['defaultValue' => 'none']
				),

				Field::text( 'video_url', __( 'Achtergrond-video locatie (in thema)', 'abelplugin' ) ),

				Field::select(
					'container_width',
					__( 'Maximale breedte', 'abelplugin' ),
					$widths,
					['defaultValue' => Settings::get( 'default_container_width' )]
				),

				Field::select(
					'section_animation',
					__( 'Animatie', 'abelplugin' ),
					$animations,
					['defaultValue' => 'none']
				)
			];
		}


	}

	\Abel\Panels\BackgroundPanel::getInstance();