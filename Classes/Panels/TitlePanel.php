<?php

	namespace Abel\Panels;

	use Abel\Helpers\Colors;
	use Cuisine\Wrappers\Field;
	use Abel\Helpers\Measurements;
	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\SettingsPanel as Panel;
	use Abel\Front\Settings;

	class TitlePanel extends StaticInstance{

		/**
		 * Constructor
		 * 
		 */
		public function __construct()
		{
			add_action( 'init', [ $this, 'registerPanel' ]);
		}


		/**
		 * Register this panel
		 * 
		 * @return void
		 */
		public function registerPanel()
		{
			
			Panel::make( 
				'Titel',
				'title',
				array(
					'position' 		=> 6,
					'icon'			=> 'dashicons-megaphone',
					'rules' 		=> [ 
						'notInContainer' 	=> 'all',
						'userRole' 			=> 'administrator' 
					]
				)

			)->set( $this->getFields() );	
		}


		/**
		 * Returns the fields for this panel
		 * 
		 * @return Array
		 */
		public function getFields()
		{

			$widths = Measurements::getWidths();
			$alignments = Measurements::getAlignments();
			$textColors = Colors::getForText();
			$fillColors = Colors::getForFill();

			return [
				Field::select(
					'title_width',
					__( 'Maximale breedte', 'abelplugin' ),
					$widths,
					['defaultValue' => 'a_max-width-1000']
				),
				Field::select(
					'title_alignment',
					__( 'Uitlijning', 'abelplugin' ),
					$alignments,
					['defaultValue' => 'v_title-left_button-left' ]
				),

				Field::select( 'textColor', __( 'Tekstkleur', 'abelplugin' ), $textColors, ['defaultValue' => Settings::get( 'default_text_color' )] ),
				Field::select( 'fillColor', __( 'Vulkleur', 'abelplugin' ), $fillColors, ['defaultValue' => Settings::get( 'default_fill_color' )] ),

				Field::button(
					'title_link',
					__( 'Button', 'abelplugin' )
				)
			];
		}


	}

	\Abel\Panels\TitlePanel::getInstance();