<?php

	namespace Abel\Panels;

	use Abel\Helpers\Colors;
	use Cuisine\Wrappers\Field;
	use Abel\Helpers\Measurements;
	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\SettingsPanel as Panel;

	class HeroPanel extends StaticInstance{

		/**
		 * Constructor
		 * 
		 */
		public function __construct()
		{
			add_action( 'init', [ $this, 'registerPanel' ]);
		}


		/**
		 * Register this panel
		 * 
		 * @return void
		 */
		public function registerPanel()
		{
			
			Panel::make( 
				'Hero',
				'hero',
				array(
					'position' 		=> 5,
					'icon'			=> 'dashicons-images-alt2',
					'rules' 		=> [ 
						'containerType' 	=> 'tabs',
						'userRole' 			=> 'administrator' 
					]
				)

			)->set( $this->getFields() );	
		}


		/**
		 * Returns the fields for this panel
		 * 
		 * @return Array
		 */
		public function getFields()
		{
			return [
				Field::checkbox(
					'linkToContent',
					__( 'Link naar content laten zien? (muis)', 'abelplugin' )
				),
				Field::checkbox(
					'showArrows',
					__( 'Pijltjes tonen?', 'abelplugin' )
				),
				Field::checkbox(
					'showNavigation',
					__( 'Navigatiebolletjes tonen?', 'abelplugin' )
				)
			];
		}


	}

	\Abel\Panels\HeroPanel::getInstance();