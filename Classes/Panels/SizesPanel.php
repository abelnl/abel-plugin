<?php

	namespace Abel\Panels;

	use Abel\Front\Settings;
	use Cuisine\Wrappers\Field;
	use Abel\Helpers\Measurements;
	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\SettingsPanel as Panel;

	class SizesPanel extends StaticInstance{


		/**
		 * Constructor
		 * 
		 */
		public function __construct()
		{
			add_action( 'init', [ $this, 'registerPanel' ]);
		}


		/**
		 * Register this panel
		 * 
		 * @return void
		 */
		public function registerPanel()
		{
			
			Panel::make( 
				'Marges & Padding',
				'sizes',
				array(
					'position' 		=> 5,
					'icon'			=> 'dashicons-move',
					'rules' 		=> [ 
						'notInContainer' 	=> 'all',
						'userRole' 			=> 'administrator' 
					]
				)

			)->set( $this->getFields() );	
		}


		/**
		 * Returns the fields for this panel
		 * 
		 * @return Array
		 */
		public function getFields()
		{

			$sizes = Measurements::getSizes();

			return [
				Field::select(
					'paddingTop',
					'Padding Top',
					$sizes,
					[
						'defaultValue' => Settings::get( 'default_padding-top' )
					]
				),
				Field::select(
					'paddingBottom',
					'Padding Bottom',
					$sizes,
					[
						'defaultValue' => Settings::get( 'default_padding-bottom' )
					]
				),
				Field::select( 
					'marginTop', 
					'Margin Top',
					$sizes,
					[
						'defaultValue' => Settings::get( 'default_margin-top' )
					]
				),
				Field::select(
					'marginBottom',
					'Margin Bottom',
					$sizes,
					[
						'defaultValue' => Settings::get( 'default_margin-bottom' )
					]
				)
			];
		}

	}

	\Abel\Panels\SizesPanel::getInstance();