<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class PartnersGenerator extends BaseGenerator{

		/**
         * Type of generator
         *
         * @var string
         */
        protected $type = 'partners';
		

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
            return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->name( 'partners' );

				$section->allowedColumns([ 'handpickedcollection', 'collection' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column('handpickedcollection')
							->post_type( 'partners' )
							->posts_per_page( 3 )
							->posts_per_row( 3 )
                            ->display_link( false )
				
				]);

			});
		}

	}

	\Abel\Generators\PartnersGenerator::getInstance();