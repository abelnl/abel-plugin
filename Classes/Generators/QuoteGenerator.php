<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class QuoteGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'quote';


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'quote' );
				$section->name( 'quote' );

				$section->allowedColumns([ 'quote' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'quote' )
				
				]);

			});
		}

	}

	\Abel\Generators\QuoteGenerator::getInstance();