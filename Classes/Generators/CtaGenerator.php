<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class CtaGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'cta';
				


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'cta' );
				$section->name( 'cta' );

				$section->allowedColumns([ 'cta' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

                    $section->column( 'cta' )
                    
				]);

			});
		}

	}

	\Abel\Generators\CtaGenerator::getInstance();