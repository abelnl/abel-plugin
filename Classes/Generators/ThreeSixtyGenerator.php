<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ThreeSixtyGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = '360image';
		

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'three-sixty-image' );
				$section->name( '360 image' );

				$section->allowedColumns([ 'abel360' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'abel360' )
				
				]);

			});
		}

	}

	\Abel\Generators\ThreeSixtyGenerator::getInstance();