<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;
	use ChefSections\Generators\ColumnGenerator;
	use ChefSections\Generators\PageGenerator;

	class HeroAltGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'hero-alt';
			
		
		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::page( 'Hero-Alt', function( PageGenerator $blueprint ){
				
				$blueprint->post(['post_title' => 'hero-alt', 'post_type' => 'section-template']);
				$blueprint->post_type('section-template');

				$blueprint->sectionContainer( 'Hero slider', [

					'name' => 'hero-alt',
					'type' => 'container',
					'slug' => 'tabs',
					'view' => 'tabbed',
					'allowedColumns' => [ 'cta' ],
					'allowedViews' => ['fullwidth'],

					'sections' => [

						$blueprint->section( 'Slide titel', [
							'view' => 'fullwidth',
							'columns' => [ 	
                                new ColumnGenerator( array( 'type' => 'cta' ) )
                            ],
                            'backgroundColor' => 'brand_secondary',
                            'background_opacity' => 'a_opacity-40',
                            'background_alignment' => 'a_cover-cc'

						])
					]
				]);
			});
		}

	}

	\Abel\Generators\HeroAltGenerator::getInstance();