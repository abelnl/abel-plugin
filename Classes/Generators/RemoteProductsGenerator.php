<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class RemoteProductsGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'remote-products';



		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'remote-products' );
				$section->name( 'remote products' );

				$section->allowedColumns([ 'productfeed' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([
					$section->column('productfeed')
				]);

			});
		}

	}

	\Abel\Generators\RemoteProductsGenerator::getInstance();