<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class BrandsGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'brands';
				
		

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'brands' );
				$section->name( 'brands' );

				$section->allowedColumns([ 'handpickedcollection', 'collection' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column('handpickedcollection')
							->post_type( 'brand' )
							->posts_per_page( 6 )
							->posts_per_row( 3 )
                            ->display_link( false )
				
				]);

			});
		}

	}

	\Abel\Generators\BrandsGenerator::getInstance();