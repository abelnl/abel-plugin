<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;

	abstract class BaseGenerator extends StaticInstance{

        /**
         * Type of generator
         *
         * @var string
         */
        protected $type = 'base';

		/**
		 * Constructor
		 */
		public function __construct()
		{
			add_action( 'wp_loaded', function(){

                $generated = get_option( "{$this->type}_generated" );
                
				if ( !$generated || ( isset( $_GET[ "regenerate_{$this->type}" ] ) ) ) {

					$generator = $this->generate();
					update_option( "{$this->type}_generated", $generator->get('postId') );
					
					$generators = get_option( 'abel_generators', [] );
					$generators[ $this->type ] = $generator->get('postId');
					update_option( 'abel_generators', $generators );
				}
			});			
		}


		/**
		 * Generate function
		 * 
		 * @return void
		 */
		public abstract function generate();

    }

