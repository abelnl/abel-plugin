<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ServicesGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'services';



		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'services' );
				$section->name( 'services' );

				$section->allowedColumns([ 'handpickedcollection', 'collection' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column('handpickedcollection')
							->view( 'overview' )
							->post_type( 'service' )
							->posts_per_page( 6 )
							->posts_per_row( 3 )
                            ->display_link( true )
				
				]);

			});
		}

	}

	\Abel\Generators\ServicesGenerator::getInstance();