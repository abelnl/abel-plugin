<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ProductsGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'products';



		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'products' );
				$section->name( 'products' );

				$section->allowedColumns([ 'collection', 'handpickedcollection' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column('collection')
							->post_type( 'product' )
							->posts_per_page( 6 )
							->posts_per_row( 3 )
                            ->display_link( true )
				
				]);

			});
		}

	}

	\Abel\Generators\ProductsGenerator::getInstance();