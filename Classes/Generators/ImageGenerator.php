<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ImageGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'image';
		

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'image-bg-fixed' );
				$section->name( 'image-bg-fixed' );

				$section->allowedColumns([ 'image' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'image' )
				
				]);

			});
		}

	}

	\Abel\Generators\ImageGenerator::getInstance();