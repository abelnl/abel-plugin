<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;
	use ChefSections\Generators\ColumnGenerator;
	use ChefSections\Generators\PageGenerator;

	class HeroGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'hero';
				

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::page( 'Hero', function( PageGenerator $blueprint ){
				
				$blueprint->post(['post_title' => 'hero', 'post_type' => 'section-template']);
				$blueprint->post_type('section-template');

				$blueprint->sectionContainer( 'Hero slider', [

					'name' => 'hero',
					'type' => 'container',
					'slug' => 'tabs',
					'view' => 'tabbed',
					'allowedColumns' => [ 'cta' ],
					'allowedViews' => ['fullwidth'],

					'sections' => [

						$blueprint->section( 'Ons bedrijf', [
							'view' => 'fullwidth',
							'container_width' => '100%',
							'columns' =>[ 	
                                new ColumnGenerator( array( 'type' => 'cta' ) )
                            ],
                            'background_opacity' => 'a_opacity-100',
                            'background_alignment' => 'a_cover-cc'
						]),

						$blueprint->section( 'Onze diensten', [
							'view' => 'fullwidth',
							'container_width' => '100%',
							'columns' =>[ 	
                                new ColumnGenerator( array( 'type' => 'cta' ) )
                            ],
                            'background_opacity' => 'a_opacity-100',
                            'background_alignment' => 'a_cover-cc'
						])
					]
				]);
			});
		}

	}

	\Abel\Generators\HeroGenerator::getInstance();