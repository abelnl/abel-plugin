<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ImageGalleryGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'image-gallery';
				
		


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'image-gallery' );
				$section->name( 'image-gallery' );

				$section->allowedColumns([ 'gallery' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'gallery' )
				
				]);

			});
		}

	}

	\Abel\Generators\ImageGalleryGenerator::getInstance();