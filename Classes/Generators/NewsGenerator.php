<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class NewsGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'news';
				


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'news' );
				$section->name( 'news' );

				$section->allowedColumns([ 'handpickedcollection', 'collection' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column('handpickedcollection')
							->post_type( 'post' )
							->posts_per_page( 6 )
							->posts_per_row( 3 )
                            ->display_link( true )
				
				]);

			});
		}

	}

	\Abel\Generators\NewsGenerator::getInstance();