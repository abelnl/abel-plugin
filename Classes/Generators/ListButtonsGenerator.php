<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ListButtonsGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'listbuttons';
		

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'listbuttons' );
				$section->name( 'listbuttons' );

				$section->allowedColumns([ 'button-repeater' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'button-repeater' )
				
				]);

			});
		}

	}

	\Abel\Generators\ListButtonsGenerator::getInstance();