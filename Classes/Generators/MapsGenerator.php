<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class MapGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'map';
				

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'map' );
				$section->name( 'map' );

				$section->allowedColumns([ 'map' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'map' )
				
				]);

			});
		}

	}

	\Abel\Generators\MapGenerator::getInstance();