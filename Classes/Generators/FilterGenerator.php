<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class FilterGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'filter';


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->name( 'filter' );

				$section->allowedColumns([ 'filter' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column( 'filter' )
				
				]);

			});
		}

	}

	\Abel\Generators\FilterGenerator::getInstance();