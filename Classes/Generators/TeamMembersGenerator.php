<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class TeamMembersGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'teammembers';



		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'fullwidth' );
				$section->class( 'team' );
				$section->name( 'team' );
				$section->post(['post_title' => 'teammembers']);
				
				$section->allowedColumns([ 'handpickedcollection', 'collection' ]);
				$section->allowedViews([ 'fullwidth' ]);

				$section->columns([

					$section->column('handpickedcollection')
							->post_type( 'teammember' )
							->posts_per_page( 6 )
							->posts_per_row( 3 )
                            ->display_link( true )
				
				]);

			});
		}

	}

	\Abel\Generators\TeamMembersGenerator::getInstance();