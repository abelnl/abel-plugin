<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class VacGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'vac';

	

		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->post(['post_title' => 'visual and content']);
				$section->view( 'half-half' );
				$section->class( 'vac' );
				$section->name( 'vac' );

				$section->allowedColumns([ 'image', 'content' ]);
				$section->allowedViews([ 'half-half' ]);

				$section->columns([

					$section->column('image'),
							
					$section->column('content')
							->title(['text' => '', 'type' => 'h2'])
							->content('')
				
				]);

			});
		}

	}

	\Abel\Generators\VacGenerator::getInstance();