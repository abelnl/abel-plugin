<?php

	namespace Abel\Generators;

	use Abel\Front\Settings;
	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\PageGenerator;
	use ChefSections\Generators\ColumnGenerator;

	class OverviewPagesGenerator extends StaticInstance{


		/**
		 * Constructor
		 */
		public function __construct()
		{

			add_action( 'wp_loaded', function(){

				$generated = get_option( 'overviews_generated' );
				if ( !$generated || ( isset( $_GET['regenerate_overviews'] ) ) ) {
					$this->generate();
					update_option( 'overviews_generated', true );
				}

			});		
		}


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{

			$pages = [
				'post' 		=> __( 'Actueel', 'abelplugin' ),
				'reference'	=> __( 'Projecten', 'abelplugin' ),
				'service'	=> __( 'Diensten', 'abelplugin' )
			];

			$names = [ 
				'post' => 'news',
				'reference' => 'references',
				'service' => 'services'
			];


			$generated = [];
			foreach( $pages as $postType => $title ){

				$name = $names[ $postType ];

				$generated[] = Generator::section( $title, function( $section ) use ( $postType, $title, $name ){

					$section->post([
						'title' => $title,
						'type' => 'page'
					]);

					$section->post_type('page');

					$section->view( 'fullwidth' );
					$section->class( $name );
					$section->name( $name );
					$section->allowedColumns([ 'collection' ]);
					$section->allowedViews([ 'fullwidth' ]);
					$section->columns([

					$section->column('collection')
							->post_type( $postType )
							->posts_per_page( 6 )
							->posts_per_row( 3 )
					]);

				});
			}

			/**
			 * Update the settings:
			 */
			$settings = Settings::findSettings();

			foreach( $generated as $gen ){
				$settings[ $gen->get('name').'-page' ] = $gen->get('postId');
			}

			update_option( 'website-settings', $settings );
		}

	}

	\Abel\Generators\OverviewPagesGenerator::getInstance();