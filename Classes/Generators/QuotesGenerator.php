<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;
	use ChefSections\Generators\ColumnGenerator;
	use ChefSections\Generators\PageGenerator;

	class QuotesGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'quotes';


		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::page( 'Quotes', function( PageGenerator $blueprint ){
				
				$blueprint->post(['post_title' => 'quotes', 'post_type' => 'section-template']);
				$blueprint->post_type('section-template');

				$blueprint->sectionContainer( 'Quotes slider', [

					'name' => 'quotes',
					'type' => 'container',
					'slug' => 'tabs',
					'view' => 'tabbed',
					'allowedColumns' => [ 'quote' ],
					'allowedViews' => [ 'fullwidth' ],
					'sections' => [

						$blueprint->section( 'Quote 1', [
							'view' => 'fullwidth',
							'columns' =>[ 	new ColumnGenerator( array( 'type' => 'quote' ) ) 
							]
						]),

						$blueprint->section( 'Quote 2', [
							'view' => 'fullwidth',
							'columns' =>[ 	new ColumnGenerator( array( 'type' => 'quote' ) ) 
							]
						]),
					]
				]);
			});
		}

	}

	\Abel\Generators\QuotesGenerator::getInstance();