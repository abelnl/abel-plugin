<?php

    namespace Abel\Generators;

    class Handler{
        
        /**
         * Regenerate all generators
         *
         * @return void
         */
        public function regenerateAll(){
        
            $generated = get_option( 'abel_generators', [] );
            if( empty( $generated ) )
                $this->removeOldGenerators();

            foreach( $generated as $key => $postId ){

                wp_delete_post( $postId, true );
                delete_option( "{$key}_generated" );
                unset( $generated[ $key ] );
            }

            update_option( 'abel_generators', $generated );
        }


        /**
         * Regenerate a single section
         *
         * @return void
         */
        public function regenerate( $name ){
            
            $generated = get_option( 'abel_generators', [] );
            if( empty( $generated ) )
            $this->removeOldGenerators( $name );

            if( isset( $generated[ $name ] ) ){
                wp_delete_post( $generated[ $name ], true );
                delete_option( "{$name}_generated" );
                unset( $generated[ $name ] );
                update_option( 'abel_generators', $generated );
                return true;   
            }

            return false;
        }


        /**
         * Deprecated way of doing things:
         *
         * @return void
         */
        public function removeOldGenerators( $name = null ){

            $generators = [
                'brands',
                'brands-alt',
                'cta',
                'filter',
                'hero',
                'hero-alt',
                'image-gallery',
                'image',
                'listbuttons',
                'map',
                'news',
                'products',
                'quote',
                'quotes',
                'references',
                'services',
                'teammembers',
                'toptasks',
                'vac'
            ];

            if( is_null( $name ) ){
                
                echo "removing old generators...\n\r";

                foreach( $generators as $generator ){

                    delete_option( "{$generator}_generated" );
                    $obj = get_page_by_path( $generator, OBJECT, 'section-template' );

                    if( !is_null( $obj ) ){
                        echo $obj->post_title." deleted.\n";
                        wp_delete_post( $obj->ID, true );
                    }
                }

            }else{
                delete_option( "{$name}_generated" );
            }

        }

    }