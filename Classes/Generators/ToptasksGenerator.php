<?php

	namespace Abel\Generators;

	use Abel\Wrappers\StaticInstance;
	use ChefSections\Wrappers\Generator;
	use ChefSections\Generators\SectionGenerator;

	class ToptasksGenerator extends BaseGenerator{
		
		/**
		 * Type of generator
		 *
		 * @var string
		 */
		protected $type = 'toptasks';



		/**
		 * Generate
		 * 
		 * @return void
		 */
		public function generate()
		{
			return Generator::section( 'blueprint', function( SectionGenerator $section ){

				$section->view( 'three-columns' );
				$section->class( 'toptasks' );
				$section->name( 'toptasks' );

				$section->allowedColumns([ 'toptask' ]);
				$section->allowedViews([ 'half-half', 'three-columns', 'four-columns' ]);

				$section->columns([

					$section->column('toptask')
							->title( [
								'text' => 'Bel ons',
								'type' => 'h3'
								]
							)
							->button([
                                'link' => 'tel::0612345678',
                                'text' => '0031(6)12345678',
                                'icon' => '',
                                'class' => '',
                                'iconClass' => '',
                            ]),
                            
					$section->column('toptask')
							->title( [
								'text' => 'Mail ons',
								'type' => 'h3'
								]
                            )
                            ->button([
                                'link' => 'mailto::info@domain.com',
                                'text' => 'info@domain.com',
                                'icon' => '',
                                'class' => '',
                                'iconClass' => '',
                            ]),

					$section->column('toptask')
							->title( [
								'text' => 'Kom langs',
								'type' => 'h3'
								]
                            )
                            ->button([
                                'link' => 'https://googlemaps.com/route=',
                                'text' => 'bekijk de route',
                                'icon' => '',
                                'class' => '',
                                'iconClass' => '',
                            ])
				]);

			});
		}

	}

	\Abel\Generators\ToptasksGenerator::getInstance();