<?php

	namespace Abel\Templates;

	class CollectionTemplate extends SectionTemplate{


		/**
		 * Overwrite the display function
		 * 
		 * @return String
		 */
		public function display()
		{
			$type = 'section';
			$section = $this->object;
			$view = new LayoutHandler( $section );
			$column = $view->getColumn();
			$query = $column->getQuery();

			add_action( 'chef_sections_before_'.$this->type.'_template', $this->object );

			include( $this->located );

			add_action( 'chef_sections_after_'.$this->type.'_template', $this->object );		
		
			//enqueue scripts:
			$this->enqueueScripts();
		}

	}