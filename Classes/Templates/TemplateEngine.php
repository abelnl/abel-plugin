<?php

	namespace Abel\Templates;

	use Abel\Wrappers\StaticInstance;

	class TemplateEngine extends StaticInstance{


		/**
		 * Constructor
		 */
		public function __construct()
		{
			$this->listen();
		}


		/**
		 * Listen to template classes being called
		 * 
		 * @return void
		 */
		public function listen()
		{
			add_filter( 'chef_sections_section_template_class', function( $class, $section ){

				$templates = $this->getTemplates();
				$key = $this->getKey( $section );

				if( !is_null( $key ) && isset( $templates[ $key ] ) )
					return '\\Abel\\Templates\\'.$templates[ $key ];


				return '\\Abel\\Templates\\SectionTemplate';

			}, 200, 2);
		}


		/**
		 * Returns an array of possible templates
		 * 
		 * @return Array
		 */
		public function getTemplates()
		{
			return [
				'collection' 	=> 'CollectionTemplate',
				'grid' 			=> 'GridTemplate',
			];			
		}


		/**
		 * Returns the key for this section template
		 * 
		 * @param  Section $section
		 * 
		 * @return String
		 */
		public function getKey( $section )
		{

			$column = $section->getColumn();
			$types = [ 'collection', 'handpickedcollection', 'gallery' ];

			if( !is_null( $column ) && !empty( $column ) && in_array( $column->type, $types ) ){

				if( $this->isGridSection( $column ) )
					return 'grid';

				if( $column->type !== 'gallery' )
					return 'collection';

			}

			return null;
		}


		/**
		 * Returns wether or not this is a grid
		 * 
		 * @param  Column  $column
		 * 
		 * @return boolean
		 */
		public function isGridSection( $column )
		{
			$pts = [ 'service' ];
			$view = ( $column->getField( 'view' ) == 'grid' ? true : false );

			if( $view && $column->type == 'gallery' )
				return true;

			//check if post-types is an array and return true if a grid-post-type is present:
			if( $view && is_array( $column->getField( 'post_type' ) ) ){

				foreach( $column->getField( 'post_type' ) as $pt ){
					if( in_array( $pt, $pts ) )
						return true;
				}

			}

			//return true if the post type is a grid post type
			if( $view && in_array( $column->getField( 'post_type'), $pts ) )
				return true;
			
			return false;
		}

	}

	\Abel\Templates\TemplateEngine::getInstance();