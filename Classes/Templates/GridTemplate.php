<?php

	namespace Abel\Templates;

	use Cuisine\Wrappers\Template;
	use Cuisine\Utilities\Sort;

	class GridTemplate extends CollectionTemplate{


		/**
		 * Overwrite the display function
		 * 
		 * @return String
		 */
		public function display()
		{
			$type = 'section';
			$section = $this->object;
			$view = new LayoutHandler( $section );
			$column = $view->getColumn();
			$items = $this->getGridItems();
			
			add_action( 'chef_sections_before_'.$this->type.'_template', $this->object );

			include( $this->located );

			add_action( 'chef_sections_after_'.$this->type.'_template', $this->object );		
		
			//enqueue scripts:
			$this->enqueueScripts();

		}

		/**
		 * Return loose items in grid
		 * 
		 * @return Array
		 */
		public function getGridItems()
		{
			$items = $this->getItemsFromSection();
			$items = Sort::byField( $items, 'position' );
			$gridItems = [];
			$sizes = [ 2, 1, 4 ];
			$currSize = 0;

			if( !empty( $items ) ){

				$i = 1;
				foreach( $items as $item ){

					//set grid item
					$gridItems[ $i ][] = $item;

					//unless current size is already a key
					//and the count is full:
					if( 
						isset( $gridItems[ $i ] ) && 
						count( $gridItems[ $i ] ) == $sizes[ $currSize ]
					){
						$i++;
						$currSize++;
					}


					if( $currSize == 3 )
						$currSize = 0;

				}

			}

			return $gridItems;
		}



		/**
		 * Returns the items from section
		 * 
		 * @return Array
		 */
		public function getItemsFromSection()
		{
			$column = $this->object->getColumn();

			if( $column->type == 'gallery' )
				return $column->getField( 'media' );

			$query = $column->getQuery();
			return $query->posts;
		}

		/**
		 * Returns the default hierarchy for this template
		 * 
		 * @return Array
		 */
		public function getName()
		{

			$name = $this->object->name;

			//remove initial grid mentions:
			$name = str_replace( '-grid', '', $name );

			//return a clean version:
			return $name.'-grid';
		}


	}