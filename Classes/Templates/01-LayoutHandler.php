<?php

	namespace Abel\Templates;

	use Cuisine\View\Image;
	use Abel\Front\Settings;
	use Abel\Helpers\Pagination;
	use ChefSections\Front\Walker;
	use Cuisine\Wrappers\Template;
	use Abel\Helpers\Image as AbelImage;
	use Abel\Helpers\Section as SectionHelper;
	use ChefSections\Wrappers\Template as ColumnTemplate;

	class LayoutHandler{


		/**
		 * Section object
		 * 
		 * @var Section
		 */
		protected $section;


		/**
		 * Constructor
		 *
		 * @param Section $section
		 *
		 * @return void
		 */
		public function __construct( $section )
		{
			$this->section = $section;
		}




		/*******************************************/
		/**         Class                          */
		/*******************************************/

		/**
		 * Returns a custom id
		 * 
		 * @return String
		 */
		public function id( $section = null )
		{
			if( is_null( $section ) )
				$section = $this->section;

			global $post;
			return $post->post_name.'-'.$section->name.'-'.$section->id;
		}

		/**
		 * Returns a section id
		 * 
		 * @return String
		 */
		public function s_id( $section = null )
		{
			if( is_null( $section ) )
				$section = $this->section;

			global $post;
			return $post->post_name.'-'.$section->name;
		}

		
		/**
		 * Returns all classes for a section
		 * 
		 * @param  array  $sizes   
		 * @param  string $default 
		 * 
		 * @return String
		 */
		public function class( $default = '' )
		{
			$classes = explode( ' ', $default );

			//sizes
			$sizeClasses = $this->sizeClasses();

			//background color:
			$bg = $this->section->getProperty('backgroundColor', 'none' );
			if( $bg !== 'none' )
				$classes[] = 'a_bg_'.$bg;

			$classes = array_merge( $classes, $sizeClasses );
			return implode( ' ', $classes );
		}


		/**
		 * Returns all container classes for a section
		 * 
		 * @param  array  $sizes   
		 * @param  string $default 
		 * 
		 * @return String
		 */
		public function containerClass( $default = '' )
		{
			$classes = explode( ' ', $default );


			$tc = $this->section->getProperty('container_textColor', 'none' );
			if( $tc == 'none' || $tc == '' )
				$tc = Settings::get('default_text_color');

			$classes[] = 'a_text_'.$tc;

			$fc = $this->section->getProperty('container_fillColor', 'none' );
			if( $fc == 'none' || $fc == '' )
				$fc = Settings::get('default_fill_color');

			$classes[] = 'a_fill_'.$fc;
		
			return implode( ' ', $classes );
		}



		/**
		 * Title Class
		 * 
		 * @return String
		 */
		public function titleClass()
		{
			$classes = [];
			$classes[] = $this->section->getProperty( 'title_alignment' );

			return implode( ' ', $classes );
		}


		/**
		 * Creates the size classes for a section
		 * 
		 * @return String
		 */
		public function sizeClasses()
		{

			$classes = [];

			//set the sizes array:
			$sizes = [

				'paddingTop' => [
					'class' => 'padding-top', 
					'default' => Settings::get('default_padding-top')
				],

				'paddingBottom' => [
					'class' => 'padding-bottom', 
					'default' => Settings::get('default_padding-bottom')
				],

				'marginTop' => [ 
					'class' => 'margin-top', 
					'default' => Settings::get('default_margin-top')
				],

				'marginBottom' => [ 
					'class' => 'margin-bottom', 
					'default' => Settings::get('default_margin-bottom')
				]
			];

			$i = 0;

			//loop through sizes, fall back on defaults if need be:
			foreach( array_keys( $sizes ) as $key ){

				$size = $this->section->getProperty( $key, $sizes[ $key ]['default'] );

				if( $size != (int)0 )
					$classes[] = 'a_'.$sizes[ $key ]['class'].'-'.$size;

				$i++;
			}

			return $classes;
		}

		/**
		 * Gets the container max width
		 * 
		 * @return Template
		 */
		public function containerMaxWidth()
		{
			return $this->section->getProperty( 'container_width' );
		}

		/**
		 * Gets the section animation
		 * 
		 * @return Template
		 */
		public function sectionAnimation()
		{
			$animation = $this->section->getProperty( 'section_animation' );
			if( $animation && $animation != 'none' && $animation != '' )
				return 'data-anime-type="'.$animation.'"';

			return 'data-anime-type="none"'; 
		}

		/**
		 * Gets the element animation
		 * 
		 * @return Template
		 */
		public function elementAnimation()
		{
			$animation = $this->section->getProperty( 'section_animation' );
			if( $animation && $animation != 'none' && $animation != '' )
				return 'data-anime-elem';

			return ''; 
		}

		/**
		 * Merge two arrays without keys
		 * 
		 * @param  Array  $overwrites
		 * 
		 * @return Array
		 */
		public function parseDefaultSizes( $overwrites = [] )
		{
			$defaults = [0,0,0,0];
			foreach( $defaults as $key => $default ){
				if( isset( $overwrites[ $key ] ) )
					$defaults[ $key ] = $overwrites[ $key ];
			}

			return $defaults;
		}


		/**
		 * Reset WP loops
		 * 
		 * @return void
		 */
		public function reset()
		{
			wp_reset_postdata();
			wp_reset_query();
			unset( $GLOBALS['image'] );
		}


		/*******************************************/
		/**         Type & amounts                 */
		/*******************************************/

		/**
		 * Returns the section type
		 * 
		 * @param  String $default
		 * 
		 * @return String
		 */
		public function type( $default = null )
		{
			//check custom field:
			if ( 
				$this->section->getProperty( 'classes' ) != '' &&
				!is_array( $this->section->getProperty( 'classes' ) )
			) {

				return $this->section->getProperty( 'classes' );
			}
 
			//check default:
			if( !is_null( $default ) )
				return $default;

			return $this->section->name;
		}


		/**
		 * Returns amounts per row
		 * 
		 * @param  Int $default
		 * 
		 * @return Int
		 */
		public function amount( $default = null )
		{
			$column = $this->getColumn();
			if( !is_null( $column ) && $this->isCollection( $column ) )
				return $column->getField('posts_per_row');

			if( !is_null( $default ) )
				return $default;

			$allowed = ['toptasks'];
			if( in_array( $this->section->name, $allowed ) )
				return count( $this->section->columns );


			return 4;	
		}

		/*******************************************/
		/**         Divider                        */
		/*******************************************/

		/**
		 * End a section
		 * 
		 * @return void
		 */
		public function end()
		{

			$this->maybeAddPagination();

			$this->reset();
			$this->divider();
			$this->backdrop();
		}


		/**
		 * Check if we need to display pagination in the end
		 * 
		 * @return String / void
		 */
		public function maybeAddPagination()
		{
			$column = $this->getColumn();

			if( !is_null( $column ) && $this->isCollection( $column ) ){
				if( $column->getField( 'nav' ) == 'pagination' && !$this->isSlider() ){

					$pagination = ( new Pagination() )->get( $column->getQuery() );
					echo $pagination;

				}
			}

			return '';
		}

		/**
		 * Return the section divider for this section 
		 *
		 * @param String $type
		 * 
		 * @return Template
		 */
		public function divider( $type = '' )
		{
			$divider = $this->section->getProperty( 'divider' );
			if( $divider != 'none' && $divider != '' ){

				$color = $this->section->getProperty( 'divider_color' );
				if( $color == 'none' ) $color = 'primary';

				$divider = 'content-section-divider-'.$divider;

				//display template:
				Template::find( 'partials/section-dividers/'.$divider, null )->display([
					'section_divider_color' => $color
				]);

			}		
		}

		/*******************************************/
		/**             Title          		       */
		/*******************************************/

		/**
		 * Loads the title partial
		 * 
		 * @return Template
		 */
		public function title()
		{
			$titleForSection = $this->section;
			$view = $this;
			
			if( !is_null( $titleForSection->getTitle() ) ){
			
				ob_start();

					Template::find( 'partials/title', null )->display([ 
						'view' => $view,
						'title' => $titleForSection->getTitle()
					]);

				return ob_get_clean();
			
			}

			return '';
		}

		/**
		 * Gets the title max width
		 * 
		 * @return Template
		 */
		public function titleMaxWidth()
		{
            $classes = [];
            $classes[] =  $this->section->getProperty( 'title_width' );
            
			$tc = $this->section->getProperty('textColor', 'none' );
			if( $tc == 'none' )
				$tc = 'default';

			$classes[] = 'a_text_'.$tc;

			$fc = $this->section->getProperty('fillColor', 'none' );
			if( $fc == 'none' )
				$fc = 'default';

			$classes[] = 'a_fill_'.$fc;
            return implode( ' ', $classes );
		}

		/**
		 * Gets the title button(s)
		 * 
		 * @return Template
		 */
		public function titleButton()
		{
			$button = $this->section->getProperty( 'title_link' );
			if (is_array($button) && count($button) > 0) {
				return $button;
			}
			return false;
		}


		/*******************************************/
		/**             Backgrounds                */
		/*******************************************/

		/**
		 * Loads the backdrop partial
		 * 
		 * @return Template
		 */
		public function backdrop( $backdropForSection = null )
		{
			if( is_null( $backdropForSection ) ){
				$backdropForSection = $this->section;
				$view = $this;
			}else{
				$view = new LayoutHandler( $backdropForSection );	
			}


			if( $view->hasBackdrop() ){

				Template::find( 'partials/backdrop', null )->display([ 
					'view' => $view
				]);
			
			}
		}

		/**
		 * Does this section have a backdrop
		 *
 		 * @return boolean
		 */
		public function hasBackdrop()
		{

			$bg = $this->getBackground();
			$vid = $this->getBackgroundVideo();

			if( is_null( $bg ) && is_null( $vid ) )
				return false;

			return true;
		}

		/**
		 * Returns the backdrop
		 * 
		 * @return Array
		 */
		public function getBackdrop()
		{
			$output = null;
			$bg = $this->getBackgroundSizes();
			$vid = $this->getBackgroundVideo();
			$blendMode = $this->section->getProperty( 'background_blendmode', '' );
			$grayscale = $this->section->getProperty( 'background_grayscale', '' );
			$opacity = $this->section->getProperty( 'background_opacity' );
			$containerClass = '';
			
			$imgClass = ($blendMode && $blendMode != '' && $blendMode != 'none')? ' '.$blendMode : '';
			$imgClass .= ($grayscale && $grayscale != '' && $grayscale != 'none')? ' '.$grayscale : '';

			//background color:
			$bgColor = $this->section->getProperty('backgroundColor', 'none' );
			if( $bgColor !== 'none' ){
				$containerClass = ' a_bg_'.$bgColor;
				//$bgColor = Settings::get( 'default_background_color' );
			}
			
			


			if( !is_null( $vid ) )
				$output = [ 'type' => 'video', 'url' => $vid, 'containerClass' => $containerClass ];
			
			if( !is_null( $bg ) ) {
				
				//make sure we see the image:
				//if( $opacity == 'a_opacity-100' && $bgColor != 'none' )
				//	$opacity = 'a_opacity-20';
				
				$imgClass .= ' '.$opacity;

				$imgClass .= ' '.$this->getBackgroundAlignment();
				$output = [ 'type' => 'image', 'imageClass' => $imgClass, 'containerClass' => $containerClass ];
				$output = array_merge( $output, $bg );
			}

			return $output; 
		}

		/**
		 * Return background image sizes
		 * 
		 * @return Array
		 */
		public function getBackgroundSizes( $section = null )
		{
			if( is_null( $section ) )
				$section = $this->section;

			$bg = $section->getProperty( 'background' );
			if( !empty( $bg['img-id'] ) ){

				$id = $bg['img-id'];

				return [
					'url' => AbelImage::url( $id, '1600w' ),
					'1600w' => AbelImage::url( $id, '1600w' ),
					'1000w' => AbelImage::url( $id, '1000w' ),
					'600w' => AbelImage::url( $id, '600w' )
				];
			}

			return null;
		}


		/**
		 * Get the background attribute
		 * 
		 * @return String
		 */
		public function getBackground( $size = '1600w', $section = null )
		{

			if( is_null( $section ) )
				$section = $this->section;

			$bg = $section->getProperty( 'background' );
			if( !empty( $bg['img-id'] ) ){

				$url = Image::getMediaUrl( $bg['img-id'], $size );
				if( !$url || $url == '' )
					$url = Image::getMediaUrl( $bg['img-id'], 'full' );

				return $url; 
			}

			return null;
		}

		/**
		 * Get the background attribute
		 * 
		 * @return String
		 */
		public function getBackgroundId( $section = null )
		{

			if( is_null( $section ) )
				$section = $this->section;

			$bg = $section->getProperty( 'background' );
			if( !empty( $bg['img-id'] ) ){
				return $bg['img-id'];
			}

			return null;
		}

		/**
		 * Return the background alignment
		 * 
		 * @param  Object $section
		 * 
		 * @return string
		 */
		public function getBackgroundAlignment( $section = null ) {
			if( is_null( $section ) )
				$section = $this->section;

			$alignment = $section->getProperty( 'background_alignment' );
			if( !$alignment || $alignment == '' ){
				$alignment = 'a_cover-cc';
			}
			return $alignment;
		}

		/**
		 * Returns the link to the background video
		 * 
		 * @return String
		 */
		public function getBackgroundVideo()
		{
			$vid = $this->section->getProperty( 'video_url' );
			if( $vid && $vid != '' )
				return $vid;

			return null;
		}


		/*******************************************/
		/**           Columns                      */
		/*******************************************/


		/**
		 * Displays the column templates
		 * 
		 * @return String
		 */
		public function columns()
		{
			ob_start();

			$i = 0;
			foreach( $this->section->columns as $column ){

				echo '<div class="'.$this->columnClass( $column->position ).'">';
					ColumnTemplate::column( $column )->display();
				echo '</div>';

				$i++;
			}

			return ob_get_clean();
		}

		/**
		 * Wether or not to show collection links
		 * 
		 * @return bool
		 */
		public function showCollectionLinks() {
			//check collections first:
			$column = $this->getColumn();
			

			if( !$column->getField( 'display_link' ) || $column->getField( 'display_link' )  == 'false' ){
				return false;
			}
			return true;
		}


		/**
		 * Returns the content of a section
		 * 
		 * @param  Section $section
		 * 
		 * @return Column | array of columns
		 */
		public function getColumn()
		{
			if( sizeof( $this->section->columns ) == 1 )
				return $this->section->getColumn();

			return null;
		}


		/**
		 * Returns the column class of a given column
		 * 
		 * @return String
		 */
		public function columnClass( $columnId = 1, $prefix = 'ac_content_' )
		{
			if( !isset( $this->section->view ) )
				return $prefix;

			switch( $this->section->view ){

				case 'fullwidth':
					return $prefix.'1-1';
					break;

				case 'half-half':
					return $prefix.'1-2';
					break;

				case 'three-columns':
					return $prefix.'1-3';
					break;

				case 'four-columns':
					return $prefix.'1-4';
					break;

				case 'sidebar-left':
					return $prefix.( $columnId == 1 ? '1-3' : '2-3' );
					break;

				case 'sidebar-right':
					return $prefix.( $columnId == 1 ? '2-3' : '1-3' );
					break;
				case 'sidebar-left':
					return $prefix.( $columnId == 1 ? '1-3' : '2-3' );
					break;

				case 'three-columns-left':
					return $prefix.( $columnId == 1 ? '1-2' : '1-4' );
					break;
					
				case 'three-columns-middle':
					return $prefix.( $columnId == 2 ? '1-2' : '1-4' );
					break;

				case 'three-columns-right':
					return $prefix.( $columnId == 3 ? '1-2' : '1-4' );
					break;

				default:
					return $prefix.'2-3';
					break;
			}
		}


		/**
		 * Returns wether or not this is a slider
		 * 
		 * @return boolean
		 */
		public function isSlider()
		{
			//check collections first:
			$column = $this->getColumn();
			if( 
				!is_null( $column ) && 
				( 
					$this->isCollection( $column ) ||
				  	$column->type == 'gallery'
				)
			){

				if( $column->getField( 'view', 'grid' ) == 'slider' ){
					wp_enqueue_script( 'section-slick' );
					return true;
				}
			} else if ( $this->section && $this->section->type == 'container' ) {
				return true;
			}


			return false;
		}


		/**
		 * Check if we're a hero and if we're linking to content
		 * 
		 * @return Bool
		 */
		public function heroLinkToContent()
		{
			if( (string)$this->section->getProperty( 'linkToContent' ) == 'true' )
				return true;

			return false;
		}

		/**
		 * Check if we're a hero and if we're linking to content
		 * 
		 * @return Bool
		 */
		public function heroShowArrows()
		{
			if( (string)$this->section->getProperty( 'showArrows' ) == 'true' )
				return true;

			return false;
		}

		/**
		 * Check if we're a hero and if we're linking to content
		 * 
		 * @return Bool
		 */
		public function heroShowNavigation()
		{
			if( (string)$this->section->getProperty( 'showNavigation' ) == 'true' )
				return true;

			return false;
		}

		

		/**
		 * Returns bool, show the navigation dots yes or no
		 * 
		 * @return bool, indicator to show navigation dots.
		 */
		public function showNavigation()
		{
			$column = $this->getColumn();
			
			if( 
				!is_null( $column ) && ( $this->isCollection( $column ) || $column->type == 'gallery' )
			){
				$nav = $column->getField( 'nav', 'none' );
				return $nav == 'pagination'?'true':'false';
			}

			return 'false';

		}

		/**
		 * Returns bool, show the navigation arrows yes or no
		 * 
		 * @return bool, indicator to show navigation arrows.
		 */
		public function showNavigationArrows()
		{
			$column = $this->getColumn();
			
			if( 
				!is_null( $column ) && ( $this->isCollection( $column ) || $column->type == 'gallery' )
			){
				$nav = $column->getField( 'nav_arrows', 'none' );
				return $nav == 'arrows';
			}

			return false;

		}

		/**
		 * Returns the slide amount
		 * 
		 * @param   $type [description]
		 * @return [type]       [description]
		 */
		public function getSlideAmount( $type = 'show' )
		{
			$column = $this->getColumn();
			
			if( 
				!is_null( $column ) && 
				($this->isCollection( $column ) || $column->type == 'gallery')
			){

				switch( $type ){

					case 'scroll':
						return $column->getField( 'posts_to_scroll', 1 );
						break;

					default:
						return $column->getField( 'posts_per_row', 1 );
						break;
				}

			//else return section values:
			}

		}


		/**
		 * Returns the slider time and auto-slide:true if set.
		 *
		 * @return String
		 */
		public function getSliderTime(){
			
			$column = $this->getColumn();
			if( is_null( $column ) ){
				$time = 0;
			
			}else{
				$time = $column->getField( 'slider-time', 0 ) ?? 0;
			
			}


			if( $time > 0 ){
				return "\"autoplay\": true,\n\"autoplaySpeed\": {$time},\n";
			}
		}


		/**
		 * Check if this is a content section
		 * 
		 * @return boolean
		 */
		public function isContentSection()
		{
			$generated = SectionHelper::generated();

			if( in_array( $this->section->name, $generated ) )
				return false;

			return true;
		}

		/**
		 * Check if something is a collection
		 * 
		 * @param  Column  $column
		 * 
		 * @return boolean        
		 */
		public function isCollection( $column )
		{
			if( $column->type == 'collection' )
				return true;

			if( $column->type == 'handpickedcollection' )
				return true;

			return false;
		}


	}