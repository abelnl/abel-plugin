<?php 

	namespace Abel\Templates;

	use Cuisine\Utilities\Url;
	use Abel\Helpers\Section as SectionHelper;
	use ChefSections\Templates\BaseSectionTemplate;

	class SectionTemplate extends BaseSectionTemplate{

		/**
		 * Get the default base folder
		 * 
		 * @var string
		 */
		protected $baseFolder = 'partials/section-';


		/**
		 * Display the actual template
		 * 
		 * @return String (html, echoed)
		 */
		public function display()
		{
			
			$type = 'section';
			$section = $this->object;
			$view = new LayoutHandler( $section );
			$column = $view->getColumn();

			add_action( 'chef_sections_before_'.$this->type.'_template', $this->object );

			include( $this->located );

			add_action( 'chef_sections_after_'.$this->type.'_template', $this->object );

			//enqueue scripts:
			$this->enqueueScripts();
		}


		/**
		 * Returns the default hierarchy for this template
		 * 
		 * @return Array
		 */
		public function getHierarchy()
		{

			return [
				$this->constructPath( $this->getName() )
			];

			return parent::getHierarchy();
		}


		/**
		 * Get template name
		 * 
		 * @return String
		 */
		public function getName()
		{
            $name = apply_filters( 'abel_object_name', $this->object->name, $this->object );

			if( !is_null( $name ) && $this->isValidName( $name ) )
				return $name;

			//default to section-content
			return 'content';
		}


		/**
		 * Checks if a name is a generated section or not
		 * 
		 * @param  String  $name
		 * 
		 * @return boolean
		 */
		public function isValidName( $name )
		{
			$generated = SectionHelper::generated();

			if( in_array( $name, $generated ) )
				return true;

			return false;
		}

		/**
		 * Checks wether or not scripts exists for this section template,
		 * and include them.
		 * 
		 * @return String
		 */
		public function enqueueScripts()
		{
			$name = $this->getName();
			$name = 'section-'.$name;
			$name = str_replace( '-grid', '', $name );
			$path = Url::path( 'theme', 'scripts/'.$name.'.js' );


			if( file_exists( $path ) ){
				wp_enqueue_script( $name, Url::theme( 'scripts/'.$name.'.js' ), [ 'jquery' ], true, true );
			
			}else{

				$path = Url::path('theme-parent', 'scripts/'.$name.'.js' );
				if( file_exists( $path) ){
					wp_enqueue_script( $name, Url::parentTheme( 'scripts/'.$name.'.js' ), ['jquery'], true, true );
				}
			}

		}


	}