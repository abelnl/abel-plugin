<?php

    namespace Abel\Wrappers;
    
    class StaticInstance {
    
        /**
         * Static bootstrapped instance.
         *
         * @var \Abel\Wrappers\StaticInstance
         */
        public static $instance = null;
    
    
    
        /**
         * Init the Assets Class
         *
         * @return \Abel\Admin\Assets
         */
        public static function getInstance(){
    
            return static::$instance = new static();
    
        }
    
    
    } 