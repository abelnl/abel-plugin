<?php

	namespace Abel\Admin;

	use \Cuisine\Wrappers\Metabox;
	use \Cuisine\Wrappers\Field;
	use \Abel\Wrappers\StaticInstance;
	use \Abel\Front\Settings;

	class ProductMetaboxListeners extends StaticInstance{


		/**
		 * Init admin metaboxes
		 */
		function __construct(){

			$this->addMetabox();

		}


		/**
		 * Creates the metaboxes for this plugin
		 * 
		 * @return void
		 */
		private function addMetabox(){

			//$products = Settings::get( 'products' );

			//if ( $products == 'manual' ) {
				$fields = $this->getFields();
				Metabox::make( __('Productdata','abelplugin'), ['product'] )->set($fields);
			//}
		}

		/**
		* Get the fields for this posttype
		* 
		* @return array all fields for the metabox
		*/
		private function getFields(){

			$fields = array(
				Field::checkbox( 
					'sale', 
					__('Aanbieding','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __( 'Aanbieding','abelplugin' )
					)
				),
				Field::text( 
					'old_price', 
					__('Oude prijs','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Oude prijs','abelplugin')
					)
				),
				Field::text( 
					'price', 
					__('Prijs','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Prijs','abelplugin')
					)
				), 
				Field::text( 
					'url', 
					__('Product url','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Product url','abelplugin')
					)
				),
				Field::textarea(
					'description',
					__( 'Korte beschrijving', 'abelplugin' ),
					array(
						'placeholder' => __( 'Typ hier de korte beschrijving van dit productgeneratorgeneratorgenerator', 'abelplugin' )
					)
				)
			);

			return $fields;

		}
	}

	if( is_admin() )
		\Abel\Admin\ProductMetaboxListeners::getInstance();
