<?php

	namespace Abel\Admin;

	use \Abel\Front\Settings;
	use \Cuisine\Utilities\Url;
	use \Cuisine\Utilities\Logger;
	use \Abel\Wrappers\StaticInstance;

	class BroadcastMedia extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->broadcast();
		}


		/**
		 * Listen for broadcasting events
		 * 
		 * @return void
		 */
		public function broadcast()
		{

			//on staging & production, and on the main site:
			if( is_multisite() && is_main_site() ){

				if( 
					!get_option( 'abel-media-page', false ) || 
					isset( $_GET['regenerate_media_page' ] ) 
				){


					add_action( 'wp_loaded', function(){

						$args = [
							'post_title' => 'Abel Media Pagina',
							'post_type' => 'page',
							'menu_order' => 999,
							'post_status' => 'publish'
						];

						$pageId = wp_insert_post( $args );

						if( !is_wp_error( $pageId ) ){

							update_option( 'abel-media-page', $pageId );
							update_site_option( 'abel-media-page', $pageId );
						}
					});

				}


				if( isset( $_GET['setSiteOptionMedia'] ) ){
					$media = get_option( 'abel-media-page' );
					update_site_option( 'abel-media-page', $media );
				}


				/**
				 * Auto attach each non-attached media item:
				 */
				add_filter( 'wp_insert_attachment_data', function( $data, $post ){

					if( 
						$data['post_parent'] == 0 && 
						get_option( 'abel-media-page', false ) != false
					)
						$data['post_parent'] = get_option( 'abel-media-page' );

					return $data;

				}, 100, 2 );



				/**
				 * Trigger a re-broadcast of the media post,
				 * after an attachment was uploaded
				 */
				//if( function_exists( 'ThreeWP_Broadcast' ) ){

				//	add_action( 'add_attachment', [ $this, 'runBroadcast']);
				//	add_action( 'attachment_updated', [ $this, 'runBroadcast']);

				//}

			}
		}

		/**
		 * Run a broadcast event
		 * 
		 * @return void
		 */
		public function runBroadcast( $postId )
		{

			$_p = get_post( $postId );
			if( $_p->post_parent == get_option( 'abel-media-page' ) ){

				ThreeWP_Broadcast()->api()->update_children( $_p->post_parent );
				Logger::message( 'Global media broadcast ran for attachemnt #'.$postId );
			}
		}


	}

	//if( is_admin() )
		\Abel\Admin\BroadcastMedia::getInstance();