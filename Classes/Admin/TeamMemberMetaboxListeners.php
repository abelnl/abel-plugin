<?php

	namespace Abel\Admin;

	use \Cuisine\Wrappers\Metabox;
	use \Cuisine\Wrappers\Field;
	use \Abel\Wrappers\StaticInstance;

	class TeamMemberMetaboxListeners extends StaticInstance{


		/**
		 * Init admin metaboxes
		 */
		function __construct(){

			$this->addMetabox();

		}


		/**
		 * Creates the metaboxes for this plugin
		 * 
		 * @return void
		 */
		private function addMetabox(){

			Metabox::make( __('Avatar', 'abelplugin'), ['teammember'], ['context' => 'side'] )->set([
				Field::image(
					'avatar',
					'Avatar'
				)
			]);

			$fields = $this->getFields();
			Metabox::make( __('Persoonsgegevens','abelplugin'), ['teammember'] )->set($fields);

		}

		/**
		* Get the fields for this posttype
		* 
		* @return array all fields for the metabox
		*/
		private function getFields(){

			$fields = array(
				Field::text( 
					'jobtitle', 
					__('Functie','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __( 'Functie','abelplugin' )
					)
				),
				Field::text( 
					'facebook', 
					__('Facebook','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Facebook','abelplugin')
					)
				),
				Field::text( 
					'twitter', 
					__('Twitter','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Twitter','abelplugin')
					)
				), 
				Field::text( 
					'linkedin', 
					__('Linkedin','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Linkedin','abelplugin')
					)
				),
				Field::text( 
					'email', 
					__('E-mailadres','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('E-mailadres','abelplugin')
					)
				),
				Field::text( 
					'googleplus', 
					__('Google Plus','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Google Plus','abelplugin')
					)
				),
				Field::text( 
					'instagram', 
					__('Instagram','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Instagram','abelplugin')
					)
				),
				Field::text( 
					'telephone', 
					__('Telefoonnummer','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __('Telefoonnummer','abelplugin')
					)
				),
			);

			return $fields;

		}
	}

	if( is_admin() )
		\Abel\Admin\TeamMemberMetaboxListeners::getInstance();
