<?php

	namespace Abel\Admin;

	use \Cuisine\Wrappers\Metabox;
	use \Cuisine\Wrappers\Field;
	use \Abel\Wrappers\StaticInstance;

	class BrandMetaboxListeners extends StaticInstance{


		/**
		 * Init admin metaboxes
		 */
		function __construct(){

			$this->addMetabox();

		}


		/**
		 * Creates the metaboxes for this plugin
		 * 
		 * @return void
		 */
		private function addMetabox(){

			$sizes = [
				'default' => __( 'Originele afmetingen', 'abelplugin' ),
				'larger' => __( 'Groter', 'abelplugin' ),
				'smaller' => __( 'Kleiner', 'abelplugin' )
			];

			Metabox::make( 'Brand logo', ['brand'], ['context' => 'side'] )->set([

				Field::image(
					'logo',
					'Logo'
				),
				Field::select(
					'size',
					__( 'Afmetingen logo', 'abelplugin' ),
					$sizes,
					[ 'defaultValue' => 'default' ]
				),
				Field::image(
					'logo-alt',
					'Logo-alt'
				)
			]);

		}
	}

	if( is_admin() )
		\Abel\Admin\BrandMetaboxListeners::getInstance();
