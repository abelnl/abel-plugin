<?php

	namespace Abel\Admin;

	use \Cuisine\Utilities\Url;
	use \Abel\Wrappers\StaticInstance;
	use \Abel\Helpers\Icon;

	class Assets extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->enqueues();

		}

		/**
		 * Enqueue scripts & Styles
		 * 
		 * @return void
		 */
		private function enqueues(){

			/**
			 * Below are just some examples
			 */
			add_action( 'admin_menu', function(){

				$url = Url::plugin( 'abel', true ).'Assets/';

				//enqueue a script
				wp_enqueue_script( 
						'chosen_images', 
						$url.'js/ImageSelect.jquery.js', 
						[ 'jquery', 'chosen' ]
				);

				wp_enqueue_script( 'abel_admin', $url.'js/Admin.js',[
					'chosen_images',
					'sections_builder'
				]);

				//enqueue a stylesheet:
				wp_enqueue_style( 'abel_style', $url.'css/admin.css' );
				
			});
		}



	}

	if( is_admin() )
		\Abel\Admin\Assets::getInstance();
