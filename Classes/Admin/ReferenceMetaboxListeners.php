<?php

	namespace Abel\Admin;

	use \Cuisine\Wrappers\Metabox;
	use \Cuisine\Wrappers\Field;
	use \Abel\Wrappers\StaticInstance;

	class ReferenceMetaboxListeners extends StaticInstance{


		/**
		 * Init admin metaboxes
		 */
		function __construct(){

			$this->addMetabox();

		}


		/**
		 * Creates the metaboxes for this plugin
		 * 
		 * @return void
		 */
		private function addMetabox(){


			$fields = $this->getFields();
			Metabox::make( __('Referentie','abelplugin'), ['reference'] )->set($fields);

		}

		/**
		* Get the fields for this posttype
		* 
		* @return array all fields for the metabox
		*/
		private function getFields(){

			$fields = array(
				Field::text( 
					'location', 
					__('Locatie','abelplugin'),
					array(
						'label' 				=> false,
						'placeholder' 			=> __( 'Locatie','abelplugin' )
					)
				),
				Field::textarea(
					'description',
					__( 'Korte beschrijving', 'abelplugin' ),
					array(
						'placeholder' => __( 'Typ hier de korte beschrijving van dit project', 'abelplugin' )
					)
				)
			);

			return $fields;

		}
	}

	if( is_admin() )
		\Abel\Admin\ReferenceMetaboxListeners::getInstance();
