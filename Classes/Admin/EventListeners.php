<?php

	namespace Abel\Admin;

	use \Cuisine\Utilities\Url;
	use \Abel\Wrappers\Generators;
	use \Cuisine\Utilities\Session;
	use \Abel\Wrappers\StaticInstance;
	
	class EventListeners extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			$this->listen();
            $this->indicators();
            $this->settingPage();
		}

		/**
		 * Listen for admin events
		 * 
		 * @return void
		 */
		private function listen(){

			//allow SVG files to be uploaded:
			add_filter( 'upload_mimes', function( $mimes ){
				$mimes['svg'] = 'image/svg+xml';
 				return $mimes;
			});


			add_action( 'init', function(){

				//hide the "add container" button in sections:
				add_filter( 'chef_sections_show_container_ui', '__return_false' );

				if( isset( $_GET['regenerateAll'] ) )
					Generators::regenerateAll();

			});


			//don't show the add form button 
			add_filter( 'gform_display_add_form_button', '__return_false' );



			//add a mobile menu by default:
			if( !get_option( 'abel_mobile_menu' ) ){
				$menuId = wp_create_nav_menu( 'Mobile' );
				update_option( 'abel_mobile_menu', $menuId );
			} 

            add_filter( 'chef_sections_tab_container_button_label', function(){
                return 'Add slide';
            });

		}




		/**
		 * Listen for broadcast indicator events
		 * 
		 * @return void
		 */
		public function indicators()
		{
			//single page:
			add_action( 'chef_sections_before_section_toolbar', function(){

				$postId = Session::postId();
				$meta = get_post_meta( $postId, 'broadcast_child_original', true );

				if( $meta ){

					echo '<div class="broadcast-notice">';
						echo '<h3>'.__( 'Let op!', 'abelplugin' ).'</h3>';
						echo '<p>'.__( 'Dit is globale content die aangemaakt is vanuit Abel & Co. Wijzigingen aan deze pagina kunnen automatisch weer verdwijnen', 'abelplugin' ).'</p>';
					echo '</div>';

				}

			});

			/**
			 * Add a post class:
			 */
			add_filter( 'post_class', function( $classes, $class, $postId ){

				if( is_admin() ){

					$meta = get_post_meta( $postId, 'broadcast_child_original', true );
					if( $meta ){
						$mediaPage = get_site_option( 'abel-media-page' );
						$classes[] = 'broadcast-child';

						if( $meta == $mediaPage )
							$classes[] = 'media-page';
					}

				}

				return $classes;

			}, 100, 3 );
        }
        

        /**
         * Make sure values in the settings-page are escaped properly. Especially for field values.
         *
         * @return void
         */
        public function settingPage(){
            
            add_filter( 'cuisine_settings_page_data_to_save', function( $save, $page, $fields ){

                foreach( $fields as $field ){
        
                    $key = $field->name;
                    
                    //save the values of flex fields;
                    if( $field->type == 'flex' ){
        
                        $flexValues = $save[ $key ];
                        $value = [];


                        foreach( $flexValues as $fkey => $fv ){

                            //clear title text:
                            if( isset( $fv['title'] ) )
                                $fv['title']['text'] = stripcslashes( $fv['title']['text'] );                                                         

                            if( isset( $fv['textarea'] ) )
                                $fv['textarea'] = stripcslashes( $fv['textarea'] );
                            
                            //check the button:
                            if( isset( $fv['button'] ) ){
                                $fv['button']['link'] = stripcslashes( $fv['button']['link'] );
                                $fv['button']['text'] = stripcslashes( $fv['button']['text'] );
                            }

                            $value[ $fkey ] = $fv;
                        }

                        $save[ $key ] = $value;
                    }
                }

                return $save;

            }, 100, 3 );

        }


	}

	if( is_admin() )
		\Abel\Admin\EventListeners::getInstance();
