<?php

	namespace Abel\Admin;

	use \Cuisine\Wrappers\Metabox;
	use \Cuisine\Wrappers\Field;
	use \Abel\Wrappers\StaticInstance;

	class ServiceMetaboxListeners extends StaticInstance{


		/**
		 * Init admin metaboxes
		 */
		function __construct(){

			$this->addMetabox();

		}


		/**
		 * Creates the metaboxes for this plugin
		 * 
		 * @return void
		 */
		private function addMetabox(){

			Metabox::make( __('Omschrijving','abelplugin'), ['service'])->set([
				Field::icon( 
					'icon', 
					__('Icoon','abelplugin')
				),
				Field::textarea(
					'description',
					__( 'Omschrijving', 'abelplugin' )
				),
				Field::text(
					'subtitle',
					__( 'Ondertitel', 'abelplugin' )
				)
			]);

		}

	
	}

	if( is_admin() )
		\Abel\Admin\ServiceMetaboxListeners::getInstance();
