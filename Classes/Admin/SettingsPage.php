<?php

	namespace Abel\Admin;

	use \Abel\Helpers\Icon;
	use \Abel\Helpers\Pages;
	use \Abel\Helpers\Colors;
	use \Abel\Front\Settings;
	use \Abel\Helpers\Section;
	use \Cuisine\Utilities\Url;
	use \Cuisine\Utilities\Sort;
	use \Cuisine\Wrappers\Field;
	use \Cuisine\Utilities\Session;
	use \Abel\Helpers\Measurements;
	use \Abel\Helpers\HeaderLayouts;
	use \Abel\Helpers\FooterLayouts;
	use \Abel\Wrappers\StaticInstance;
	use \Cuisine\Wrappers\SettingsTab;
	use \Cuisine\Wrappers\SettingsPage;


	class SettingsPageBuilder extends StaticInstance{

		/**
		 * Init admin events & vars
		 */
		function __construct(){

			add_action( 'wp_loaded', function(){
				$this->settingsPage();
			});


			add_action( 'cuisine_after_settings_page_update', function( $settingsPage ){

				if( $settingsPage->getSlug() == 'website-settings' ){
                    
					wp_redirect( $settingsPage->getUrl() );
					exit();
				}

			});
			
		}


		/**
		 * The settcuisine_after_settings_page_updateingspage used by this plugin
		 *
		 * @return void
		 */
		private function settingsPage(){

			$tabs = $this->getTabs();

			$options = [
				'parent'		=> 'options-general.php',
				'menu_title'	=> __('Website instellingen', 'abelplugin')
			];

			SettingsPage::make(

				__( 'Website instellingen', 'abelplugin' ),
				'website-settings',
				$options

			)->set( $tabs );

		}

		/**
		 * Returns the tabs for this settingspage:
		 *
		 * @return array
		 */
		public function getTabs()
		{
			return [

				SettingsTab::make(
					__( 'Algemeen', 'abelplugin' ),
					$this->getGeneralFields()
				),

				SettingsTab::make(
					__( 'Menus', 'abelplugin' ),
					$this->getMenuFields()
				),

				SettingsTab::make(
					__( 'Footer', 'abelplugin' ),
					$this->getFooterFields()
				),

				SettingsTab::make(
					__( 'Style', 'abelplugin' ),
					$this->getStyleFields()
				),

				SettingsTab::make(
					__( 'Social media', 'abelplugin' ),
					$this->getSocialFields()
				),

				SettingsTab::make(
					__( 'Overzichtspagina\'s', 'abelplugin' ),
					$this->getOverviewFields()
				),

				SettingsTab::make(
					__( 'Configuratie', 'abelplugin' ),
					$this->getConfigurationFields()
				),

				SettingsTab::make(
					__( 'Vaste toptaken', 'abelplugin' ),
					$this->getTopTasksFields()
				)
			];
		}


		/**
		 * Return fields for the general tab:
		 *
		 * @return array
		 */
		private function getGeneralFields(){

			$fields = [

                Field::text( 
					'telephone', 
					__('Telefoonnummer wordt getoond in mobile header','abelplugin'),
                    [ 'defaultValue' => Settings::get( 'telephone' ) ]
				),
                /*Field::text( 
					'email', 
					__('E-mailadres','abelplugin'),
					[ 'defaultValue' => Settings::get( 'email' ) ]
				),*/
				Field::checkbox( 
					'cookie_show', 
					__('Show Cookie','abelplugin'),
                    [ 'defaultValue' => Settings::get( 'cookie_show' ) ]
				),
				Field::text( 
					'cookie_title', 
					__('Cookie title','abelplugin'),
                    [ 'defaultValue' => Settings::get( 'cookie_title' ) ]
				),
                Field::textarea( 
					'cookie_message', 
					__('Cookie message','abelplugin'),
					[ 'defaultValue' => Settings::get( 'cookie_message' ) ]
				),
				Field::text( 
					'cookie_buttontext', 
					__('Cookie button text','abelplugin'),
                    [ 'defaultValue' => Settings::get( 'cookie_buttontext' ) ]
				)

			];

			return $fields;
		}


		/**
		 * Return fields for the menu tab:
		 *
		 * @return array
		 */
		private function getMenuFields(){

            $menus = array(  
                '1' => [ 
					'label' => __('Horizontaal 1','abelplugin'),
					'areas' => 3
                ],
                '2' => [
                	'label' => __('Horizontaal 2','abelplugin'),
                	'areas' => 5
                ],
                '3' => [
                	'label' => __('Horizontaal 3','abelplugin'),
                	'areas' => 1

                ],
                '4' => [
                	'label' => __('Horizontaal 4','abelplugin'),
                	'areas' => 4
                ],
                '5' => [
                	'label' => __('Horizontaal 5','abelplugin'),
                	'areas' => 2
                ],
                '101' => [
                	'label' => __('Verticaal','abelplugin'),
                	'areas' => 2
                ]
            );

   			$menuId = (string)Settings::get( 'menutype', '1' );
			if( is_null( $menuId ) || $menuId == '' ) $menuId = '1';

			$fields = [
				Field::select(
                    'menutype',
                    __( 'Menu', 'abelplugin' ),
                    array_combine( array_keys( $menus ), Sort::pluck( $menus, 'label' ) ),
                    [ 'defaultValue' => $menuId ]
                ),
			];

			//add fields for the amount of zones in a menu:
			$amount = $menus[ $menuId ]['areas'];

			for( $i = 1; $i <= $amount; $i++ ){
                $fields[] = Field::flex(
                	'zone_'.$i,
                	__( 'Zone '.$i, 'abelplugin' ),
                	HeaderLayouts::getAll(),
                	[ 
                		'defaultValue' => Settings::get( 'zone_'.$i ),
                		'button_text' => __( 'Zone toevoegen', 'abelplugin' ),
                		'maxItems' => 2
                	]
                );
            }

			return $fields;
		}


		/**
		 * Return fields for the footer tab:
		 *
		 * @return array
		 */
		private function getFooterFields(){

            $footers = array(  
                '1' => [ 
					'label' => __('Footer 1','abelplugin'),
					'areas' => 4
                ],
                '2' => [
                	'label' => __('Footer 2','abelplugin'),
                	'areas' => 6
                ]
            );

            $templates = ['none' => __( 'Geen footer section' )] + Section::templates();


			$footerId = (string)Settings::get( 'footertype', 1 );
			if( is_null( $footerId ) || $footerId == '' ) $footerId = '1';

			$fields = [
				Field::radio(
                    'footertype',
                    __( 'Footer', 'abelplugin' ),
                    array_combine( array_keys( $footers ), Sort::pluck( $footers, 'label' ) ),
                    [ 'defaultValue' => Settings::get( 'footertype' ) ]
                ),
                Field::select(
                	'footersection',
                	__( 'Footer section', 'abelplugin' ),
                	$templates,
                	[ 'defaultValue' => Settings::get( 'footersection' ) ]
                )
			];


			//add fields for the amount of zones in a menu:
			$amount = $footers[ $footerId ]['areas'];

			for( $i = 1; $i <= $amount; $i++ ){
                $fields[] = Field::flex(
                	'footer_zone_'.$i,
                	__( 'Footer Zone '.$i, 'abelplugin' ),
                	FooterLayouts::getAll(),
                	[ 
                		'defaultValue' => Settings::get( 'footer_zone_'.$i ),
                		'button_text' => __( 'Zone toevoegen', 'abelplugin' ),
                		'maxItems' => 1
                	]
                );
            }

			return $fields;
		}


		/**
		 * Return the styling fields
		 * 
		 * @return Array
		 */
		public function getStyleFields()
		{
            $colors = Colors::get();
            $colors = [ 'transparent' => __( 'Geen achtergrondkleur') ] + $colors;

			$fields = [

				Field::select(
					'default_background_color',
					__( 'Standaard achtergrondkleur', 'abelplugin' ),
					$colors,
					[ 'defaultValue' => Settings::get( 'default_background_color' ) ]
				),

				Field::select(
					'default_text_color',
					__( 'Standaard tekstkleur', 'abelplugin' ),
					Colors::getForText(),
					[ 'defaultValue' => Settings::get( 'default_text_color' ) ]
				),

				Field::select( 
					'default_fill_color',
					__( 'Standaard vulkleur', 'abelplugin' ),
					Colors::getForFill(),
					['defaultValue' => Settings::get('default_fill_color' )]
				),

				Field::select(
					'default_container_width',
					__( 'Standaard container breedte', 'abelplugin' ),
					Measurements::getWidths(),
					[ 'defaultValue' => Settings::get( 'default_container_width' ) ]
				),

				Field::select(
					'default_padding-top',
					__( 'Standaard padding top', 'abelplugin' ),
					Measurements::getSizes(),
					[ 'defaultValue' => Settings::get( 'default_padding-top' ) ]
				),

				Field::select(
					'default_padding-bottom',
					__( 'Standaard padding bottom', 'abelplugin' ),
					Measurements::getSizes(),
					[ 'defaultValue' => Settings::get( 'default_padding-bottom') ]
				),

				Field::select(
					'default_margin-top',
					__( 'Standaard margin top', 'abelplugin' ),
					Measurements::getSizes(),
					[ 'defaultValue' => Settings::get( 'default_margin-top' ) ]
				),

				Field::select(
					'default_margin-bottom',
					__( 'Standaard margin bottom', 'abelplugin' ),
					Measurements::getSizes(),
					['defaultValue' => Settings::get( 'default_margin-bottom') ]
				),

				Field::select(
					'icon_library',
					__( 'Iconen set', 'abelplugin' ),
					Icon::getLibraries( true ),
					[ 'defaultValue' => Settings::get( 'icon_library' ) ]
				)

			];

			return $fields;
		}

		/**
		 * Returns all social settings
		 *
		 * @return array
		 */
		private function getSocialFields()
		{
			$fields = [

				Field::text( 
					'social_description', 
					__('Beschrijving','abelplugin'),
					[ 'defaultValue' => Settings::get( 'social_description' ) ]
				),
				Field::text( 
					'facebook', 
					__('Facebook','abelplugin'),
					[ 'defaultValue' => Settings::get( 'facebook' ) ]
				),
				Field::text( 
					'facebook_description', 
					__('Facebook beschrijving','abelplugin'),
					[ 'defaultValue' => Settings::get( 'facebook_description' ) ]
				),
				Field::text( 
					'twitter', 
					__('Twitter','abelplugin'),
					[ 'defaultValue' => Settings::get( 'twitter' ) ]
				), 
				Field::text( 
					'twitter_description', 
					__('Twitter beschrijving','abelplugin'),
					[ 'defaultValue' => Settings::get( 'twitter_description' ) ]
				), 
				Field::text( 
					'linkedin', 
					__('Linkedin','abelplugin'),
					[ 'defaultValue' => Settings::get( 'linkedin' ) ]
				),
				Field::text( 
					'linkedin_description', 
					__('Linkedin beschrijving','abelplugin'),
					[ 'defaultValue' => Settings::get( 'linkedin_description' ) ]
				),
				Field::text( 
					'googleplus', 
					__('Google Plus','abelplugin'),
					[ 'defaultValue' => Settings::get( 'googleplus' ) ]
				),
				Field::text( 
					'googleplus_description', 
					__('Google Plus beschrijving','abelplugin'),
					[ 'defaultValue' => Settings::get( 'googleplus_description' ) ]
                ),
                Field::text(
                    'youtube',
                    __( 'Youtube', 'abelplugin' ),
                    [ 'defaultValue' => Settings::get( 'youtube' ) ]
                ),
                Field::text(
                    'youtube_description',
                    __( 'Youtube beschrijving', 'abelplugin' ),
                    [ 'defaultValue' => Settings::get( 'youtube_description' ) ]
                ),
				Field::text( 
					'instagram', 
					__('Instagram','abelplugin'),
					[ 'defaultValue' => Settings::get( 'instagram' ) ]
				),
				Field::text( 
					'instagram_description', 
					__('Instagram beschrijving','abelplugin'),
					[ 'defaultValue' => Settings::get( 'instagram_description' ) ]
				)
			];

			return $fields;
		}


		/**
		 * Return the fields for the overview pages
		 * 
		 * @return void
		 */
		private function getOverviewFields()
		{
			$pages = [ 'none' => __( 'Geen overzicht', 'abelplugin' ) ] + Pages::all();

			$fields = [

				Field::select(
					'news-page',
					__( 'Actualiteiten pagina', 'abelplugin' ),
					$pages,
					[ 'defaultValue' => Settings::get( 'news-page' )]
				),
				Field::select(
					'references-page',
					__( 'Projecten pagina', 'abelplugin' ),
					$pages,
					[ 'defaultValue' => Settings::get( 'references-page' )]
				),
				Field::select(
					'services-page',
					__( 'Diensten pagina', 'abelplugin' ),
					$pages,
					[ 'defaultValue' => Settings::get( 'services-page' )]
				),
				Field::select(
					'brand-page',
					__( 'Merken pagina', 'abelplugin' ),
					$pages,
					['defaultValue' => Settings::get( 'brand-page' )]
				),
				Field::select(
					'partner-page',
					__( 'Partners pagina', 'abelplugin' ),
					$pages,
					['defaultValue' => Settings::get( 'partner-page' )]
				),
				Field::select(
					'product-page',
					__( 'Producten pagina', 'abelplugin' ),
					$pages,
					['defaultValue' => Settings::get( 'product-page' )]
				),

				Field::select(
					'teammember-page',
					__( 'Team pagina', 'abelplugin' ),
					$pages,
					['defaultValue' => Settings::get( 'teammember-page' )]
				)
			];

			return $fields;
		}


		/**
		 * Return all fields for the site configuration settings page:
		 *
		 * @return array
		 */
		private function getConfigurationFields(){


			//create the fields array:
			$fields = array(

				Field::text(
					'ga_code',
					__('Google analytics code','abelplugin'),
					array(
						'defaultValue'	=> Settings::get( 'ga_code' )
					)
				),
				Field::text(
					'hotjar_code',
					__('Hotjar ID','abelplugin'),
					array(
						'defaultValue'	=> Settings::get( 'hotjar_code' )
					)
				),
				Field::text(
					'from_email',
					__('E-mailadres waar vanuit gemaild wordt','abelplugin'),
					array(
						'defaultValue'	=> Settings::get( 'from_email' )
					)
				),
				Field::text(
					'from_name',
					__('Naam waar vanuit gemaild wordt','abelplugin'),
					array(
						'defaultValue'	=> Settings::get( 'from_name' )
					)
				),
                Field::radio(
                    'toptask_image',
                    __( 'Toptaak beeld', 'abelplugin' ),
                    ['picture' => 'Foto', 'icon' => 'Icoon'],
                    [ 'defaultValue' => Settings::get( 'toptask_image' ) ]
                ),
				/*Field::radio(
                    'products',
                    __( 'Producten', 'abelplugin' ),
                    ['from_abel_webshop' => 'Vanuit Abel webshop', 'manual' => 'Handmatige invoer', 'none' => 'Geen'],
                    [ 'defaultValue' => Settings::get( 'products' ) ]
                ),*/
				Field::checkbox( 
					'brand_logo_alt', 
					__('Show alternative brand logos','abelplugin'),
                    [ 'defaultValue' => Settings::get( 'brand_logo_alt' ) ]
				),
				Field::checkbox( 
					'partner_logo_alt', 
					__('Show alternative partner logos','abelplugin'),
                    [ 'defaultValue' => Settings::get( 'partner_logo_alt' ) ]
				),
			);

			return $fields;

		}

		/**
		* Return all the fields for the fixed TopTasks
		*
		* @return array
		*/
		private function getTopTasksFields() {

			$fields = array (
				
				Field::repeater(
					'toptasks',
					__( 'Knoppen', 'abelplugin' ),
					[ 
						Field::button( 'toptaskbutton', __( 'Klik hier','abelplugin' ) )	
					],
					[
						'defaultValue' => Settings::get( 'toptasks' )
					]
				)
			);

			return $fields;
		}


	}

	if( is_admin() )
		\Abel\Admin\SettingsPageBuilder::getInstance();
