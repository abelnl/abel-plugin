<?php

	namespace Abel\Admin;

	use \Cuisine\Wrappers\Metabox;
	use \Cuisine\Wrappers\Field;
	use \Abel\Wrappers\StaticInstance;

	class PartnerMetaboxListeners extends StaticInstance{


		/**
		 * Init admin metaboxes
		 */
		function __construct(){

			$this->addMetabox();

		}


		/**
		 * Creates the metaboxes for this plugin
		 * 
		 * @return void
		 */
		private function addMetabox(){

			$sizes = [
				'default' => __( 'Originele afmetingen', 'abelplugin' ),
				'larger' => __( 'Groter', 'abelplugin' ),
				'smaller' => __( 'Kleiner', 'abelplugin' )
			];

			Metabox::make( 'Partner logo', ['partner'], ['context' => 'side'] )->set([

				Field::image(
					'logo',
					'Logo'
				),
				Field::select(
					'size',
					__( 'Afmetingen logo', 'abelplugin' ),
					$sizes,
					[ 'defaultValue' => 'default' ]
				),
				Field::image(
					'logo-alt',
					'Logo-alt'
				),
				Field::text(
					'external-link',
					__( 'Externe link', 'abelplugin' )
				)
			]);

		}
	}

	if( is_admin() )
		\Abel\Admin\PartnerMetaboxListeners::getInstance();
