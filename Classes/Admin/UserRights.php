<?php
	
	namespace Abel\Admin;

	use Cuisine\Wrappers\User;
	use Abel\Wrappers\StaticInstance;

	class UserRights extends StaticInstance{

		/**
		 * Constructor
		 * 
		 */
		public function __construct()
		{
			$this->listen();	
		}

		/**
		 * Listen for events
		 * 
		 * @return void
		 */
		public function listen()
		{
			//templates
			add_filter( 'chef_sections_show_template_ui', function(){

				if( User::hasRole( 'administrator' ) )
					return true;

				return false;

			});

			//containers
			add_filter( 'chef_sections_show_container_ui', function(){
				
				if( User::hasRole( 'administrator' ) )
					return true;

				return false;

			});

			//general section button:
			add_filter( 'chef_sections_show_section_ui', function(){

				if( User::hasRole( 'administrator' ) || User::hasRole( 'editor' ) )
					return true;

				return false;
			});


			//base section panel:
			add_filter('chef_sections_base_panel_args', function( $args ){

				$args['rules'] = [
					'userRole' => 'administrator' 
				];

				return $args;
            });
            
            //remove broadcasting rights for anybody but administrators:
            add_action( 'admin_menu', function(){
                if( User::hasRole( 'administrator') == false && isset( $_GET['post'] ) ){
                    remove_meta_box( 'threewp_broadcast', get_post_type( $_GET['post'] ), 'side' );
                }
            });

            //add gravity forms access to editors:
            add_action( 'admin_init', function(){
                $role = get_role( 'editor' );
                $role->add_cap( 'gform_full_access' );
            });
			
		}

	}

	\Abel\Admin\UserRights::getInstance();