<?php

	namespace Abel\Columns;

	use Cuisine\View\Image;
	use Cuisine\Utilities\Url;
	use Cuisine\Wrappers\Field;
	use Cuisine\Wrappers\Script;
	use Cuisine\Utilities\Session;
	use ChefSections\Columns\DefaultColumn;
	use ChefSections\Collections\SectionCollection;

	class ImageColumn extends DefaultColumn{


		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'abelimage';
	
		/*=============================================================*/
		/**             Frontend                                        */
		/*=============================================================*/

		/**
		 * Return the image with a specific size
		 * 
		 * @return String
		 */
		public function getImage( $size )
		{
			$img = $this->getField('image');
			if( isset( $img['img-id'] ) && $img['img-id'] != '' ){
				$id = $img['img-id'];
				return Image::getMediaUrl( $id, $size );

			}

			return null;
		}

	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview image
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildPreview(){

			$url = $this->getImage('medium');

			if( is_null( $url ) || $url == 'false' || $url == '' )
				$url = $this->getImage( 'full' );


			echo '<div class="img-wrapper">';

				if( !is_null( $url ) )
					echo '<img src="'.esc_attr( $url ).'"/>';

			echo '</div>';
			
		}


		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){
	

			$fields = array(

				Field::image(
					'image',
					__( 'Afbeelding', 'abelplugin' ),
					[ 'defaultValue' => $this->getField( 'image' ) ]
				),
				
				Field::select(
					'size',
					__( 'Afmeting', 'abelplugin' ),
					$this->getSizes(),
					array(
						'defaultValue' => $this->getField( 'size', '600w' )
					)
				)
			);
			

			$fields = apply_filters( 'chef_filter_column_main_fields', $fields, $this );
			return $fields;
	
		}	


		/*=============================================================*/
		/**             Getters & Setters                              */
		/*=============================================================*/


		/**
		 * Get all available sizes
		 *
		 * @return array
		 */
		public function getSizes(){

			return array_combine( 
				get_intermediate_image_sizes(),
				get_intermediate_image_sizes()
			);
		}


	}