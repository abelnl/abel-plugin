<?php

	namespace Abel\Columns;
	
	use ChefSections\Columns\DefaultColumn;
	use Cuisine\Wrappers\Field;
	use Cuisine\Wrappers\Script;
	use Cuisine\Utilities\Url;
	
	
	class MapColumn extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'map';


		/*=============================================================*/
		/**             Frontend                                       */
		/*=============================================================*/

		/**
		 * Returns the content formatted
		 * 
		 * @return String
		 */
		public function getContent()
		{
			return json_encode( apply_filters( 'the_content', $this->getField( 'content' ) ) );				
		}
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
	
			echo '<p>'.$this->getField( 'address' ).'</p>';
			$apikey = ( $this->getField('apikey')&&$this->getField('apikey')!='' )?$this->getField('apikey'):'AIzaSyAYRXKiWhZdalPN4HYwJFJrjKNqnCHVujU';
			
			echo '<img src="https://maps.googleapis.com/maps/api/staticmap?center='.$this->getField( 'latlng' )['lat'].','.$this->getField( 'latlng' )['lng'].'&zoom=20&scale=false&size=200x200&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:red%7C'.$this->getField( 'latlng' )['lat'].','.$this->getField( 'latlng' )['lng'].'&markers=size:mid%7Ccolor:red%7Clabel:1%7C'.$this->getField( 'address' ).'&key='.$apikey.'">';
		}

		/**
		 * Start the column template
		 * 
		 * @return string ( html, echoed )
		 */
		public function beforeTemplate(){
			$apikey = ( $this->getField('apikey')&&$this->getField('apikey')!='' )?$this->getField('apikey'):'AIzaSyAYRXKiWhZdalPN4HYwJFJrjKNqnCHVujU';
			$mapsUrl = 'https://maps.googleapis.com/maps/api/js?key='.$apikey;
			echo '<script src="'.$mapsUrl.'"></script>';
			$this->setLocationsVar();

		}

		/**
		 * Add the javascript vars for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function setLocationsVar(){
	
			//get the required vars:

			//lat lng
			$latlng = $this->getField('latlng');
			
			//infowindow
			$infowindow = '';

			//show controls
			$controls = $this->getField( 'showcontrols' );

			if( $controls == 'true' ){

				$controls = true;

			}else {

				$controls = false;

			}

			//show infowindow
			$showinfowindow = $this->getField( 'showinfowindow' );

			if( $showinfowindow == 'true' ){

				$infowindow = $this->getField( 'content' );

			} else {

				$showinfowindow = false;

			}

			//set variables object:
			$vars = array(

				'locations' => array(

						'address' 		=> $this->getField( 'address'),
						'lat'			=> $latlng['lat'],
						'lng'			=> $latlng['lng'],
						'infowindow'	=> $infowindow
				),

				'showcontrols' 			=> $controls,
				'showinfowindow' 		=> $showinfowindow,
				'zoomlevel'				=> (int)$this->getField( 'zoomlevel' )
			);

			Script::variable( 'Locations', $vars );
			
		}

		/**
		 * Save the properties of this column
		 * 
		 * @return bool
		 */
		public function saveProperties(){

			global $post;
			$props = $_POST['properties'];


			if (isset( $props['address']) ){

				if ($props['address'] != $this->getField('address')) {
					if ($props['region'])
						$props['latlng'] = $this->getLatLng( $props['address'], $props['region'] );
					else
						$props['latlng'] = $this->getLatLng( $props['address'] );
				}else {
					$props['latlng'] = $this->getField( 'latlng' );
				}
			}

			if( !isset( $props['showcontrols'] ) )
				$props['showcontrols'] = 0;

			if( !isset( $props['showinfowindow'] ) )
				$props['showinfowindow'] = 0;

			$saved = update_post_meta( 
				$post->ID, 
				'_column_props_'.$this->fullId, 
				$props
			);

			//set the new properties in this class
			$this->properties = $props;
			return $saved;

		}
	
	
		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
	
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){
	
			$fields = array(

				Field::hidden( 
					'apikey', //this needs a unique id 
					array(
						'label'				=> false,
						'placeholder'		=> __( 'Een geldige JS SDK API Key (Static Map Permissions voor de preview)', 'abelplugin' ),
						'defaultValue' 		=> $this->getField( 'apikey' )
					)
				),
				Field::text( 
					'address', //this needs a unique id 
					__( 'Adres', 'abelplugin' ), 
					array(
						'label'				=> false,
						'placeholder'		=> __( 'Adres', 'abelplugin' ),
						'defaultValue' 		=> $this->getField( 'address' )
					)
				),
				Field::hidden( 
					'region', //this needs a unique id 
					array(
						'label'				=> false,
						'placeholder'		=> __( 'Regio (default - Nederland)', 'abelplugin' ),
						'defaultValue' 		=> $this->getField( 'region' )
					)
				),
				Field::number( 
					'zoomlevel', //this needs a unique id 
					__( 'Zoom level', 'abelplugin' ), 
					array(
						'label'				=> __( 'Zoom level', 'abelplugin' ),
						'placeholder'		=> __( 'Zoom level', 'abelplugin' ),
						'defaultValue' 		=> $this->getField( 'zoomlevel', 16 ),
						'userRoles' 		=> ['administrator']
					)
				),
				Field::editor( 
					'content', //this needs a unique id 
					__( 'Informatie popup', 'abelplugin' ), 
					array(
						'defaultValue' 		=> $this->getField( 'content' ),
						'id'				=> $this->fullId
					)
				),
				Field::checkbox( 
					'showcontrols', //this needs a unique id 
					__( 'Toon mapcontrols', 'abelplugin' ), 
					array(
						'defaultValue' 		=> $this->getField( 'showcontrols', false ),
						'class'				=> array( 'input-field', 'field-showcontrols', 'type-checkbox', 'subfield' ),
						'userRoles' 		=> ['administrator']
					)
				),
				Field::checkbox( 
					'showinfowindow', //this needs a unique id 
					__( 'Toon infowindow', 'abelplugin' ), 
					array(
						'defaultValue' 		=> $this->getField( 'showinfowindow', false ),
						'class'				=> array( 'input-field', 'field-showinfowindow', 'type-checkbox', 'subfield' ),
						'userRoles' 		=> ['administrator']
					)
				)

			);
			
			
			return $fields;
	
		}	

		/**
		* Get the Latitude and Longitude of a address
		*
		* @return Array (lat and long) 
		*/
		public function getLatLng( $address, $region = 'Nederland' ){

			$address = str_replace( ' ', '+', $address );
			$region = str_replace( ' ', '+', $region ); 

			$json = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false&region='.$region);
			$json = json_decode($json);

			$latlng = Array (
				"lat" => 0,
				"lng" => 0
			);

			try {
			    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

				$latlng = Array (
					"lat" => $lat,
					"lng" => $long
				);
			} catch (Exception $e) {
			    // TODO: Add exceptionhandling here!

			} 

			return $latlng;
		}
	}

	