<?php

	namespace Abel\Columns;
	
	use ChefSections\Columns\DefaultColumn;
	use Cuisine\Wrappers\Field;
    use Cuisine\Utilities\Url;
    use Cuisine\Utilities\Sort;
	
	
	class ProductFeedColumn extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'productfeed';
	
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
            
            $amount = count( $this->getField( 'feed', [] ) );
            echo '<strong>'.sprintf( __( '%s producten weergegeven', 'abelplugin' ), $amount ).'</strong>';
	
		}
	
		/**
		 * Check to see if this column has buttons
		 * 
		 * @return boolean
		 */
		public function hasProducts()
		{
			if( $this->getField( 'feed', false ) ){

				if( !empty( $this->getField( 'feed', [] ) ) )
					return true;
			}

			return false;
		}

        /**
         * Returns the feed
         *
         * @return String
         */
        public function getFeed()
        {
            if( $this->hasProducts() ){

                $feed = [];
                $products = Sort::byField( $this->getField( 'feed', [] ), 'position' );
                foreach( $products as $product ){
                    $feed[] = $product['url'];
                }

                return json_encode( $feed );
            }

            return '';
        }


		/**
		 * Simple echo function for the getField method
		 *
		 * @param  string $name
		 * @return string ( html, echoed )
		 */
		public function theField( $name, $default = null ){

			if( !is_null( $this->getField( $name, $default ) ) ){
				echo $this->getField( $name, $default );
			}
		}
	



		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){
	
			$fields = array(
				Field::repeater(
					'feed',
					__( 'Product Urls', 'abelplugin' ),
					$this->getUrlFields(),
					[
						'defaultValue' => $this->getField( 'feed' ),
						'view' => 'compact'
					]
				),
				Field::text( 
					'shopurl',
					__( 'Webshop Url', 'abelplugin' ),
					[ 'defaultValue' => $this->getField( 'shopurl' ) ]
				)
			);

			return $fields;
	
		}	


		/**
		 * Returns the button fields
		 * 
		 * @return Array
		 */
		public function getUrlFields()
		{
			return [
				Field::text( 'url', __('Product Url','abelplugin') )
			];
		}
	}

	