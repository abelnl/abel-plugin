<?php

	namespace Abel\Columns;
	
	use Abel\Front\Settings;
	use ChefSections\Columns\DefaultColumn;
	use Cuisine\Wrappers\Field;
	use Cuisine\Utilities\Url;
	
	
	class ToptaskColumn extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'toptask';
	
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
	
			$title = $this->getField( 'title' );
			echo '<strong>'.esc_html( $title['text'] ).'</strong>';
	
		}
	
	
		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){
	
			$fields = array(

				0 => Field::title( 
					'title',
					__('Titel', 'abelplugin'),
					
					array(
						'label' 		=> false,	// Show Label false - top - left
						'placeholder' 	=> __('Titel', 'abelplugin'),
						'defaultValue'	=> $this->getField( 'title' ),
					)
				),


				4 => Field::textarea(
					'description',
					__('Beschrijving', 'abelplugin'),
					array(
						'defaultValue' => $this->getField( 'description' )
					)
				),

				5 => Field::button(
					'button',
					__( 'Button', 'abelplugin' ),
					array(
						'defaultValue' => $this->getField( 'button' )
					)
				)
			);


			$type = Settings::get( 'toptask_image', 'icon' );

			if( $type == 'icon' ){

				$fields[2] = Field::icon(
					'icon',
					__('Icoon', 'abelplugin'),
					array(
						'defaultValue' => $this->getField( 'icon' )
					)
				);

				$fields[3] = Field::hidden( 'image', [ 'defaultValue' => '' ]);

			}else{

				$fields[2] = Field::image(
					'image',
					__('Afbeelding', 'abelplugin'),
					array(
						'defaultValue' => $this->getField( 'image', [] )
					)
				);

				$fields[3] = Field::hidden( 'icon', [ 'defaultValue' => 'none' ]);
			}
			
			
			ksort( $fields );
			return $fields;
	
		}	
	}

	