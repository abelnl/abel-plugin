<?php

	namespace Abel\Columns;
	
	use Cuisine\Wrappers\Field;
	use Cuisine\Utilities\Url;
	use ChefSections\Columns\DefaultColumn;
	
	
	class CtaColumn extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'cta';
	
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
	
			$title = $this->getField( 'title' );
			echo '<strong>'.esc_html( $title['text'] ).'</strong>';
	
		}
	
		/**
		 * Check to see if this column has buttons
		 * 
		 * @return boolean
		 */
		public function hasButtons()
		{
			if( $this->getField( 'buttons', false ) ){

				if( sizeof( $this->getField( 'buttons', [] ) ) > 0 )
					return true;
			}

			return false;
		}

		/**
		 * Simple echo function for the getField method
		 *
		 * @param  string $name
		 * @return string ( html, echoed )
		 */
		public function theField( $name, $default = null ){

			if( !is_null( $this->getField( $name, $default ) ) ){

				if( $name == 'description' ){

					echo apply_filters( 'the_content', $this->getField( $name, $default ) );

				}else{

					echo $this->getField( $name, $default );

				}
			}
		}
	



		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){
	
			$fields = array(

				Field::title( 
					'title',
					__( 'Titel', 'abelplugin' ),
					[ 'defaultValue' => $this->getField( 'title' ) ]
				),
				Field::textarea(
					'description',
					__( 'Beschrijving', 'abelplugin' ),
					[ 'defaultValue' => $this->getField( 'description' ) ]
				),

				Field::repeater(
					'buttons',
					__( 'Knoppen', 'abelplugin' ),
					[ 
						Field::button( 'button', __( 'Klik hier','abelplugin' ) )	
					],
					[
						'defaultValue' => $this->getField( 'buttons' )
					]
				),
				Field::image(
					'image',
					__('Afbeelding', 'abelplugin'),
					array(
						'defaultValue' => $this->getField( 'image' )
					)
				),
				Field::checkbox( 
					'showsocials', //this needs a unique id 
					__( 'Toon social media iconen', 'abelplugin' ), 
					array(
						'defaultValue' 		=> $this->getField( 'showsocials', false ),
						'class'				=> array( 'input-field', 'field-showsocials', 'type-checkbox', 'subfield' )
					)
				)


			);
			
			
			return $fields;
	
		}	

	}

	