<?php

	namespace Abel\Columns;

	use GFAPI;
	use Cuisine\Wrappers\Field;
	use Cuisine\Utilities\Sort;
	use ChefSections\Columns\DefaultColumn;

	class FormColumn extends DefaultColumn{


		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'form';
	
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
			echo '<strong>FORM</strong>';	
		}

		/**
		 * Returns the field
		 *
		 * @param String $name
		 * @param mixed $default
		 * @return mixed
		 */
		public function getField( $name, $default = null ){
			
			if( 
				!isset(  $this->properties[ $name ] ) && 
				$name == 'form' && 
				is_null( $default )
			){
				$forms = GFAPI::get_forms();
				$forms = array_combine( Sort::pluck( $forms, 'id' ), Sort::pluck( $forms, 'title' ) );
				$default = array_keys( $forms );
				$default = $default[0];

				return $default;
			}
			
			return parent::getField( $name, $default );
		}
	
		/**
		 * Simple echo function for the getField method
		 *
		 * @param  string $name
		 * @return string ( html, echoed )
		 */
		public function theField( $name, $default = null ){

			if( !is_null( $this->getField( $name, $default ) ) ){

				if( $name == 'form' ){

					$shortcode = '[gravityform id="';
					$shortcode .= $this->getField( 'form' );
					$shortcode .= '" title="false" description="false" ajax="true"]';
					echo do_shortcode( $shortcode );

				}else{

					echo $this->getField( $name, $default );

				}
			}
		}
	



		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){

			$forms = GFAPI::get_forms();
			$forms = array_combine( Sort::pluck( $forms, 'id' ), Sort::pluck( $forms, 'title' ) );

			$fields = array(

				Field::select(
					'form',
					__( 'Formulier', 'abelplugin' ),
					$forms,
					[ 
						'defaultValue' => $this->getField( 'form' ),
						'userRoles' => ['administrator']
					]
				)

			);
			
			
			return $fields;
	
		}	

	}