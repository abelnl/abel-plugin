<?php

	namespace Abel\Columns;
	
	use ChefSections\Columns\DefaultColumn;
	use Cuisine\Wrappers\Field;
	use Cuisine\Utilities\Url;
	
	
	class QuoteColumn extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'quote';
	
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
	
			$quote = $this->getField( 'quote' );
			$author = $this->getField( 'author' );
			echo '<blockquote>'.esc_html( $quote ).'</blockquote>';
			if ( $author && $author != '' ) {
				echo '<span class=author>'.esc_html( $author ).'</span>';
			}
	
		}
	
	
		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){

			$alignment = array(

				'left'			=> __( 'Links', 'abelplugin' ),
				'right'			=> __( 'Rechts', 'abelplugin' ),
				'center'		=> __( 'Centreer', 'abelplugin' )

			);
	
			$fields = array(

				Field::textarea( 
					'quote',
					__('Quote', 'abelplugin'),
					
					array(
						'defaultValue'	=> $this->getField( 'quote' ),
					)
				),

				Field::text(
					'author',
					__('Auteur', 'abelplugin'),

					array(
						'defaultValue' => $this->getField( 'author' )
					)
				),

				Field::select(
					'alignement',
					__( 'Uitlijnen', 'chefsections' ),
					$alignment,
					array(
						'defaultValue'		=> $this->getField( 'alignment', 'center' )
						
					)
				),

				Field::image(
					'avatar',
					__( 'Foto', 'abelplugin' ),
					[
						'defaultValue' => $this->getField( 'avatar' )
					]
				)
			);
			
			
			return $fields;
	
		}	
	}

	