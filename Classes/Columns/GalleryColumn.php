<?php

	namespace Abel\Columns;
	
	use Cuisine\Utilities\Url;
	use Cuisine\Utilities\Sort;
	use Cuisine\Wrappers\Field;
	use ChefSections\Columns\DefaultColumn;
	
	
	class GalleryColumn extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'gallery';
	
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
		
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
	
			$media = $this->getMedia();
			$i = 0;
			echo '<div style="text-align:center;padding-bottom:10px;">';
			foreach( $media as $item ){

				if( $i < 4 )
					echo '<img src="'.$item['preview'].'" style="max-width:50px;height:auto;margin:0 5px 5px 0"/>';

				$i++;
			}
			echo '</div>';
	
		}
	

		/**
		 * Return the media, sorted
		 * 
		 * @return Array
		 */
		public function getMedia()
		{
			$media = $this->getField( 'media', [] );
			if( !empty( $media ) )
				$media = Sort::byField( $media, 'position' );

			return $media;
		}

		/**
		 * Checks if this column has a lightbox
		 * 
		 * @return boolean
		 */
		public function hasLightbox()
		{
			$has = $this->getField( 'add_lightbox' );
			if( $has && (string)$has == 'true' )
				return true;

			return false;
		}

		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
				
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) )
						echo $field->renderTemplate();
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
				$subfields = $this->getSubFields();

				foreach( $subfields as $field ){

					$field->render();

					if( method_exists( $field, 'renderTemplate' ) )
						echo $field->renderTemplate();
				}
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array
		 */
		public function getFields(){
	
			$fields = array(

				Field::media(
					'media',
					__( 'Media', 'abelplugin' ),
					[ 'defaultValue' => $this->getField( 'media' ) ]
				)
			);
			
			
			return $fields;
	
		}	


		/**
		 * Returns the side-fields
		 * 
		 * @return Array
		 */
		public function getSubFields()
		{
			$nav = array(
				'none'			=> __( 'None', 'abelplugin' ),
				'pagination'	=> __( 'Pagination', 'abelplugin' )
			);

			$nav_arrows = array(
				'none'			=> __( 'None', 'abelplugin' ),
				'arrows'	=> __( 'Arrows', 'abelplugin' )
			);

			$fields = [

				Field::radio(
					'view',
					__( 'Weergave', 'abelplugin' ),
					[
						'overview' => __( 'Overview', 'abelplugin' ),
						'grid' => __( 'Grid', 'abelplugin' ),
						'slider' => __( 'Slider', 'abelplugin' )
					],
					[
						'defaultValue' => $this->getField( 'view', 'slider' ),
						'subname' => $this->fullId.'view',
						'wrapper-class' => ['viewselect']
					]
				),

				Field::number(
					'slider-time',
					__( 'Auto-slide time', 'abelplugin' ),
					[
						'defaultValue' => $this->getField( 'slider-time', 0 ),
						'wrapper-class' => ['slidertime']
					]
				),

				Field::checkbox(
					'add_lightbox',
					__( 'Volledige weergave toevoegen', 'abelplugin' ),
					[ 'defaultValue' => $this->getField( 'add_lightbox', 'true' ) ]
				),

				Field::text( 
					'posts_per_row', //this needs a unique id 
					__( 'Afbeeldingen per rij', 'abelplugin' ),
					array(
						'defaultValue' 		=> $this->getField( 'posts_per_row', 1 )
					)
				),
				Field::hidden( 
					'posts_to_scroll', //this needs a unique id 
					array(
						'label'				=> false,
						'defaultValue' 		=> $this->getField( 'posts_to_scroll', 1 )
					)
				),
				Field::radio(
					'nav',
					__( 'Navigation', 'abelplugin' ),
					$nav,
					array(
						'defaultValue'	=> $this->getField( 'nav', 'none' ),
						'subname' => $this->fullId.'nav'
					)
				),
				Field::radio(
					'nav_arrows',
					__( 'Navigation arrows', 'abelplugin' ),
					$nav_arrows,
					array(
						'defaultValue' => $this->getField( 'nav_arrows', 'none' ),
						'subname' => $this->fullId.'nav_arrows'
					)
				)
			];

			return $fields;	
		}

	}

	